-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2015 at 03:16 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thesis_pofms`
--
CREATE DATABASE `thesis_pofms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `thesis_pofms`;

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_acos`
--

DROP TABLE IF EXISTS `pfoms_acos`;
CREATE TABLE IF NOT EXISTS `pfoms_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pfoms_acos`
--


-- --------------------------------------------------------

--
-- Table structure for table `pfoms_aros`
--

DROP TABLE IF EXISTS `pfoms_aros`;
CREATE TABLE IF NOT EXISTS `pfoms_aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pfoms_aros`
--

INSERT INTO `pfoms_aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(3, NULL, 'User', 1, NULL, 1, 2),
(4, NULL, 'User', 1, NULL, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_aros_acos`
--

DROP TABLE IF EXISTS `pfoms_aros_acos`;
CREATE TABLE IF NOT EXISTS `pfoms_aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pfoms_aros_acos`
--


-- --------------------------------------------------------

--
-- Table structure for table `pfoms_gradelevels`
--

DROP TABLE IF EXISTS `pfoms_gradelevels`;
CREATE TABLE IF NOT EXISTS `pfoms_gradelevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `pfoms_gradelevels`
--

INSERT INTO `pfoms_gradelevels` (`id`, `name`) VALUES
(1, 'Grade 1'),
(2, 'Grade 6'),
(3, 'Grade 2'),
(4, 'Grade 3'),
(5, 'Grade 4'),
(6, 'Grade 5'),
(7, 'Grade 7'),
(8, 'Grade 8'),
(9, 'Grade 9'),
(10, 'Grade 10'),
(11, 'Grade 11'),
(12, 'Grade 12');

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_gradesections`
--

DROP TABLE IF EXISTS `pfoms_gradesections`;
CREATE TABLE IF NOT EXISTS `pfoms_gradesections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gradelevel_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pfoms_gradesections`
--

INSERT INTO `pfoms_gradesections` (`id`, `gradelevel_id`, `name`) VALUES
(1, 2, 'Section A'),
(2, 2, 'Section B'),
(3, 2, 'Section C');

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_groups`
--

DROP TABLE IF EXISTS `pfoms_groups`;
CREATE TABLE IF NOT EXISTS `pfoms_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pfoms_groups`
--


-- --------------------------------------------------------

--
-- Table structure for table `pfoms_positions`
--

DROP TABLE IF EXISTS `pfoms_positions`;
CREATE TABLE IF NOT EXISTS `pfoms_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pfoms_positions`
--


-- --------------------------------------------------------

--
-- Table structure for table `pfoms_schoolgrades`
--

DROP TABLE IF EXISTS `pfoms_schoolgrades`;
CREATE TABLE IF NOT EXISTS `pfoms_schoolgrades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `gradelevel_id` int(11) NOT NULL,
  `gradesection_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `schoolsubject_id` int(11) DEFAULT NULL,
  `grades` text NOT NULL,
  `first_quarter` decimal(12,2) DEFAULT NULL,
  `second_quarter` decimal(12,2) DEFAULT NULL,
  `third_quarter` decimal(12,2) DEFAULT NULL,
  `fourth_quarter` decimal(12,2) DEFAULT NULL,
  `quarter_tgrade` decimal(12,2) DEFAULT NULL,
  `tgrade` decimal(12,2) DEFAULT NULL,
  `ggrade` decimal(12,2) DEFAULT NULL,
  `schoolyear_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `added` date DEFAULT NULL,
  `modified` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `pfoms_schoolgrades`
--

INSERT INTO `pfoms_schoolgrades` (`id`, `student_id`, `gradelevel_id`, `gradesection_id`, `teacher_id`, `schoolsubject_id`, `grades`, `first_quarter`, `second_quarter`, `third_quarter`, `fourth_quarter`, `quarter_tgrade`, `tgrade`, `ggrade`, `schoolyear_id`, `user_id`, `added`, `modified`, `modified_by`) VALUES
(1, 3, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,80,75,75,75,75,SCIENCE,80,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,78,75,75,75,75,EPP,78,75,75,75,75,HEKASI,80,75,75,75,75,MSEP,77,75,75,75,75,VALUES EDUCATION,76,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(2, 4, 2, 1, 1, NULL, 'ENGLISH,76,76,76,76,76,MATHEMATICS,78,76,76,76,76,SCIENCE,78,76,76,76,76,FILIPINO,76,76,76,76,76,MAKABAYAN,79,76,76,76,76,EPP,78,76,76,76,76,HEKASI,81,76,76,76,76,MSEP,77,76,76,76,76,VALUES EDUCATION,81,76,76,76,76', NULL, NULL, NULL, NULL, NULL, 76.00, 76.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(3, 5, 2, 1, 1, NULL, 'ENGLISH,70,70,70,70,70,MATHEMATICS,72,70,70,70,70,SCIENCE,71,70,70,70,70,FILIPINO,70,70,70,70,70,MAKABAYAN,72,70,70,70,70,EPP,75,70,70,70,70,HEKASI,70,70,70,70,70,MSEP,72,70,70,70,70,VALUES EDUCATION,72,70,70,70,70', NULL, NULL, NULL, NULL, NULL, 70.00, 70.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(4, 6, 2, 1, 1, NULL, 'ENGLISH,72,72,72,72,72,MATHEMATICS,70,72,72,72,72,SCIENCE,72,72,72,72,72,FILIPINO,70,72,72,72,72,MAKABAYAN,74,72,72,72,72,EPP,76,72,72,72,72,HEKASI,72,72,72,72,72,MSEP,73,72,72,72,72,VALUES EDUCATION,76,72,72,72,72', NULL, NULL, NULL, NULL, NULL, 72.00, 72.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(5, 7, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,77,75,75,75,75,SCIENCE,76,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,79,75,75,75,75,EPP,78,75,75,75,75,HEKASI,81,75,75,75,75,MSEP,76,75,75,75,75,VALUES EDUCATION,81,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(6, 8, 2, 1, 1, NULL, 'ENGLISH,70,70,70,70,70,MATHEMATICS,71,70,70,70,70,SCIENCE,73,70,70,70,70,FILIPINO,70,70,70,70,70,MAKABAYAN,73,70,70,70,70,EPP,75,70,70,70,70,HEKASI,72,70,70,70,70,MSEP,73,70,70,70,70,VALUES EDUCATION,72,70,70,70,70', NULL, NULL, NULL, NULL, NULL, 70.00, 70.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(7, 9, 2, 1, 1, NULL, 'ENGLISH,85,85,85,85,85,MATHEMATICS,80,85,85,85,85,SCIENCE,81,85,85,85,85,FILIPINO,83,85,85,85,85,MAKABAYAN,82,85,85,85,85,EPP,85,85,85,85,85,HEKASI,80,85,85,85,85,MSEP,78,85,85,85,85,VALUES EDUCATION,83,85,85,85,85', NULL, NULL, NULL, NULL, NULL, 85.00, 85.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(8, 11, 2, 1, 1, NULL, 'ENGLISH,79,79,79,79,79,MATHEMATICS,79,79,79,79,79,SCIENCE,80,79,79,79,79,FILIPINO,81,79,79,79,79,MAKABAYAN,81,79,79,79,79,EPP,82,79,79,79,79,HEKASI,82,79,79,79,79,MSEP,77,79,79,79,79,VALUES EDUCATION,81,79,79,79,79', NULL, NULL, NULL, NULL, NULL, 79.00, 79.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(9, 12, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,76,75,75,75,75,SCIENCE,77,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,76,75,75,75,75,EPP,77,75,75,75,75,HEKASI,76,75,75,75,75,MSEP,75,75,75,75,75,VALUES EDUCATION,75,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(10, 13, 2, 1, 1, NULL, 'ENGLISH,79,79,79,79,79,MATHEMATICS,76,79,79,79,79,SCIENCE,77,79,79,79,79,FILIPINO,80,79,79,79,79,MAKABAYAN,79,79,79,79,79,EPP,82,79,79,79,79,HEKASI,77,79,79,79,79,MSEP,75,79,79,79,79,VALUES EDUCATION,82,79,79,79,79', NULL, NULL, NULL, NULL, NULL, 79.00, 79.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(11, 14, 2, 1, 1, NULL, 'ENGLISH,78,78,78,78,78,MATHEMATICS,78,78,78,78,78,SCIENCE,78,78,78,78,78,FILIPINO,78,78,78,78,78,MAKABAYAN,78,78,78,78,78,EPP,79,78,78,78,78,HEKASI,76,78,78,78,78,MSEP,77,78,78,78,78,VALUES EDUCATION,82,78,78,78,78', NULL, NULL, NULL, NULL, NULL, 78.00, 78.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(12, 15, 2, 1, 1, NULL, 'ENGLISH,72,72,72,72,72,MATHEMATICS,73,72,72,72,72,SCIENCE,76,72,72,72,72,FILIPINO,72,72,72,72,72,MAKABAYAN,75,72,72,72,72,EPP,73,72,72,72,72,HEKASI,77,72,72,72,72,MSEP,75,72,72,72,72,VALUES EDUCATION,76,72,72,72,72', NULL, NULL, NULL, NULL, NULL, 72.00, 72.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(13, 16, 2, 1, 1, NULL, 'ENGLISH,86,86,86,86,86,MATHEMATICS,83,86,86,86,86,SCIENCE,83,86,86,86,86,FILIPINO,86,86,86,86,86,MAKABAYAN,84,86,86,86,86,EPP,85,86,86,86,86,HEKASI,85,86,86,86,86,MSEP,79,86,86,86,86,VALUES EDUCATION,85,86,86,86,86', NULL, NULL, NULL, NULL, NULL, 86.00, 86.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(14, 17, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,76,75,75,75,75,SCIENCE,79,75,75,75,75,FILIPINO,76,75,75,75,75,MAKABAYAN,76,75,75,75,75,EPP,76,75,75,75,75,HEKASI,78,75,75,75,75,MSEP,76,75,75,75,75,VALUES EDUCATION,75,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(15, 18, 2, 1, 1, NULL, 'ENGLISH,81,81,81,81,81,MATHEMATICS,82,81,81,81,81,SCIENCE,82,81,81,81,81,FILIPINO,84,81,81,81,81,MAKABAYAN,83,81,81,81,81,EPP,85,81,81,81,81,HEKASI,84,81,81,81,81,MSEP,81,81,81,81,81,VALUES EDUCATION,81,81,81,81,81', NULL, NULL, NULL, NULL, NULL, 81.00, 81.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(16, 19, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,76,75,75,75,75,SCIENCE,76,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,77,75,75,75,75,EPP,78,75,75,75,75,HEKASI,76,75,75,75,75,MSEP,77,75,75,75,75,VALUES EDUCATION,78,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(17, 20, 2, 1, 1, NULL, 'ENGLISH,72,72,72,72,72,MATHEMATICS,75,72,72,72,72,SCIENCE,80,72,72,72,72,FILIPINO,72,72,72,72,72,MAKABAYAN,75,72,72,72,72,EPP,76,72,72,72,72,HEKASI,72,72,72,72,72,MSEP,75,72,72,72,72,VALUES EDUCATION,75,72,72,72,72', NULL, NULL, NULL, NULL, NULL, 72.00, 72.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(18, 21, 2, 1, 1, NULL, 'ENGLISH,80,80,80,80,80,MATHEMATICS,77,80,80,80,80,SCIENCE,77,80,80,80,80,FILIPINO,83,80,80,80,80,MAKABAYAN,81,80,80,80,80,EPP,84,80,80,80,80,HEKASI,78,80,80,80,80,MSEP,77,80,80,80,80,VALUES EDUCATION,86,80,80,80,80', NULL, NULL, NULL, NULL, NULL, 80.00, 80.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(19, 22, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,77,75,75,75,75,SCIENCE,79,75,75,75,75,FILIPINO,76,75,75,75,75,MAKABAYAN,79,75,75,75,75,EPP,78,75,75,75,75,HEKASI,80,75,75,75,75,MSEP,78,75,75,75,75,VALUES EDUCATION,80,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(20, 23, 2, 1, 1, NULL, 'ENGLISH,78,78,78,78,78,MATHEMATICS,77,78,78,78,78,SCIENCE,78,78,78,78,78,FILIPINO,79,78,78,78,78,MAKABAYAN,78,78,78,78,78,EPP,79,78,78,78,78,HEKASI,81,78,78,78,78,MSEP,76,78,78,78,78,VALUES EDUCATION,75,78,78,78,78', NULL, NULL, NULL, NULL, NULL, 78.00, 78.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(21, 24, 2, 1, 1, NULL, 'ENGLISH,79,79,79,79,79,MATHEMATICS,77,79,79,79,79,SCIENCE,79,79,79,79,79,FILIPINO,79,79,79,79,79,MAKABAYAN,78,79,79,79,79,EPP,81,79,79,79,79,HEKASI,77,79,79,79,79,MSEP,78,79,79,79,79,VALUES EDUCATION,77,79,79,79,79', NULL, NULL, NULL, NULL, NULL, 79.00, 79.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(22, 25, 2, 1, 1, NULL, 'ENGLISH,82,82,82,82,82,MATHEMATICS,81,82,82,82,82,SCIENCE,82,82,82,82,82,FILIPINO,83,82,82,82,82,MAKABAYAN,83,82,82,82,82,EPP,85,82,82,82,82,HEKASI,80,82,82,82,82,MSEP,80,82,82,82,82,VALUES EDUCATION,85,82,82,82,82', NULL, NULL, NULL, NULL, NULL, 82.00, 82.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(23, 26, 2, 1, 1, NULL, 'ENGLISH,83,83,83,83,83,MATHEMATICS,81,83,83,83,83,SCIENCE,83,83,83,83,83,FILIPINO,84,83,83,83,83,MAKABAYAN,83,83,83,83,83,EPP,83,83,83,83,83,HEKASI,82,83,83,83,83,MSEP,81,83,83,83,83,VALUES EDUCATION,85,83,83,83,83', NULL, NULL, NULL, NULL, NULL, 83.00, 83.00, 1, 1, '2015-02-16', '2015-02-16', 0),
(24, 29, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,80,75,75,75,75,SCIENCE,80,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,78,75,75,75,75,EPP,78,75,75,75,75,HEKASI,80,75,75,75,75,MSEP,77,75,75,75,75,VALUES EDUCATION,76,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(25, 30, 2, 1, 1, NULL, 'ENGLISH,76,76,76,76,76,MATHEMATICS,78,76,76,76,76,SCIENCE,78,76,76,76,76,FILIPINO,76,76,76,76,76,MAKABAYAN,79,76,76,76,76,EPP,78,76,76,76,76,HEKASI,81,76,76,76,76,MSEP,77,76,76,76,76,VALUES EDUCATION,81,76,76,76,76', NULL, NULL, NULL, NULL, NULL, 76.00, 76.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(26, 31, 2, 1, 1, NULL, 'ENGLISH,70,70,70,70,70,MATHEMATICS,72,70,70,70,70,SCIENCE,71,70,70,70,70,FILIPINO,70,70,70,70,70,MAKABAYAN,72,70,70,70,70,EPP,75,70,70,70,70,HEKASI,70,70,70,70,70,MSEP,72,70,70,70,70,VALUES EDUCATION,72,70,70,70,70', NULL, NULL, NULL, NULL, NULL, 70.00, 70.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(27, 32, 2, 1, 1, NULL, 'ENGLISH,72,72,72,72,72,MATHEMATICS,70,72,72,72,72,SCIENCE,72,72,72,72,72,FILIPINO,70,72,72,72,72,MAKABAYAN,74,72,72,72,72,EPP,76,72,72,72,72,HEKASI,72,72,72,72,72,MSEP,73,72,72,72,72,VALUES EDUCATION,76,72,72,72,72', NULL, NULL, NULL, NULL, NULL, 72.00, 72.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(28, 33, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,77,75,75,75,75,SCIENCE,76,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,79,75,75,75,75,EPP,78,75,75,75,75,HEKASI,81,75,75,75,75,MSEP,76,75,75,75,75,VALUES EDUCATION,81,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(29, 34, 2, 1, 1, NULL, 'ENGLISH,70,70,70,70,70,MATHEMATICS,71,70,70,70,70,SCIENCE,73,70,70,70,70,FILIPINO,70,70,70,70,70,MAKABAYAN,73,70,70,70,70,EPP,75,70,70,70,70,HEKASI,72,70,70,70,70,MSEP,73,70,70,70,70,VALUES EDUCATION,72,70,70,70,70', NULL, NULL, NULL, NULL, NULL, 70.00, 70.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(30, 35, 2, 1, 1, NULL, 'ENGLISH,85,85,85,85,85,MATHEMATICS,80,85,85,85,85,SCIENCE,81,85,85,85,85,FILIPINO,83,85,85,85,85,MAKABAYAN,82,85,85,85,85,EPP,85,85,85,85,85,HEKASI,80,85,85,85,85,MSEP,78,85,85,85,85,VALUES EDUCATION,83,85,85,85,85', NULL, NULL, NULL, NULL, NULL, 85.00, 85.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(31, 37, 2, 1, 1, NULL, 'ENGLISH,79,79,79,79,79,MATHEMATICS,79,79,79,79,79,SCIENCE,80,79,79,79,79,FILIPINO,81,79,79,79,79,MAKABAYAN,81,79,79,79,79,EPP,82,79,79,79,79,HEKASI,82,79,79,79,79,MSEP,77,79,79,79,79,VALUES EDUCATION,81,79,79,79,79', NULL, NULL, NULL, NULL, NULL, 79.00, 79.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(32, 38, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,76,75,75,75,75,SCIENCE,77,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,76,75,75,75,75,EPP,77,75,75,75,75,HEKASI,76,75,75,75,75,MSEP,75,75,75,75,75,VALUES EDUCATION,75,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(33, 39, 2, 1, 1, NULL, 'ENGLISH,79,79,79,79,79,MATHEMATICS,76,79,79,79,79,SCIENCE,77,79,79,79,79,FILIPINO,80,79,79,79,79,MAKABAYAN,79,79,79,79,79,EPP,82,79,79,79,79,HEKASI,77,79,79,79,79,MSEP,75,79,79,79,79,VALUES EDUCATION,82,79,79,79,79', NULL, NULL, NULL, NULL, NULL, 79.00, 79.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(34, 40, 2, 1, 1, NULL, 'ENGLISH,78,78,78,78,78,MATHEMATICS,78,78,78,78,78,SCIENCE,78,78,78,78,78,FILIPINO,78,78,78,78,78,MAKABAYAN,78,78,78,78,78,EPP,79,78,78,78,78,HEKASI,76,78,78,78,78,MSEP,77,78,78,78,78,VALUES EDUCATION,82,78,78,78,78', NULL, NULL, NULL, NULL, NULL, 78.00, 78.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(35, 41, 2, 1, 1, NULL, 'ENGLISH,72,72,72,72,72,MATHEMATICS,73,72,72,72,72,SCIENCE,76,72,72,72,72,FILIPINO,72,72,72,72,72,MAKABAYAN,75,72,72,72,72,EPP,73,72,72,72,72,HEKASI,77,72,72,72,72,MSEP,75,72,72,72,72,VALUES EDUCATION,76,72,72,72,72', NULL, NULL, NULL, NULL, NULL, 72.00, 72.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(36, 42, 2, 1, 1, NULL, 'ENGLISH,86,86,86,86,86,MATHEMATICS,83,86,86,86,86,SCIENCE,83,86,86,86,86,FILIPINO,86,86,86,86,86,MAKABAYAN,84,86,86,86,86,EPP,85,86,86,86,86,HEKASI,85,86,86,86,86,MSEP,79,86,86,86,86,VALUES EDUCATION,85,86,86,86,86', NULL, NULL, NULL, NULL, NULL, 86.00, 86.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(37, 43, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,76,75,75,75,75,SCIENCE,79,75,75,75,75,FILIPINO,76,75,75,75,75,MAKABAYAN,76,75,75,75,75,EPP,76,75,75,75,75,HEKASI,78,75,75,75,75,MSEP,76,75,75,75,75,VALUES EDUCATION,75,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(38, 44, 2, 1, 1, NULL, 'ENGLISH,81,81,81,81,81,MATHEMATICS,82,81,81,81,81,SCIENCE,82,81,81,81,81,FILIPINO,84,81,81,81,81,MAKABAYAN,83,81,81,81,81,EPP,85,81,81,81,81,HEKASI,84,81,81,81,81,MSEP,81,81,81,81,81,VALUES EDUCATION,81,81,81,81,81', NULL, NULL, NULL, NULL, NULL, 81.00, 81.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(39, 45, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,76,75,75,75,75,SCIENCE,76,75,75,75,75,FILIPINO,75,75,75,75,75,MAKABAYAN,77,75,75,75,75,EPP,78,75,75,75,75,HEKASI,76,75,75,75,75,MSEP,77,75,75,75,75,VALUES EDUCATION,78,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(40, 46, 2, 1, 1, NULL, 'ENGLISH,72,72,72,72,72,MATHEMATICS,75,72,72,72,72,SCIENCE,80,72,72,72,72,FILIPINO,72,72,72,72,72,MAKABAYAN,75,72,72,72,72,EPP,76,72,72,72,72,HEKASI,72,72,72,72,72,MSEP,75,72,72,72,72,VALUES EDUCATION,75,72,72,72,72', NULL, NULL, NULL, NULL, NULL, 72.00, 72.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(41, 47, 2, 1, 1, NULL, 'ENGLISH,80,80,80,80,80,MATHEMATICS,77,80,80,80,80,SCIENCE,77,80,80,80,80,FILIPINO,83,80,80,80,80,MAKABAYAN,81,80,80,80,80,EPP,84,80,80,80,80,HEKASI,78,80,80,80,80,MSEP,77,80,80,80,80,VALUES EDUCATION,86,80,80,80,80', NULL, NULL, NULL, NULL, NULL, 80.00, 80.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(42, 48, 2, 1, 1, NULL, 'ENGLISH,75,75,75,75,75,MATHEMATICS,77,75,75,75,75,SCIENCE,79,75,75,75,75,FILIPINO,76,75,75,75,75,MAKABAYAN,79,75,75,75,75,EPP,78,75,75,75,75,HEKASI,80,75,75,75,75,MSEP,78,75,75,75,75,VALUES EDUCATION,80,75,75,75,75', NULL, NULL, NULL, NULL, NULL, 75.00, 75.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(43, 49, 2, 1, 1, NULL, 'ENGLISH,78,78,78,78,78,MATHEMATICS,77,78,78,78,78,SCIENCE,78,78,78,78,78,FILIPINO,79,78,78,78,78,MAKABAYAN,78,78,78,78,78,EPP,79,78,78,78,78,HEKASI,81,78,78,78,78,MSEP,76,78,78,78,78,VALUES EDUCATION,75,78,78,78,78', NULL, NULL, NULL, NULL, NULL, 78.00, 78.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(44, 50, 2, 1, 1, NULL, 'ENGLISH,79,79,79,79,79,MATHEMATICS,77,79,79,79,79,SCIENCE,79,79,79,79,79,FILIPINO,79,79,79,79,79,MAKABAYAN,78,79,79,79,79,EPP,81,79,79,79,79,HEKASI,77,79,79,79,79,MSEP,78,79,79,79,79,VALUES EDUCATION,77,79,79,79,79', NULL, NULL, NULL, NULL, NULL, 79.00, 79.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(45, 51, 2, 1, 1, NULL, 'ENGLISH,82,82,82,82,82,MATHEMATICS,81,82,82,82,82,SCIENCE,82,82,82,82,82,FILIPINO,83,82,82,82,82,MAKABAYAN,83,82,82,82,82,EPP,85,82,82,82,82,HEKASI,80,82,82,82,82,MSEP,80,82,82,82,82,VALUES EDUCATION,85,82,82,82,82', NULL, NULL, NULL, NULL, NULL, 82.00, 82.00, 2, 1, '2015-02-16', '2015-02-16', 0),
(46, 52, 2, 1, 1, NULL, 'ENGLISH,83,83,83,83,83,MATHEMATICS,81,83,83,83,83,SCIENCE,83,83,83,83,83,FILIPINO,84,83,83,83,83,MAKABAYAN,83,83,83,83,83,EPP,83,83,83,83,83,HEKASI,82,83,83,83,83,MSEP,81,83,83,83,83,VALUES EDUCATION,85,83,83,83,83', NULL, NULL, NULL, NULL, NULL, 83.00, 83.00, 2, 1, '2015-02-16', '2015-02-16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_schoolsubjects`
--

DROP TABLE IF EXISTS `pfoms_schoolsubjects`;
CREATE TABLE IF NOT EXISTS `pfoms_schoolsubjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `pfoms_schoolsubjects`
--

INSERT INTO `pfoms_schoolsubjects` (`id`, `name`, `code`) VALUES
(1, 'English', ''),
(2, 'Mathematics', ''),
(3, 'Science', ''),
(4, 'Filipino', ''),
(5, 'Makabayan', ''),
(6, 'EPP', ''),
(7, 'Hekasi', ''),
(8, 'MSEP', ''),
(9, 'Value Education', '');

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_schoolyears`
--

DROP TABLE IF EXISTS `pfoms_schoolyears`;
CREATE TABLE IF NOT EXISTS `pfoms_schoolyears` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sy_from` varchar(4) NOT NULL,
  `sy_to` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pfoms_schoolyears`
--

INSERT INTO `pfoms_schoolyears` (`id`, `sy_from`, `sy_to`) VALUES
(1, '2014', '2015'),
(2, '2006', '2007');

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_settings`
--

DROP TABLE IF EXISTS `pfoms_settings`;
CREATE TABLE IF NOT EXISTS `pfoms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` text,
  `address` text,
  `others` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pfoms_settings`
--


-- --------------------------------------------------------

--
-- Table structure for table `pfoms_studentindicators`
--

DROP TABLE IF EXISTS `pfoms_studentindicators`;
CREATE TABLE IF NOT EXISTS `pfoms_studentindicators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pfoms_studentindicators`
--

INSERT INTO `pfoms_studentindicators` (`id`, `name`, `code`, `description`) VALUES
(1, 'Not Applicable', 'N/A', ''),
(2, 'Transferred Out', 'T/O', 'Name of public (P) private (PR) School & Effectivity Date'),
(3, 'Transferred In', 'T/I', 'Name of public (P) Private ( PR) school and effectivity date');

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_students`
--

DROP TABLE IF EXISTS `pfoms_students`;
CREATE TABLE IF NOT EXISTS `pfoms_students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lrn` varchar(10) DEFAULT NULL,
  `firstname` varchar(35) DEFAULT NULL,
  `middlename` varchar(35) DEFAULT NULL,
  `lastname` varchar(35) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthplace` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `mother_tongue` varchar(10) DEFAULT NULL,
  `ethnic_group` varchar(10) DEFAULT NULL,
  `religion` varchar(25) DEFAULT NULL,
  `address_house` varchar(255) DEFAULT NULL,
  `address_brgy` varchar(255) DEFAULT NULL,
  `address_municipal` varchar(255) DEFAULT NULL,
  `address_province` varchar(255) DEFAULT NULL,
  `father_fullname` varchar(50) DEFAULT NULL,
  `mother_fullname` varchar(50) DEFAULT NULL,
  `guardian_fullname` varchar(50) DEFAULT NULL,
  `relationship` varchar(35) DEFAULT NULL,
  `contact_number` varchar(11) DEFAULT NULL,
  `studentindicator_id` int(11) DEFAULT NULL,
  `added` date DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `gradelevel_id` int(11) DEFAULT NULL,
  `gradesection_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `schoolyear_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `pfoms_students`
--

INSERT INTO `pfoms_students` (`id`, `lrn`, `firstname`, `middlename`, `lastname`, `gender`, `birthdate`, `birthplace`, `age`, `mother_tongue`, `ethnic_group`, `religion`, `address_house`, `address_brgy`, `address_municipal`, `address_province`, `father_fullname`, `mother_fullname`, `guardian_fullname`, `relationship`, `contact_number`, `studentindicator_id`, `added`, `added_by`, `modified`, `modified_by`, `gradelevel_id`, `gradesection_id`, `teacher_id`, `schoolyear_id`) VALUES
(1, '1234', 'Jerel', 'C', 'Basigsig', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(2, '1234', 'Tony Ven', 'v', 'Calamba', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(3, '1234', 'Christian', 'E.', 'Castillo', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(4, '1234', 'Glenn Mark', 'S.', 'Dionson', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(5, '1234', 'Johnryl', 'S.', 'Dumaguite', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(6, '1234', 'Anthony', 'L.', 'Ellazo', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(7, '1234', 'Gilbert', 'A.', 'Estrada', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(8, '1234', 'Dendo', 'S.', 'Garnace', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(9, '1234', 'Jeremiah', 'M.', 'Habana', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(10, '1234', 'Romy\n', 'Y.', 'Lubiano', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(11, '1234', 'Jay', 'T.', 'Paver', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(12, '1234', 'Paulo Jasper', 'B.', 'Sintapo', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(13, '1234', 'Jake', 'D.', 'Victoriano', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(14, '1234', 'Randy Jose', 'A.', 'Virtudazo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(15, '1234', 'Charelyn', 'D.', 'Arcones', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(16, '1234', 'Rennie Mae', 'D.', 'Artana', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(17, '1234', 'April Jane', 'A.', 'Bustillo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(18, '1234', 'Mary Joy', 'B.', 'Caracol', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(19, '1234', 'Rosemarie', 'A.', 'Jayoma', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(20, '1234', 'Elene Kien', 'S.', 'Layupan', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(21, '1234', 'Jecel Mae', 'H.', 'Masancay', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(22, '1234', 'Mary-An', 'P.', 'Morales', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(23, '1234', 'Jhazer', 'L.', 'Perante', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(24, '1234', 'Melodie', 'O.', 'Sentapo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(25, '1234', 'Jessa', 'W.', 'Verbo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(26, '1234', 'Charlotte', 'J.', 'Yakit', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 1),
(27, '1234', 'Jerel', 'C', 'Basigsig', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(28, '1234', 'Tony Ven', 'v', 'Calamba', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(29, '1234', 'Christian', 'E.', 'Castillo', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(30, '1234', 'Glenn Mark', 'S.', 'Dionson', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(31, '1234', 'Johnryl', 'S.', 'Dumaguite', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(32, '1234', 'Anthony', 'L.', 'Ellazo', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(33, '1234', 'Gilbert', 'A.', 'Estrada', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(34, '1234', 'Dendo', 'S.', 'Garnace', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(35, '1234', 'Jeremiah', 'M.', 'Habana', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(36, '1234', 'Romy\n', 'Y.', 'Lubiano', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(37, '1234', 'Jay', 'T.', 'Paver', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(38, '1234', 'Paulo Jasper', 'B.', 'Sintapo', 'F', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(39, '1234', 'Jake', 'D.', 'Victoriano', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(40, '1234', 'Randy Jose', 'A.', 'Virtudazo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(41, '1234', 'Charelyn', 'D.', 'Arcones', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(42, '1234', 'Rennie Mae', 'D.', 'Artana', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(43, '1234', 'April Jane', 'A.', 'Bustillo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(44, '1234', 'Mary Joy', 'B.', 'Caracol', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(45, '1234', 'Rosemarie', 'A.', 'Jayoma', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(46, '1234', 'Elene Kien', 'S.', 'Layupan', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(47, '1234', 'Jecel Mae', 'H.', 'Masancay', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(48, '1234', 'Mary-An', 'P.', 'Morales', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(49, '1234', 'Jhazer', 'L.', 'Perante', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(50, '1234', 'Melodie', 'O.', 'Sentapo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(51, '1234', 'Jessa', 'W.', 'Verbo', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2),
(52, '1234', 'Charlotte', 'J.', 'Yakit', 'M', '1996-02-10', '', 0, 'b', '123', 'catholic', '2', 'mangagoy', 'bislic city', NULL, 'dulfo', 'dilfa', NULL, NULL, '123', 1, '2015-02-15', 1, '2015-02-15', 0, 2, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_teachers`
--

DROP TABLE IF EXISTS `pfoms_teachers`;
CREATE TABLE IF NOT EXISTS `pfoms_teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gradelevel_id` int(11) NOT NULL,
  `gradesection_id` int(11) NOT NULL,
  `firstname` varchar(35) NOT NULL,
  `middlename` varchar(35) NOT NULL,
  `lastname` varchar(35) NOT NULL,
  `title` varchar(5) NOT NULL,
  `others` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pfoms_teachers`
--

INSERT INTO `pfoms_teachers` (`id`, `gradelevel_id`, `gradesection_id`, `firstname`, `middlename`, `lastname`, `title`, `others`) VALUES
(1, 4, 2, 'Test', 'test', 'test', 'Mr', 'test'),
(2, 4, 2, 'testtest', 'testestest', 'testset', 'Mrs', 'testest'),
(3, 4, 2, 'testtes', 'testsetes', 'Testset', 'Miss', 'stests');

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_teachersubjects`
--

DROP TABLE IF EXISTS `pfoms_teachersubjects`;
CREATE TABLE IF NOT EXISTS `pfoms_teachersubjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `schoolsubject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `pfoms_teachersubjects`
--

INSERT INTO `pfoms_teachersubjects` (`id`, `teacher_id`, `schoolsubject_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 6),
(4, 2, 3),
(5, 2, 2),
(6, 3, 2),
(7, 3, 4),
(8, 3, 3),
(9, 3, 3),
(10, 3, 5),
(11, 3, 7),
(12, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pfoms_users`
--

DROP TABLE IF EXISTS `pfoms_users`;
CREATE TABLE IF NOT EXISTS `pfoms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `firstname` varchar(35) DEFAULT NULL,
  `middlename` varchar(35) DEFAULT NULL,
  `lastname` varchar(35) DEFAULT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(35) DEFAULT NULL,
  `contact` varchar(11) DEFAULT NULL,
  `lastlogin` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pfoms_users`
--

INSERT INTO `pfoms_users` (`id`, `group_id`, `firstname`, `middlename`, `lastname`, `username`, `password`, `email`, `contact`, `lastlogin`) VALUES
(1, 0, 'administrator', '', '', 'admin', '40a96ddeb651371e2bbf291f789a86becf5ba303', 'sampl@email.com', '00000000000', '2015-02-14');
