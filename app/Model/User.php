<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * User Model
 *
 * @property Schoolgrade $Schoolgrade
 */
class User extends AppModel {
	
	public $virtualFields = array('full_name' => 'CONCAT(User.firstname, " " ,User.lastname)');
	public $displayField = 'full_name';
	
	public $actsAs = array('Acl' => array('type' => 'requester'));
	
	public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }
	
	public function transformtext($data){
		return strtolower(Inflector::slug($data, ''));
	}
	
	public function beforeSave($options=array()){		
		if(!empty($this->data['User']['password'])){
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		}
		
		if(!empty($this->data['User']['firstname']) && !empty($this->data['User']['lastname'])){					
			$this->data['User']['firstname'] = ucwords(strtolower(trim($this->data['User']['firstname'])));		
			$this->data['User']['lastname'] = ucwords(strtolower(trim($this->data['User']['lastname'])));							
		}
		
		if(!empty($this->data['User']['username'])){
			$this->data['User']['username'] = $this->transformtext($this->data['User']['username']);
		}
		
		if(!empty($this->data['User']['username']) && !empty($this->data['User']['password'])){
			$this->data['User']['added'] = date('Y-m-d');
		}
		
		return true;
	}
	
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Schoolgrade' => array(
			'className' => 'Schoolgrade',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
