<?php
App::uses('AppModel', 'Model');
/**
 * Student Model
 *
 * @property Studentindicator $Studentindicator
 * @property Gradelevel $Gradelevel
 * @property Schoolgrade $Schoolgrade
 */
class Student extends AppModel {
	
	
	
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
	public $validate = array(
		'lrn'	=> array(
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message'	=> 'LRN already taken.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),	
		),		
		'firstname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),			
		),
		'lastname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			
		),
		'middlename' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			
		),
		'birthdate' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			
		),
	);
		

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Studentindicator' => array(
			'className' => 'Studentindicator',
			'foreignKey' => 'studentindicator_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradelevel' => array(
			'className' => 'Gradelevel',
			'foreignKey' => 'gradelevel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradesection' => array(
			'className' => 'Gradesection',
			'foreignKey' => 'gradesection_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Teacher' => array(
			'className' => 'Teacher',
			'foreignKey' => 'teacher_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Schoolyear' => array(
			'className' => 'Schoolyear',
			'foreignKey' => 'schoolyear_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Schoolgrade' => array(
			'className' => 'Schoolgrade',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
