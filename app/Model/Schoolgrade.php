<?php
App::uses('AppModel', 'Model');
/**
 * Schoolgrade Model
 *
 * @property Student $Student
 * @property Schoolsubject $Schoolsubject
 * @property User $User
 */
class Schoolgrade extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'student_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Teacher' => array(
			'className' => 'Teacher',
			'foreignKey' => 'teacher_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradelevel' => array(
			'className' => 'Gradelevel',
			'foreignKey' => 'gradelevel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradesection' => array(
			'className' => 'Gradesection',
			'foreignKey' => 'gradesection_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Schoolsubject' => array(
			'className' => 'Schoolsubject',
			'foreignKey' => 'schoolsubject_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Schoolyear' => array(
			'className' => 'Schoolyear',
			'foreignKey' => 'schoolyear_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
