<?php
App::uses('AppModel', 'Model');
/**
 * Gradelevel Model
 *
 * @property Student $Student
 */
class Gradelevel extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

		public $validate = array(
			'name'	=> array(
				'notEmpty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Data already exists',
					//'allowEmpty' => false,
					//'required' => false,
					//'last' => false, // Stop validation after this rule
					//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),	
				'isUnique' => array(
					'rule' => array('isUnique'),
					'message' => 'Data already exists',
					//'allowEmpty' => false,
					//'required' => false,
					//'last' => false, // Stop validation after this rule
					//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),	
			)		
	);
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'gradelevel_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Teacher' => array(
			'className' => 'Teacher',
			'foreignKey' => 'gradelevel_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Gradesection' => array(
			'className' => 'Gradesection',
			'foreignKey' => 'gradelevel_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Schoolgrade' => array(
			'className' => 'Schoolgrade',
			'foreignKey' => 'gradelevel_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}
