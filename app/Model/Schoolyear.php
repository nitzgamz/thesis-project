<?php
App::uses('AppModel', 'Model');
/**
 * Schoolyear Model
 *
 */
class Schoolyear extends AppModel {
	
	public $virtualFields = array('name' => 'CONCAT(Schoolyear.sy_from, " - " ,Schoolyear.sy_to)');
	public $displayField = 'name';
	
	public function beforeSave($options=array()){
		parent::beforeSave();
		
		$this->data['Schoolyear']['sy_from'] 	= $this->data['Schoolyear']['sy_from'];
		$this->data['Schoolyear']['sy_to'] 		= $this->data['Schoolyear']['sy_from'] + 1;
		
		return true;
		
	}
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'sy_from' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Data already exists',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'minLength'	=> array(
				'rule'	=> array('minlength', 4),
				'message'	=> 'invalid year format'
			),
			'maxLength'	=> array(
				'rule'	=> array('maxlength', 4),
				'message'	=> 'invalid year format'
			)
		),
		/*
		'sy_to' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
	);
	
	public $hasMany = array(
		'Schoolgrade' => array(
			'className' => 'Schoolgrade',
			'foreignKey' => 'schoolyear_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'schoolyear_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
}
