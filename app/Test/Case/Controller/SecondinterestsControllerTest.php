<?php
App::uses('SecondinterestsController', 'Controller');

/**
 * SecondinterestsController Test Case
 *
 */
class SecondinterestsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.secondinterest',
		'app.loan',
		'app.firstinterest',
		'app.loanjoinedpayment',
		'app.loantablepayment'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
