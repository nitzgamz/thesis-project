<?php
App::uses('Firstinterest', 'Model');

/**
 * Firstinterest Test Case
 *
 */
class FirstinterestTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.firstinterest',
		'app.loan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Firstinterest = ClassRegistry::init('Firstinterest');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Firstinterest);

		parent::tearDown();
	}

}
