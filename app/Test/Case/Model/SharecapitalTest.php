<?php
App::uses('Sharecapital', 'Model');

/**
 * Sharecapital Test Case
 *
 */
class SharecapitalTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sharecapital'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sharecapital = ClassRegistry::init('Sharecapital');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sharecapital);

		parent::tearDown();
	}

}
