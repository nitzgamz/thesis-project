<?php
/* Studentindicator Test cases generated on: 2015-02-14 18:03:49 : 1423933429*/
App::uses('Studentindicator', 'Model');

/**
 * Studentindicator Test Case
 *
 */
class StudentindicatorTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.studentindicator', 'app.student');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Studentindicator = ClassRegistry::init('Studentindicator');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Studentindicator);

		parent::tearDown();
	}

}
