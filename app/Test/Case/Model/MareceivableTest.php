<?php
App::uses('Mareceivable', 'Model');

/**
 * Mareceivable Test Case
 *
 */
class MareceivableTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mareceivable'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mareceivable = ClassRegistry::init('Mareceivable');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mareceivable);

		parent::tearDown();
	}

}
