<?php
/* Gradelevel Test cases generated on: 2015-02-14 18:03:18 : 1423933398*/
App::uses('Gradelevel', 'Model');

/**
 * Gradelevel Test Case
 *
 */
class GradelevelTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.gradelevel', 'app.student');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Gradelevel = ClassRegistry::init('Gradelevel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Gradelevel);

		parent::tearDown();
	}

}
