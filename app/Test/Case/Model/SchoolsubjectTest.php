<?php
/* Schoolsubject Test cases generated on: 2015-02-14 18:03:38 : 1423933418*/
App::uses('Schoolsubject', 'Model');

/**
 * Schoolsubject Test Case
 *
 */
class SchoolsubjectTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.schoolsubject', 'app.schoolgrade', 'app.student', 'app.user');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Schoolsubject = ClassRegistry::init('Schoolsubject');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Schoolsubject);

		parent::tearDown();
	}

}
