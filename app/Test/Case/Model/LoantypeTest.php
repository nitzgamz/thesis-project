<?php
App::uses('Loantype', 'Model');

/**
 * Loantype Test Case
 *
 */
class LoantypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.loantype',
		'app.op'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Loantype = ClassRegistry::init('Loantype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Loantype);

		parent::tearDown();
	}

}
