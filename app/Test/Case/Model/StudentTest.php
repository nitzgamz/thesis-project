<?php
/* Student Test cases generated on: 2015-02-14 18:03:54 : 1423933434*/
App::uses('Student', 'Model');

/**
 * Student Test Case
 *
 */
class StudentTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.student', 'app.studentindicator', 'app.gradelevel', 'app.schoolgrade', 'app.schoolsubject', 'app.user');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Student = ClassRegistry::init('Student');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Student);

		parent::tearDown();
	}

}
