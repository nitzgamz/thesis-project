<?php
/* Schoolgrade Test cases generated on: 2015-02-14 18:03:31 : 1423933411*/
App::uses('Schoolgrade', 'Model');

/**
 * Schoolgrade Test Case
 *
 */
class SchoolgradeTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.schoolgrade', 'app.student', 'app.schoolsubject', 'app.user');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Schoolgrade = ClassRegistry::init('Schoolgrade');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Schoolgrade);

		parent::tearDown();
	}

}
