<?php
/**
 * CareceivableFixture
 *
 */
class CareceivableFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'jrnref_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 35, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'accnt_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'debit' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'credit' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'sl_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 35, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'jrnref_no' => 'Lorem ipsum dolor sit amet',
			'accnt_code' => 'Lorem ipsum d',
			'debit' => 1,
			'credit' => 1,
			'sl_code' => 'Lorem ipsum dolor sit amet'
		),
	);

}
