<?php
/**
 * LoantypeFixture
 *
 */
class LoantypeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'loan_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'loan_desc' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'int_cd' => array('type' => 'integer', 'null' => false, 'default' => null),
		'fines_cd' => array('type' => 'integer', 'null' => false, 'default' => null),
		'op_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'term' => array('type' => 'integer', 'null' => false, 'default' => null),
		'int_rate' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 18, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'installment_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'service_fee' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'add_on_flag' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'add_on_pert' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'int_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'class_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'req_crdt_chk' => array('type' => 'integer', 'null' => false, 'default' => null),
		'prepaid_int' => array('type' => 'integer', 'null' => false, 'default' => null),
		'add_on_sva' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'int_factor' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'loan_purpose' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'srv_cd' => array('type' => 'integer', 'null' => false, 'default' => null),
		'no_of_payments' => array('type' => 'integer', 'null' => false, 'default' => null),
		'use_sbracket' => array('type' => 'integer', 'null' => false, 'default' => null),
		'use_ibracket' => array('type' => 'integer', 'null' => false, 'default' => null),
		'use_intfactbrkt' => array('type' => 'integer', 'null' => false, 'default' => null),
		'dday_colmn' => array('type' => 'integer', 'null' => false, 'default' => null),
		'to_par' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'loan_type' => 'Lorem ips',
			'loan_desc' => 'Lorem ipsum dolor sit amet',
			'int_cd' => 1,
			'fines_cd' => 1,
			'op_id' => 'Lorem ips',
			'term' => 1,
			'int_rate' => 'Lorem ipsum dolo',
			'installment_type' => 'Lorem ips',
			'service_fee' => 'Lorem ips',
			'add_on_flag' => '',
			'add_on_pert' => 'Lorem ips',
			'int_type' => '',
			'class_code' => 'Lorem ips',
			'req_crdt_chk' => 1,
			'prepaid_int' => 1,
			'add_on_sva' => 'Lorem ips',
			'int_factor' => 'Lorem ips',
			'loan_purpose' => 'Lorem ipsum dolor sit amet',
			'srv_cd' => 1,
			'no_of_payments' => 1,
			'use_sbracket' => 1,
			'use_ibracket' => 1,
			'use_intfactbrkt' => 1,
			'dday_colmn' => 1,
			'to_par' => 1
		),
	);

}
