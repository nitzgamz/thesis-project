<?php
/**
 * SharecapitalFixture
 *
 */
class SharecapitalFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'member_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'deposit_type' => array('type' => 'integer', 'null' => false, 'default' => null),
		'tran_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'tran_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 18, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'amount' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'member_no' => 'Lorem ipsum d',
			'deposit_type' => 1,
			'tran_date' => '2014-02-27 18:45:59',
			'tran_no' => 'Lorem ipsum dolo',
			'amount' => 1
		),
	);

}
