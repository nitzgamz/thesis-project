<?php
/**
 * LoantablepaymentFixture
 *
 */
class LoantablepaymentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'loan_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'tran_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'tran_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 18, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'prin_payment' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'int_payment' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'loan_id' => 1,
			'tran_date' => '2014-02-27 18:44:35',
			'tran_no' => 'Lorem ipsum dolo',
			'prin_payment' => 1,
			'int_payment' => 1
		),
	);

}
