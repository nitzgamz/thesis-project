<?php
/**
 * DepositFixture
 *
 */
class DepositFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'member_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'deposit_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'balance' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'withdrawable' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'last_trans_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'member_no' => 'Lorem ipsum dolor sit a',
			'deposit_type' => 'Lorem ip',
			'balance' => 1,
			'withdrawable' => 1,
			'last_trans_date' => '2014-02-27 18:43:57'
		),
	);

}
