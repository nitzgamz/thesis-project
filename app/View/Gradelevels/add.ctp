<?php echo $this->element('submenu/subjects'); ?>
<h3 class="default"><?php echo __('Register Grade Level'); ?></h3>
<div class="gradelevels form">
<?php echo $this->Form->create('Gradelevel');?>
	<fieldset>
	
	<?php
		echo '<label for="">Grade Name</label>';
		echo $this->Form->input('name', array('label' => ''));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
</div>
<br />
