<div class="gradelevels index">
	<?php echo $this->element('submenu/subjects'); ?>
	<h3 class="default"><?php echo __('Grade levels');?></h3>	
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th class=""><?php echo __('No. of Student');?></th>
			<th class=""><?php echo __('Sections');?></th>
			<th class=""><?php echo __('Teachers');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($gradelevels as $gradelevel): ?>
	<tr>		
		<td><?php echo h($gradelevel['Gradelevel']['name']); ?>&nbsp;</td>
		<td>
			<?php 
				$students = $this->requestAction(array('controller' => 'students', 'action' => 'getallstudents', $gradelevel['Gradelevel']['id']));
				if(!empty($students)){
					echo count($students);
				}else{
					echo '0';
				}
			 ?>
		</td>
		<td>
			<?php 
				$sections = $this->requestAction(array('controller' => 'gradesections', 'action' => 'getallsections', $gradelevel['Gradelevel']['id']));
				if(!empty($sections)){
					echo count($sections);
				}else{
					echo '0';
				}
			 ?>
		</td>
		<td>
			<?php 
				$teachers = $this->requestAction(array('controller' => 'teachers', 'action' => 'getallteachers', $gradelevel['Gradelevel']['id']));
				if(!empty($teachers)){
					echo '<ul>';
					foreach($teachers as $teacher):
						echo '<li>'.$teacher['Teacher']['title'] .' '. $teacher['Teacher']['firstname'] .' '.$teacher['Teacher']['lastname'].'</li>';
					endforeach;
					echo '</ul>';
				}else{
					echo '0';
				}
			 ?>
		</td>
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $gradelevel['Gradelevel']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $gradelevel['Gradelevel']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $gradelevel['Gradelevel']['id']), null, __('Are you sure you want to delete # %s?', $gradelevel['Gradelevel']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>

