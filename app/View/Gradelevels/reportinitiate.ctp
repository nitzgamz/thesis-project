<?php echo $this->element('submenu/student'); ?>
<div class="col-md-5 col-lg-5 col-xs-5 " style="background: #ccc; border: 1px solid #ccc; border-radius: 10px 10px; margin-top: 30px;">	
<h3 class="default">School Form 6 ( SF 6 ) Summarized Report on Promotion & Level of Proficiency</h3>
<div class="students form">
<?php echo $this->Form->create('Gradelevel');?>
	<?php
		echo '<label for="">School Year</label>';
		echo $this->Form->input('schoolyear_id', array('label' => ''));
		echo '</div>';
	?>
	<br />
	<div class="col-md-4 col-lg-4 col-xs-4 no-padding-left">
		<?php echo $this->Form->submit(__('Generate PDF Report'), array('class' => 'btn btn-success'));?>
	</div>	
	<?php 
		if($download){ 
			echo '<div class="col-md-4 col-lg-4 col-xs-4">';
				echo $this->Html->link('Download / View File', array('action' => 'report', 'ext' => 'pdf', $this->data['Gradelevel']['schoolyear_id']), array('class' => 'btn btn-danger', 'target' => '_blank'));
			echo '</div>';
		}
	?>
	<div class="clear"></div>
	<br /><br />
</div>
</div>
<div class="clear"></div>
<br />