<div class="processes index">
	<table cellpadding="0" cellspacing="0">
	<tr class="table-header">			
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('filename'); ?></th>
			<th><?php echo $this->Paginator->sort('trigger_time'); ?></th>
			<th><?php echo $this->Paginator->sort('import'); ?></th>
			<th>Last Import</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($processes as $process): ?>
	<tr class="table-information2">		
		<td>
			<?php 
				$font = 'green';
				switch($process['Process']['name']){
					case "Members": $font = 'red'; break;
					case "LoanTypes": $font = 'red'; break;
					case "List of Loans": $font = 'blue'; break;
					case "LoanPaymentsJoined": $font = 'blue'; break;
					case "Savings": $font = 'blue'; break;
					case "ShareCapital": $font = 'blue'; break;
					case "MAReceivable": $font = 'blue'; break;
					case "CA Receivable": $font = 'blue'; break;
					case "Pangkabuhayan": $font = 'blue'; break;
					case "Interest1": $font = 'blue'; break;
					case "Interest2": $font = 'blue'; break;
					default:						
					break;
				}
				echo '<font style="color: '.$font.'">'.strtoupper($process['Process']['name']).'</font>'; 
				//echo '<div style="font-size: 9px;">'.$process['Process']['controller_page'].'</div>';
				//echo '<div style="font-size: 9px;">'.$process['Process']['model_page'].'</div>';
			?>
		</td>
		<td>
			<?php echo h($process['Process']['filename']); ?>
		</td>
		<td><?php echo date('h:i a', strtotime($process['Process']['trigger_time'])); ?></td>
		<td style="text-align: center;"><?php echo $process['Process']['import']; ?>&nbsp;</td>
		<td><?php 
			if($process['Process']['import_date'] > 0){
				echo date('l M d, Y', strtotime($process['Process']['import_date'])); 
			}
		?></td>
		<td style="width: 140px;">
			<div class="btn-group">
			<?php //if(!empty($process['Process']['controller_page'])){ ?>
				<?php //echo $this->Html->link('Import', array('controller' => $process['Process']['controller_page'], 'action' => 'add', $process['Process']['id']), array('class' => 'view thickbox')); ?>				
				<div class="btn btn-default"><?php echo $this->Html->link('Import Data', array('controller' => 'processes', 'action' => 'index')); ?></div>							
			<?php //}else{ ?>
				<?php //echo $this->Html->link('Import', array(''), array('class' => 'view disable')); ?>
			<?php //} ?>
			<?php //echo $this->Html->link('Edit', array('action' => 'edit', $process['Process']['id']), array('class' => 'edit')); ?>		
				<div class="btn btn-default"><?php echo $this->Html->link('Edit', array('action' => 'edit', $process['Process']['id'])); ?></div>
			</div>
		</td>
	</tr>
<?php endforeach; ?>
	<?php echo $this->element('table/table-footer', array('navigation' => true)); ?>
</div>

