<?php
	if(file_exists($filepath . $process['Process']['filename'])){
		echo '<div class="success_message" style="text-align: center">';
			echo 'File Location<br /> '.$filepath . $process['Process']['filename'];
		echo '</div>';
		echo '<div>';
			echo '<div class="top-text-header">'.__('File to process').'</div>';
			echo '<h1>'.$process['Process']['name'].'</h1>';
			echo $this->Form->create('Process', array('id' => 'processform'));			
			echo $this->Form->input('filetoprocess', array('type' => 'hidden', 'default' => $process['Process']['filename']));
			echo $this->Form->input('selectedfile', array('type' => 'hidden', 'default' => $process['Process']['name']));
			echo '<div class="end-form">';
				echo $this->Form->submit('Begin Import', array('class' => 'submitbtn', 'title' => 'Import '.$process['Process']['name']));
			echo '</div>';			
		echo '</div>';
	}else{
		echo '<div class="error" style="text-align: center">';
			echo 'File is missing, please inform the programmer';
		echo '</div>';
	}
?>
<script type="text/javascript">
$(document).ready( function(){
		$('#processform').submit(function(){ 
			$(".end-form").html('<div class="process_message"><img src="../../webroot/img/loading.gif" /><br />Processing data... This will take a moment, please wait...</div>');
		});
});
</script>
