<div class="processes form" style="width: 50%;">
<?php echo $this->Form->create('Process'); ?>
	<?php
		/*echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('filename');
		echo $this->Form->input('trigger_time');
		echo $this->Form->input('controller_page');
		echo $this->Form->input('model_page');
		echo $this->Form->input('priority_level');
		echo $this->Form->input('order_no');
		echo $this->Form->input('include_in_journal');*/
		echo $this->Form->input('id');
	?>
	<div class="input-group before-input">
		<span class="input-group-addon label-text">Process Name</span>
		<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => '')); ?>
	</div>
	<div class="input-group before-input">	
		<span class="input-group-addon label-text">Filename</span>
		<?php echo $this->Form->input('filename', array('class' => 'form-control', 'label' => '')); ?>		
	</div>
	<div class="input-group before-input">	
		<span class="input-group-addon label-text">Import Time</span>
		<?php echo $this->Form->input('trigger_time', array('class' => 'trigger-time', 'label' => '')); ?>		
	</div>
	<div class="input-group before-input">	
		<span class="input-group-addon label-text">Control Page</span>
		<?php echo $this->Form->input('controller_page', array('class' => 'form-control', 'label' => '')); ?>		
	</div>
	<div class="input-group before-input">	
		<span class="input-group-addon label-text">Model Page</span>
		<?php echo $this->Form->input('model_page', array('class' => 'form-control', 'label' => '')); ?>		
	</div>
	<div class="input-group before-input">	
		<span class="input-group-addon label-text">Priority Level</span>
		<?php echo $this->Form->input('priority_level', array('class' => 'form-control', 'label' => '')); ?>		
	</div>
	<div class="input-group before-input">	
		<span class="input-group-addon label-text">Order No.</span>
		<?php echo $this->Form->input('order_no', array('class' => 'form-control', 'label' => '')); ?>		
	</div>
	<div class="input-group before-input">	
		<span class="input-group-addon label-text">Include in the Journal</span>
		<?php echo $this->Form->input('include_in_journal', array('class' => 'form-control', 'label' => '')); ?>		
	</div>
	<?php echo $this->Form->submit(__('Update & Continue'), array('class' => 'submitbtn')); ?>			
		
	
</div>