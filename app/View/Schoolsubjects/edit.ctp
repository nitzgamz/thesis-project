<?php echo $this->element('submenu/subjects'); ?>
<div class="schoolsubjects form">
<?php echo $this->Form->create('Schoolsubject');?>
	<fieldset>
		<legend><?php echo __('Update Subject'); ?></legend>
	<?php
		echo '<label for="">Name</label>';
		echo $this->Form->input('id', array('label' => ''));
		echo $this->Form->input('name', array('label' => ''));
		echo '<label for="">Code</label>';
		echo $this->Form->input('code', array('label' => ''));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Update'), array('class' => 'btn btn-primary'));?>
</div>
<br /><br />

