<div class="schoolsubjects index">
	<?php echo $this->element('submenu/teachers'); ?>
	<h3 class="default"><?php echo __('Subjects');?></h3>	
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th><?php echo $this->Paginator->sort('name');?></th>			
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($schoolsubjects as $schoolsubject): ?>
	<tr>		
		<td><?php echo h($schoolsubject['Schoolsubject']['name']); ?>&nbsp;</td>		
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $schoolsubject['Schoolsubject']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $schoolsubject['Schoolsubject']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $schoolsubject['Schoolsubject']['id']), null, __('Are you sure you want to delete # %s?', $schoolsubject['Schoolsubject']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
<?php echo $this->element('bottom.navigation'); ?>
</div>
