<?php
	$user = $this->requestAction('users/loggeduser');
	echo $this->element('navigation/page.crumbs', array(
		'text' => array('Users', 'View Profile'),
		'pages' => array('/users', '/users/myprofile/'.$user['User']['id'])
	));	
?>

<div class="users view">
	<h2><?php  echo __('Account Profile');?></h2>	
	<div class="below-table-link" style="text-align: left;">
				<?php echo $this->Html->link('Update Profile', array('controller' => 'users', 'action' => 'updatemyprofile', $users['User']['id'])); ?>				
				<?php echo $this->Html->link('Change Password', array('action' => 'changepassword')); ?>
			</div>	
	<div class="clear"></div>
	<div style="padding: 10px 0;">
		<div class="applicant-image">
		</div>		
		<div class="applicant-info" style="font: bold 12px arial, heveltica, sans-serif; padding: 10px 0;">
			<h3 style="font-size: 15px;"><?php echo strtoupper($users['User']['firstname'].' '.$users['User']['lastname']); ?></h3>
			<div><?php echo $users['Group']['name']; ?></div>
			<div><?php echo __('Last Access : ').' '.date('l F d, Y', strtotime($users['User']['lastaccess'])); ?></div>
			<div><?php echo $users['User']['contact']; ?></div>
			<div><?php echo $users['User']['email']; ?></div>
		</div>
	</div>
	<div class="clear"></div>
	<h2><?php echo __('Account Details'); ?></h2>
	<table>
		<tr class="table-header">
			<?php foreach(array('Account','Department',  'Firstname', 'Lastname', 'Last IP Used', 'Last Access') as $header): ?>
				<th><?php echo $header; ?></th>
			<?php endforeach; ?>
		</tr>
		<?php if(!empty($users)){ ?>
		<tr class="table-information">
			<td><?php echo $users['Group']['name']; ?></td>			
			<td><?php echo $users['Department']['name']; ?></td>
			<td><?php echo $users['User']['firstname']; ?></td>
			<td><?php echo $users['User']['lastname']; ?></td>
			<td><?php echo $users['User']['lastip']; ?></td>
			<td><?php echo date('d-M-Y h:ia', strtotime($users['User']['lastaccess'])); ?></td>			
		</tr>
		<?php } ?>
	</table>	
	
				
</div>

