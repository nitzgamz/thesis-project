<?php
	$user = $this->requestAction('users/loggeduser');
	echo $this->element('navigation/page.crumbs', array(
		'text' => array('Users', 'Update Account'),
		'pages' => array('/users', '/users/updatemyprofile/'.$userid)
	));	
?>

<?php 
	echo $this->Element('Forms/add.form', 
				array(
					'labels' => array(	
						NULL,
						'Firstname',
						'Middlename / M.I.',
						'Lastname',						
						'Username',						
						'Email',
						'Email Password',
						'Contact Number',
						
							
					),
					'inputs' => array(
						'id',						
						'firstname',
						'middlename',
						'lastname',
						'username',											
						'email',
						'emailpwd',
						'contact'
						
					),		
					
					'hints' => array(),
					'model' => 'User',
					'formtitle' => 'Update Account',
					'controller' => 'users',
				)
		);
?>

