<?php
	$user = $this->requestAction('users/loggeduser');
	echo $this->element('navigation/page.crumbs', array(
		'text' => array('Users', 'Change Password'),
		'pages' => array('/users', '/users/changepassword/'.$userid)
	));	
?>

<div class="groups-form">
<div class="form-title"><h2>Change Password</h2></div>
<table cellpadding="0" cellscpacing="0" style="border: none;">
<?php 
	echo $this->Form->create('Changepassword', array('onsubmit' => 'return confirm("The system is about to process a certain data, are you sure you want to continue?");'));	
		echo '<tr class="table-form-label">';
			echo '<td class="text-label">';
				echo '<div class="form-label">'.__('Current Password').'</div>';
			echo '</td>';
			echo '<td class="text-input">';
				echo $this->Form->input('current', array('label' => '', 'class' => 'normalinput', 'type' => 'password'));
			echo '</td>';
		echo '</tr>';
		echo '<tr class="table-form-label">';
			echo '<td class="text-label">';
				echo '<div class="form-label">'.__('New Password').'</div>';
			echo '</td>';
			echo '<td class="text-input">';
				echo $this->Form->input('newpassword', array('label' => '', 'class' => 'normalinput', 'type' => 'password'));
			echo '</td>';
		echo '</tr>';
		echo '<tr class="table-form-label">';
			echo '<td class="text-label">';
				echo '<div class="form-label">'.__('Confirm Password').'</div>';
			echo '</td>';
			echo '<td class="text-input">';
				echo $this->Form->input('confirmnew', array('label' => '', 'class' => 'normalinput', 'type' => 'password'));
			echo '</td>';
		echo '</tr>';			
?>
</table>
<div class="end-form">
<?php
	echo $this->Form->submit('Submit', array('class' => 'submitbtn', 'title' => 'Submit Information'));
	echo $this->Form->button('Cancel', array('type' => 'Reset', 'class' => 'cancelbtn', 'title' => 'Cancel Information'));
	echo '<div class="clear"></div>';
?>
</div>