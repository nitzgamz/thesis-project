<div class="users view">
<h2><?php  echo __('User');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Firstname'); ?></dt>
		<dd>
			<?php echo h($user['User']['firstname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Middlename'); ?></dt>
		<dd>
			<?php echo h($user['User']['middlename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lastname'); ?></dt>
		<dd>
			<?php echo h($user['User']['lastname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact'); ?></dt>
		<dd>
			<?php echo h($user['User']['contact']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lastlogin'); ?></dt>
		<dd>
			<?php echo h($user['User']['lastlogin']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolgrades'), array('controller' => 'schoolgrades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolgrade'), array('controller' => 'schoolgrades', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Schoolgrades');?></h3>
	<?php if (!empty($user['Schoolgrade'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Student Id'); ?></th>
		<th><?php echo __('Schoolsubject Id'); ?></th>
		<th><?php echo __('First Quarter'); ?></th>
		<th><?php echo __('Second Quarter'); ?></th>
		<th><?php echo __('Third Quarter'); ?></th>
		<th><?php echo __('Fourth Quarter'); ?></th>
		<th><?php echo __('Quarter Tgrade'); ?></th>
		<th><?php echo __('Tgrade'); ?></th>
		<th><?php echo __('Ggrade'); ?></th>
		<th><?php echo __('Sy'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Added'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Schoolgrade'] as $schoolgrade): ?>
		<tr>
			<td><?php echo $schoolgrade['id'];?></td>
			<td><?php echo $schoolgrade['student_id'];?></td>
			<td><?php echo $schoolgrade['schoolsubject_id'];?></td>
			<td><?php echo $schoolgrade['first_quarter'];?></td>
			<td><?php echo $schoolgrade['second_quarter'];?></td>
			<td><?php echo $schoolgrade['third_quarter'];?></td>
			<td><?php echo $schoolgrade['fourth_quarter'];?></td>
			<td><?php echo $schoolgrade['quarter_tgrade'];?></td>
			<td><?php echo $schoolgrade['tgrade'];?></td>
			<td><?php echo $schoolgrade['ggrade'];?></td>
			<td><?php echo $schoolgrade['sy'];?></td>
			<td><?php echo $schoolgrade['user_id'];?></td>
			<td><?php echo $schoolgrade['added'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'schoolgrades', 'action' => 'view', $schoolgrade['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'schoolgrades', 'action' => 'edit', $schoolgrade['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'schoolgrades', 'action' => 'delete', $schoolgrade['id']), null, __('Are you sure you want to delete # %s?', $schoolgrade['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Schoolgrade'), array('controller' => 'schoolgrades', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
