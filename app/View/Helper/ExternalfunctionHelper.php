<?php	
	class ExternalfunctionHelper extends AppHelper {  
		
		public function getbeginningbalance($employee=null, $basedate=null, $loantypeid=null, $requesttype=null){
			$balance = 0;
			$beginningbalance = 0;
			$loan_amount = $this->requestAction('loans/loanbalance/'.$employee.'/'.$basedate.'/'.$loantypeid);			
			
			if(!empty($loan_amount)){			
				//$loan_payments = $this->requestAction('loanjoinedpayments/loanpayments/'.$employee.'/'.$basedate.'/'.$loantypeid.'/'.$loan_amount);
				$beginningbalance = $loan_amount;
			}
			
			switch($requesttype){
				case "beginningbalance":
					return $beginningbalance;
				break;
				case "remainingbalance":
					
				break;
			}
			
		}
		
		public function sakocarepreviousbeginningbalance($employee=null, $basedate=null, $loantypeid=null, $requesttype=null){
			$balance = 0;
			$beginningbalance = 0;
			$loan_amount = $this->requestAction('loans/sakocarepreviousloanbalance/'.$employee.'/'.$basedate.'/'.$loantypeid);			
			
			if(!empty($loan_amount)){							
				$beginningbalance = $loan_amount;
			}
			
			switch($requesttype){
				case "beginningbalance":
					return $beginningbalance;
				break;
				case "remainingbalance":
					
				break;
			}
			
		}
		
		public function getreloans($employee=null, $basedate=null, $loantypeid=null, $requesttype=null){														
					switch($requesttype){
						case "reloan":
							$loanamounts = $this->requestAction('loans/reloanbalance/'.$employee.'/'.$basedate.'/'.$loantypeid);	
							if(!empty($loanamounts)){
								foreach($loanamounts as $key => $reloan):
									echo '<tr class="table-information table-balance-normal">';
										//if($loantype==$payment['Loanjoinedpayment']['loantype_id']){							
										switch($reloan['Loan']['loantype_id']){							
											case 11711.1: $tablegap = 0; break; //motorcycle
											case 11711.2: $tablegap = 1; break; //appliance
											case 11711.3: $tablegap = 2; break; //appliance
											case 11711.4: $tablegap = 3; break; //petty cash
											case 11711.5: $tablegap = 4; break; //special secured loan
											case 11711.8: $tablegap = 5; break; //maxiloan							
											case 11711.9: $tablegap = 6; break; //sako care							
											case 11720: $tablegap = 7; break; //eyeglass							
											default: $tablegap = 0; break;
										}
											echo '<td>'.date('m/d/Y', strtotime($reloan['Loan']['trans_date'])).'</td>';
											echo '<td>'.$reloan['Loan']['voucher'].'</td>';
											echo '<td>&nbsp;</td>';
											echo '<td>&nbsp;</td>';
											echo '<td>&nbsp;</td>';						
											if(!empty($tablegap) || $tablegap <= 0){
												for($i=1; $i<=$tablegap; $i++){
													echo '<td>&nbsp;</td>';
												}
											}
											echo '<td>'.__('').number_format($reloan['Loan']['amount'], 2, '.', ',').'</td>';
										//}
									echo '</tr>';
								endforeach;
							}
						break;
						case "balance":
							$total_reloans = 0;
								$loanamounts = $this->requestAction('loans/remainingbalance/'.$employee.'/'.$basedate.'/'.$loantypeid);	
								if(!empty($loanamounts)){
									foreach($loanamounts as $key => $reloan):
									$total_reloans += $reloan['Loan']['amount'];
									endforeach;
								}
								
							
							return $total_reloans;
						break;
					}					
		}
		
		public function fundbeginningbalance($member=null, $basedate=null, $depositype=null, $requesttype=null){
			$fundbalance = $this->requestAction('savings/beginningbalance/'.$member.'/'.$basedate.'/'.$depositype.'/'.$requesttype);				
			return $fundbalance;			
		}
		
				
		public function fundbeginningbalancefixed($member=null, $basedate=null, $depositype=null, $requesttype=null){
			$fundbalance = $this->requestAction('sharecapitals/beginningbalance/'.$member.'/'.$basedate.'/'.$depositype.'/'.$requesttype);				
			return $fundbalance;			
		}		
		
		public function fundbeginningbalancefixeddeposit($member=null, $depositype=null){
			$fundbalance = $this->requestAction('deposits/beginningbalance/'.$member.'/'.$depositype);				
			return $fundbalance;
		}
		
		public function receivablebeginningbalance($member=null, $basedate=null, $loantype=null){
			$fundbalance = $this->requestAction('mareceivables/beginningbalance/'.$member.'/'.$loantype);	
			//if(!empty($fundbalance)){
				echo __('P' ).number_format($fundbalance, 2, '.', ',');
			//}
		}
		
		
		public function savingstransactions($member=null, $basedate=null, $requestype=null){
			$savings = $this->requestAction('savings/latestpayments/'.$member.'/'.$basedate);				
			$debit = 0;
			$credit = 0;
			
			if(!empty($savings)){
				foreach($savings as $saving):
					echo '<tr class="table-information funds">';													
						echo '<td>'.date('m/d/Y', strtotime($saving['Saving']['tran_date'])).'</td>';
							echo '<td>'.$saving['Saving']['tran_no'].'</td>';
							switch($saving['Saving']['depositype']){							
								case 21100: $tablegap = 0; break; //savings
								case 30150: $tablegap = 1; break; //fixed									
								default: $tablegap = 0; break;
							}								
								if(!empty($tablegap) || $tablegap <= 0){
									for($i=1; $i<=$tablegap; $i++){
										echo '<td>&nbsp;</td>';
									}
								}
							//$credit += $saving['Saving']['credit'];
							//$debit += $saving['Saving']['debit'];
							//$final_amount = $credit - $debit;
							
						echo '<td>'.__('P ');
							if($saving['Saving']['credit'] > 0){
								echo $saving['Saving']['credit'];
							}						
							if($saving['Saving']['debit'] > 0 ){
							echo __('( ');
								echo $saving['Saving']['debit'];
							echo __(' )');
							}						
						echo '</td>';
					echo '</tr>';					
				endforeach;
				echo '<tr class="table-information table-separator"><td colspan="15"></td></tr>';
			}
		}
		
		public function fixedstransactions($member=null, $basedate=null, $requestype=null){
			$savings = $this->requestAction('sharecapitals/latestpayments/'.$member.'/'.$basedate);				
			$debit = 0;
			$credit = 0;
			
			if(!empty($savings)){
				foreach($savings as $saving):
					echo '<tr class="table-information funds">';													
						echo '<td>'.date('m/d/Y', strtotime($saving['Sharecapital']['tran_date'])).'</td>';
							echo '<td>'.$saving['Sharecapital']['tran_no'].'</td>';
							switch($saving['Sharecapital']['depositype']){							
								case 21100: $tablegap = 0; break; //savings
								case 30150: $tablegap = 1; break; //fixed									
								default: $tablegap = 0; break;
							}								
								if(!empty($tablegap) || $tablegap <= 0){
									for($i=1; $i<=$tablegap; $i++){
										echo '<td>&nbsp;</td>';
									}
								}						
							
						echo '<td>'.__('P ').$saving['Sharecapital']['amount'].'</td>';
					echo '</tr>';					
				endforeach;
				echo '<tr class="table-information table-separator"><td colspan="15"></td></tr>';
			}
		}
		
		public function totalsavingstransactions($member=null, $basedate=null){
			$savings = $this->requestAction('savings/latestpayments/'.$member.'/'.$basedate);				
			$transactions = '';
			
			if(!empty($savings)){
				foreach($savings as $saving):		
						$credit += $saving['Saving']['credit'];
						$debit += $saving['Saving']['debit'];								
				endforeach;				
				$transactions = $credit - $debit;			
			}
			
			return $transactions;
		}
		
		public function receivablestransactions($member=null, $basedate=null, $requestype=null){
			$receivables = $this->requestAction('mareceivables/latestpayments/'.$member.'/'.$basedate);				
			
			if(!empty($receivables)){
				foreach($receivables as $receivable):
					echo '<tr class="table-information funds">';													
						echo '<td>'.date('m/d/Y', strtotime($saving['Saving']['tran_date'])).'</td>';
							echo '<td>'.$saving['Saving']['tran_no'].'</td>';
							switch($saving['Saving']['depositype']){							
								case 21100: $tablegap = 0; break; //savings
								case 30150: $tablegap = 1; break; //fixed									
								default: $tablegap = 0; break;
							}								
								if(!empty($tablegap) || $tablegap <= 0){
									for($i=1; $i<=$tablegap; $i++){
										echo '<td>&nbsp;</td>';
									}
								}
						echo '<td>'.__('P ').$saving['Saving']['amount'].'</td>';
					echo '</tr>';
				endforeach;
			}
		}
		
		public function gettotaltransactions($member=null, $basedate=null, $loantype=null){
			$total_payments = 0;
			$payments = $this->requestAction('loanjoinedpayments/latestotalpayments/'.$member.'/'.$basedate.'/'.$loantype);	
			if(!empty($payments)){
				foreach($payments as $payment):
					$total_payments += $payment['Loanjoinedpayment']['prin_payment'];
				endforeach;
			}
			
			return $total_payments;
		}
		
		public function transactions($member=null, $basedate=null, $loantype=null){
			$payments = $this->requestAction('loanjoinedpayments/latestpayments/'.$member.'/'.$basedate.'/'.$loantype);	
			
			if(!empty($payments)){
				foreach($payments as $key => $payment):
					echo '<tr class="table-information">';
						//if($loantype==$payment['Loanjoinedpayment']['loantype_id']){							
						switch($payment['Loanjoinedpayment']['loantype_id']){							
							case 11711.1: $tablegap = 0; break; //motorcycle
							case 11711.2: $tablegap = 1; break; //appliance
							case 11711.3: $tablegap = 2; break; //appliance
							case 11711.4: $tablegap = 3; break; //petty cash
							case 11711.5: $tablegap = 4; break; //special secured loan
							case 11711.8: $tablegap = 5; break; //maxiloan							
							case 11711.9: $tablegap = 6; break; //sako care							
							case 11720: $tablegap = 7; break; //eyeglass							
							default: $tablegap = 0; break;
						}
							echo '<td>'.date('m/d/Y', strtotime($payment['Loanjoinedpayment']['trans_date'])).'</td>';
							echo '<td>'.$payment['Loanjoinedpayment']['loanvchno'].'</td>';
							echo '<td>&nbsp;</td>';
							echo '<td>&nbsp;</td>';
							echo '<td>'.__('P ').$payment['Loanjoinedpayment']['int_payment'].'</td>';
							if(!empty($tablegap) || $tablegap <= 0){
								for($i=1; $i<=$tablegap; $i++){
									echo '<td>&nbsp;</td>';
								}
							}
							echo '<td>'.__('(').number_format($payment['Loanjoinedpayment']['prin_payment'], 2, '.', ',').')</td>';
						//}
					echo '</tr>';
				endforeach;
			}
		}
		
		public function loaninterest($member=null, $basedate=null, $requesttype=null){
			$total = 0;
			switch($requesttype){
				case "beginningbalance":
					$famount = $this->requestAction('firstinterests/beginningbalance/'.$member.'/'.$basedate);
					$samount = $this->requestAction('secondinterests/beginningbalance/'.$member.'/'.$basedate);
					$total = $famount + $samount;
				break;
				case "totalbalance":
					$famount = $this->requestAction('firstinterests/totalbalance/'.$member.'/'.$basedate);
					$samount = $this->requestAction('secondinterests/totalbalance/'.$member.'/'.$basedate);
					$total = $famount + $samount;
				break;
				case "balanceinterest":
					$amount = $this->requestAction('firstinterests/balanceinterest/'.$member.'/11711.5');
					$total = $amount;
				break;
			}
			
			return $total;
		}
		
		public function getindicatorid($name){
			$indicator = $this->requestAction(array('controller' => 'studentindicators', 'action' => 'getindicator', $name));
			return $indicator;
		}
				
		public function getnamebyid($id, $controller, $action){	
			$id = $this->requestAction(array('controller' => $controller, 'action' => $action, $id));
			return $id;			
		}
		
		public function studentprofeciency($grade, $comparison, $gender, $sy, $gradelevel, $section, $teacher){
			$total = $this->requestAction(array('controller' => 'schoolgrades', 'action' => 'studentprofeciency', $grade, $comparison, $gender, $sy, $gradelevel, $section, $teacher));
			return $total;
		}
		
		public function studentprofeciencylevel2($grade, $grade2, $gender, $sy, $gradelevel, $section, $teacher){
			$total = $this->requestAction(array('controller' => 'schoolgrades', 'action' => 'studentprofeciency2', $grade, $grade2, $gender, $sy, $gradelevel, $section, $teacher));
			return $total;
		}
		
		public function gradelevelcount($id, $gender, $sy){
			$total = $this->requestAction(array('controller' => 'students', 'action' => 'gradelevelcount', $id, $gender, $sy));
			return $total;
		}
		
		public function studentprofeciencyall($grade, $comparison, $gender, $glevel1, $glevel2, $sy){
			$total = $this->requestAction(array('controller' => 'schoolgrades', 'action' => 'studentprofeciencyall', $grade, $comparison, $gender, $glevel1, $glevel2, $sy));
			return $total;
		}
		
		public function studentprofeciencyall2($grade, $grade2, $gender, $glevel1, $glevel2, $sy){
			$total = $this->requestAction(array('controller' => 'schoolgrades', 'action' => 'studentprofeciencyall2', $grade, $grade2, $gender, $glevel1, $glevel2, $sy));
			return $total;
		}
		
	}
?>