<?php echo $this->element('submenu/student'); ?>
<h3 class="default"><?php echo __('Add Student(s) using Batch Upload');?></h3>
<div class="students form">
<?php echo $this->Form->create('Student', array('enctype' => 'multipart/form-data'));?>	
	<?php
		echo '<div class="col-md-4 col-lg-4 col-xs-4 no-padding-left">';
		echo '<label for="">School Year</label>';
		echo $this->Form->input('schoolyear_id', array('label' => ''));	
		echo '<label for="">Select Grade</label>';
		echo $this->Form->input('gradelevel_id', array('label' => '', 'class' => ''));	
		echo '<label for="">Select Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => '', 'class' => ''));	
		echo '</div>';
		echo '<div class="col-md-4 col-lg-4 col-xs-4">';
		echo '<label for="">Select Teacher</label>';
		echo $this->Form->input('teacher_id', array('label' => '', 'class' => ''));			
		echo '<label for="">Select file to upload ( xls, xlsx )</label>';
		echo $this->Form->file('filetoupload', array('label' => 'Select file to upload'));				
		echo $this->Form->submit('Upload File & Save', array('class' => 'btn btn-primary'));
		echo '</div>';
		echo '<div class="clear"></div>';
	?>	
	<br /><br />
</div>
