<div class="students index">
	<?php echo $this->element('submenu/student'); ?>
	<div class="col-md-12 col-lg-12 col-xs-12" style="padding: 2px 8px 10px 8px; border: 1px solid #ccc;  margin: 5px 0 10px 0; background: #f4f2f2;">
	<div class="col-md-3 col-lg-3 col-xs-3 pull-left" style="padding-left: 0;">
		<h3>Student's Masterlist</h3>
	</div>
	
	<div class="col-md-5 col-lg-5 col-xs-5 pull-right">
		<?php
			echo $this->Form->create('Search');					
			echo '<div class="col-md-5 col-lg-5 col-xs-5" style="padding-right: 0;">';
				echo '<label for="">Keyword</label>';
				echo $this->Form->input('keyword', array('type' => 'text', 'label' => '', 'placeholder' => 'lastname / firstname ....'));
			echo '</div>';
			echo '<div class="col-md-5 col-lg-5 col-xs-5" style="padding-left: 0; padding-right: 0;">';
				echo '<label for="">LRN</label>';
				echo $this->Form->input('lrn', array('type' => 'text', 'label' => '', 'placeholder' => 'Lrn ....'));
			echo '</div>';
			echo '<div class="col-md-2 col-lg-2 col-xs-2">';
				echo '<br />';
				echo $this->Form->submit('Search', array('class' => 'btn btn-success')); 
			echo '</div>';
		?>
	</div>
	
	<div class="col-md-3 col-lg-3 col-xs-3 pull-right">
		<?php
			echo $this->Form->create('Filter');
			echo '<div class="col-md-10 col-lg-10 col-xs-10" style="padding-right: 0;">';				
				echo '<label for="">&nbsp;</label>';
				echo $this->Form->input('schoolyear_id', array('label' => 'S.Y.&nbsp;', 'class' => 'normalinput'));
			echo '</div>';
			echo '<div class="col-md-2 col-lg-2 col-xs-2">';
				echo '<br />';
				echo $this->Form->submit('Filter', array('class' => 'btn btn-primary')); 
			echo '</div>';
		?>
	</div>
	
	
	<div class="clear"></div>
	</div>
	
	<?php //echo $this->element('top.navigation'); ?>
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th><?php echo $this->Paginator->sort('lrn');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('firstname');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('MI');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('lastname');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('gender');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('birthdate');?><span class="caret"></span></th>	
			<th class=""><?php echo __('Age');?></th>			
			<th class="actions"><?php echo __('Indicator');?></th>
			<th class="actions"><?php echo __('S.Y.');?></th>
			<th><?php echo $this->Paginator->sort('Teacher');?></th>
			
			<th class="actions"><?php echo __('Grade');?></th>
			<th><?php echo $this->Paginator->sort('section_id');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($students as $student): ?>
	<tr>		
		<td><?php echo h($student['Student']['lrn']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['firstname']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['middlename']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['lastname']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['gender']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['birthdate']); ?>&nbsp;</td>		
		<td><?php 
			echo floor((time() - strtotime($student['Student']['birthdate'])) / 31556926);
		?></td>				
		<td>
			<?php echo $student['Studentindicator']['code']; ?>
		</td>
		<td><?php echo h($student['Schoolyear']['name']); ?>&nbsp;</td>		
		<td><?php echo $student['Teacher']['title'] .' '. $student['Teacher']['lastname']; ?>&nbsp;</td>		
		
		
		
		<td>
			<?php echo $student['Gradelevel']['name']; ?>
		</td>
		<td>
			<?php echo $student['Gradesection']['name']; ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $student['Student']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $student['Student']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $student['Student']['id']), null, __('Are you sure you want to delete # %s?', $student['Student']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>

