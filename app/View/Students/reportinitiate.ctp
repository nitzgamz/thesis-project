<?php echo $this->element('submenu/student'); ?>
<div class="col-md-5 col-lg-5 col-xs-5 " style="background: #ccc; border: 1px solid #ccc; border-radius: 10px 10px; margin-top: 30px;">	
<h3 class="default">School Form 5 ( SF 5 ) Report on Promotion & Level of Proficiency</h3>
<div class="students form">

<?php echo $this->Form->create('Student');?>
	
	<?php
	
		echo '<label for="">Teacher</label>';
		echo $this->Form->input('teacher_id', array('label' => ''));
		echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => ''));
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => ''));
		echo '<label for="">School Year</label>';
		echo $this->Form->input('schoolyear_id', array('label' => ''));		
	?>
	<br />
	<div class="col-md-4 col-lg-4 col-xs-4 no-padding-left">
		<?php echo $this->Form->submit(__('Generate PDF Report'), array('class' => 'btn btn-success'));?>
	</div>	
	<?php 
		if($download){ 
			echo '<div class="col-md-4 col-lg-4 col-xs-4" style="margin-top: 5px;">';
				echo $this->Html->link('Download / View File', array('action' => 'report', 'ext' => 'pdf', $this->data['Student']['teacher_id'], $this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id']), array('class' => 'btn btn-danger', 'target' => '_blank'));
			echo '</div>';
		}
	?>
	<div class="clear"></div>
	<br /><br />
</div>
</div>
<div class="clear"></div>
<br />