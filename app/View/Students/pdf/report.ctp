<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
	<title>School Form 1</title>
	<meta name="dompdf.view" content="Fit"/>
	<meta name="Author" content="S&amp;S Enterprises"/>
	<meta name="description" content="Wish List Quote Request" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
		html, body{margin: 20px 20px;}		
		/*table {page-break-before: always; page-break-after: always;}*/
		@page{
			margin-top: 5px;
			margin-left: 20px;
			margin-bottom: 5px;
			margin-right: 20px;
		}
		table{border-collapse:collapse;}
		table,tbody,tr,th,td{margin:0;padding:0;}
		/*th,td{white-space:nowrap;}*/
		 #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after { content: counter(page, upper-roman); }
	</style>
<body>	
<div id="header">
    <h1>ibmphp.blogspot.com</h1>
  </div>
  <div id="footer">
    
  </div>
<script type="text/php">
        if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(30, 18, "Page {PAGE_NUM} out of {PAGE_COUNT}", $font, 8, array(255,0,0));

        }
    </script>
	
<h2 style="text-align: center; font: bold 18px arial, heveltica, sans-serif; padding: 0; margin: 0;">
	School Form 5 ( SF 5 ) Report on Promotion & Level of Profeciency
	<div style="font-size: 10px; font-style: italic;">( This replaced Forms 18-E1, 18-E2 and List of Graduates )</div>
</h2>

<div style="float: left; width: 150px; margin-left: 0;">
	<img src="<?php echo APP; ?>/webroot/img/1.png" style="width: 100%;"/>
</div>
<div style="position: absolute; width: 820px; top: 70px; left: 200px;">
<table style="font: bold 12px arial, heveltica, sans-serif;" cellpadding="5" cellspacing="0" width="100%">
	
	<tr>
		<td style="border: 1px solid #666;">REGION</td><td style="border: 1px solid #666; text-align: left;">CARAGA</td>
		<td style="border: 1px solid #666;">DIVISION</td><td style="border: 1px solid #666; text-align: left;">BISLIG CITY</td>
		<td style="border: 1px solid #666;">DISTRICT</td><td style="border: 1px solid #666; text-align: left;">BISLIG I</td>
	</tr>
	<tr>
		<td style="border: 1px solid #666;">SCHOOL ID</td><td style="border: 1px solid #666;">132609</td>
		<td style="border: 1px solid #666;">SCHOOL YEAR</td><td style="border: 1px solid #666;"><?php echo $sy['Schoolyear']['name']; ?></td>
		<td style="border: 1px solid #666;">CURRICULUM</td><td style="border: 1px solid #666; text-transform: uppercase;">RBEC</td>	
		
	</tr>
	<tr>
		<td style="border: 1px solid #666;">SCHOOL NAME</td><td colspan="5" style="border: 1px solid #666;">BISLIG CENTRAL ELEMENTARY SCHOOL</td>
	</tr>
	<tr>
		<td style="border: 1px solid #666;">GRADE</td><td colspan="2"style="border: 1px solid #666; text-transform: uppercase;"><?php echo $gradelevel['Gradelevel']['name']; ?></td>	
		<td style="border: 1px solid #666;">SECTION</td><td colspan="2" style="border: 1px solid #666; text-transform: uppercase;"><?php echo $section['Gradesection']['name']; ?></td>
	</tr>

</table>
</div>
<div style="position: absolute; width: 200px; top: 60px; right: 0px;">
	<img src="<?php echo APP; ?>/webroot/img/2.png" style="width: 100%;"/>
</div>
<div style="clear: both;"></div>

<div style="">

<table style="font: bold 12px arial, heveltica, sans-serif; margin-top: 10px;" cellpadding="5" cellspacing="0" width="100%">
	<thead>
	<tr>
		<td style="border: 1px solid #000;">LRN</td>
		<td style="border: 1px solid #000;">LEARNERS NAME <br /> ( Last Name, First Name, Middle Name )</td>
		<td style="border: 1px solid #000;">GENERAL AVERAGE <br />(Numerical Value in 3 decimal places for honor learner, 2 for non-honor & Descriptive Letter)</td>
		<td style="border: 1px solid #000;">ACTION TAKEN: PROMOTED, *IRREGULAR or RETAINED  </td>
		<td style="border: 1px solid #000;">INCOMPLETE SUBJECT/S<br />(This column is for K to 12 Curriculum and remaining RBEC in High School. Elementary grades level that still implementing RBEC need not to fill up this column)</td>
	</tr>
	</thead>
	<tbody>
	<?php
		$male = 0;
		$female = 0;
		$total = 0;
		if(!empty($students)){
			foreach($students as $student):
			if($student['Student']['gender']=="M"){
				$male++;				
	?>
			<tr>
				<td style="border: 1px solid #666; border-left: 1px solid #666;"><?php echo $student['Student']['lrn']; ?></td>
				<td style="border: 1px solid #666;"><?php echo $student['Student']['lastname'].', '.$student['Student']['firstname'].' '.$student['Student']['middlename']; ?></td>
				<td style="border: 1px solid #666;">&nbsp;</td>
				<td style="border: 1px solid #666;">PROMOTED</td>
				<td style="border: 1px solid #666; border-right: 1px solid #666;">&nbsp;</td>
			</tr>
	<?php
			}
			endforeach;
	?>
			<tr><td colspan="5" style="border: 2px solid #000; border-top: 2px solid #000;">TOTAL MALE : <?php echo $male; ?></td></tr>
	<?php		
			foreach($students as $student):
			if($student['Student']['gender']=="F"){
				$female++;
	?>
			<tr>
				<td style="border: 1px solid #666;"><?php echo $student['Student']['lrn']; ?></td>
				<td style="border: 1px solid #666;"><?php echo $student['Student']['lastname'].', '.$student['Student']['firstname'].' '.$student['Student']['middlename']; ?></td>
				<td style="border: 1px solid #666;">&nbsp;</td>
				<td style="border: 1px solid #666;">PROMOTED</td>
				<td style="border: 1px solid #666;">&nbsp;</td>
			</tr>
	<?php
			}
			endforeach;
	?>
			<tr><td colspan="5" style="border: 2px solid #000; border-top: 2px solid #000;">TOTAL FEMALE : <?php echo $female; ?></td></tr>
			<tr><td colspan="5" style="border: 2px solid #000; border-top: 2px solid #000;">COMBINED : <?php echo $male + $female; ?></td></tr>
	<?php
		}
	?>
	<tbody>
</table>

</div>
<div style="page-break-before:always;">
<table style="font: bold 12px arial, heveltica, sans-serif; border-top: 2px solid #000; margin-top: 10px; border: 1px solid #000;" cellpadding="5" cellspacing="2" width="50%">
	<tr>
		<td>SUMMARY TABLE</td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #666;">STATUS</td>
		<td style="border-bottom: 1px solid #666;">MALE</td>
		<td style="border-bottom: 1px solid #666;">FEMALE</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
	</tr>
	<tr>
		<td>PROMOTED</td>
		<td><?php echo $male; ?></td>
		<td><?php echo $female; ?></td>
		<td><?php echo $male + $female; ?></td>
	</tr>
	<tr>
		<td>IRREGULAR</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>RATAINED</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>

<table style="font: bold 12px arial, heveltica, sans-serif; border-top: 2px solid #000; margin-top: 10px; border: 1px solid #000;" cellpadding="5" cellspacing="2" width="50%">
	<tr>
		<td>LEVEL OF <br />PROFECIENCY</td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">MALE</td>
		<td style="border-bottom: 1px solid #666;">FEMALE</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
	</tr>
	<tr>
		<td>BEGINNNING <br /> (B: 74% and below)</td>
		<?php
			$male74 = $this->Externalfunction->studentprofeciency('74', '<=', 'M', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
			$female74 = $this->Externalfunction->studentprofeciency('74', '<=', 'M', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
		?>
		<td><?php echo $male74; ?></td>
		<td><?php echo $female74; ?></td>
		<td><?php echo $male74 + $female74; ?></td>
	</tr>
	<tr>
		<td>DEVELOPING <br /> (D: 75%-79%)</td>
		<?php
			$male75 = $this->Externalfunction->studentprofeciencylevel2('75', '79', 'M', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
			$female75 = $this->Externalfunction->studentprofeciencylevel2('75', '79', 'F', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
		?>
		<td><?php echo $male75; ?></td>
		<td><?php echo $female75; ?></td>
		<td><?php echo $male75 + $female75; ?></td>
	</tr>
	<tr>
		<td>APPROACHING <br />PROFICIENCY <br /> (AP: 80%-84%)</td>
		<?php
			$male80 = $this->Externalfunction->studentprofeciencylevel2('80', '84', 'M', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
			$female80 = $this->Externalfunction->studentprofeciencylevel2('80', '84', 'F', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
		?>
		<td><?php echo $male80; ?></td>
		<td><?php echo $female80; ?></td>
		<td><?php echo $male80 + $female80; ?></td>
	</tr>
	<tr>
		<td>PROFICIENT <br /> (P: 85% -89%)</td>
		<?php
			$male85 = $this->Externalfunction->studentprofeciencylevel2('85', '89', 'M', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
			$female85 = $this->Externalfunction->studentprofeciencylevel2('85', '89', 'F', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
		?>
		<td><?php echo $male85; ?></td>
		<td><?php echo $female85; ?></td>
		<td><?php echo $male85 + $female85; ?></td>
	</tr>
	<tr>
		<td>ADVANCED <br /> (A: 90%  and above)</td>
		<?php
			$male90 = $this->Externalfunction->studentprofeciency('90', '>=', 'M', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
			$female90 = $this->Externalfunction->studentprofeciency('90', '>=', 'M', $sy['Schoolyear']['id'], $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], $teacher['Teacher']['id']);
		?>
		<td><?php echo $male90; ?></td>
		<td><?php echo $female90; ?></td>
		<td><?php echo $male90 + $female90; ?></td>
	</tr>
</table>
</div>
<div style="page-break-before:always;">
<table style="font: bold 12px arial, heveltica, sans-serif; margin-top: 50px;" cellpadding="5" cellspacing="2" width="25%">
	<tr><td>PREPARED BY:</td></tr>
	<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
	<tr><td style="text-align: center;">CLASS ADVISER <br /> ( Name & Signature )</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>CERTIFIED CORRECT & SUBMITTED:</td></tr>
	<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
	<tr><td style="text-align: center;">SCHOOL HEAD <br /> ( Name & Signature )</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>REVIEWED BY:</td></tr>
	<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
	<tr><td style="text-align: center;">DIVISION REPRESENTATIVE <br /> ( Name & Signature )</td></tr>
</table>
</div>