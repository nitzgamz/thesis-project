<div class="students index">
	<?php echo $this->element('submenu/student'); ?>
	<div class="col-md-3 col-lg-3 col-xs-3" style="padding-left: 0;">
	<h2><?php echo __('Students ( ') . $studentscount . ' )';?></h2>
	</div>
	<div class="col-md-3 col-lg-3 col-xs-3 pull-right">
		<?php
			echo $this->Form->create('Student');
			echo $this->Form->input('schoolyear_id', array('label' => ''));
			echo $this->Form->submit('Filter', array('class' => 'btn btn-primary')); 
		?>
	</div>
	<div class="col-md-3 col-lg-3 col-xs-3 pull-right">
		<?php
			echo $this->Form->create('Search', array('url' => array('controller' => 'students', 'action' => 'add')));		
			echo '<div style="margin-top: 12px;"></div>';
			echo $this->Form->input('keyword', array('type' => 'text', 'label' => 'Keyword&nbsp;'));
			echo $this->Form->submit('Search', array('class' => 'btn btn-success')); 
		?>
	</div>
	<div class="clear"></div>
	<?php echo $this->element('top.navigation.less.left'); ?>
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th><?php echo $this->Paginator->sort('lrn');?></th>
			<th><?php echo $this->Paginator->sort('firstname');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('MI');?></th>
			<th><?php echo $this->Paginator->sort('lastname');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('gender');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('birthdate');?><span class="caret"></span></th>	
			<th class=""><?php echo __('Age');?></th>			
			<th><?php echo $this->Paginator->sort('Indicator');?></th>
			<th><?php echo $this->Paginator->sort('SY');?></th>
			<th><?php echo $this->Paginator->sort('Teacher');?></th>
			
			<th><?php echo $this->Paginator->sort('gradelevel_id');?></th>
			<th><?php echo $this->Paginator->sort('section_id');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($students as $student): ?>
	<tr>		
		<td><?php echo h($student['Student']['lrn']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['firstname']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['middlename']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['lastname']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['gender']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['birthdate']); ?>&nbsp;</td>		
		<td><?php 
			echo floor((time() - strtotime($student['Student']['birthdate'])) / 31556926);
		?></td>				
		<td>
			<?php echo $student['Studentindicator']['code']; ?>
		</td>
		<td><?php echo h($student['Schoolyear']['name']); ?>&nbsp;</td>		
		<td><?php echo $student['Teacher']['title'] .' '. $student['Teacher']['lastname']; ?>&nbsp;</td>		
		
		
		
		<td>
			<?php echo $student['Gradelevel']['name']; ?>
		</td>
		<td>
			<?php echo $student['Gradesection']['name']; ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $student['Student']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $student['Student']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $student['Student']['id']), null, __('Are you sure you want to delete # %s?', $student['Student']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>

