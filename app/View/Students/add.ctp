<?php echo $this->element('submenu/student'); ?>
<div class="students form">
<h3 class="default"><?php echo __('Add New Student'); ?></h3>
<?php echo $this->Form->create('Student');?>
	<fieldset>		
	<?php
		echo '<div class="col-md-4 col-lg-4 col-xs-4 no-padding-left">';
		echo '<label for="">LRN</label>';
		echo $this->Form->input('lrn', array('label' => ''));
		echo '<label for="">School Year</label>';
		echo $this->Form->input('schoolyear_id', array('label' => ''));	
		echo '<label for="">Teacher</label>';
		echo $this->Form->input('teacher_id', array('label' => ''));
		echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => ''));
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => ''));
		echo '<label for="">Firstname</label>';
		echo $this->Form->input('firstname', array('label' => ''));
		echo '<label for="">Middle</label>';
		echo $this->Form->input('middlename', array('label' => ''));
		echo '<label for="">Lastname</label>';
		echo $this->Form->input('lastname', array('label' => ''));
		echo '<label for="">Gender</label>';
		echo $this->Form->input('gender', array('options' => array('M' => 'M', 'F' => 'F'), 'label' => ''));
		
		echo '</div>';
		echo '<div class="col-md-4 col-lg-4 col-xs-4">';	
		
		echo '<label for="">Birthdate</label>';
		echo $this->Form->input('birthdate', array('label' => ''), array('class' => 'birthdate'));		
			
		echo '<label for="">Birthplace</label>';
		echo $this->Form->input('birthplace', array('label' => ''));			
		
		echo '<label for="">Mother Tongue</label>';
		echo $this->Form->input('mother_tongue', array('label' => ''));
		echo '<label for="">IP (Specify Ethnic Group)</label>';
		echo $this->Form->input('ethnic_group', array('label' => ''));
		echo '<label for="">Religion</label>';
		echo $this->Form->input('religion', array('label' => ''));
		echo '<label for="">House # / Street/Sitio/ Purok</label>';
		echo $this->Form->input('address_house', array('label' => ''));
		echo '<label for="">Barangay</label>';
		echo $this->Form->input('address_brgy', array('label' => ''));
		echo '<label for="">Municipality/ City </label>';
		echo $this->Form->input('address_municipal', array('label' => ''));
		echo '<label for="">Province</label>';
		echo $this->Form->input('address_province', array('label' => ''));
		echo '</div>';
		echo '<div class="col-md-4 col-lg-4 col-xs-4">';
		echo '<label for="">Father Name <br /> (1st name only if family name identical to learner)</label>';
		echo $this->Form->input('father_fullname', array('label' => ''));
		echo '<label for="">Mother Name <br /> (Maiden: 1st Name, Middle & Last Name)</label>';
		echo $this->Form->input('mother_fullname', array('label' => ''));
		echo '<label for="">Guardian Name</label>';
		echo $this->Form->input('guardian_fullname', array('label' => ''));
		echo '<label for="">Relationship</label>';
		echo $this->Form->input('relationship', array('label' => ''));
		echo '<label for="">Contact Number (Parent /Guardian)</label>';
		echo $this->Form->input('contact_number', array('label' => ''));
		echo '<label for="">Remakrs (Please refer to the legend)</label>';
		echo $this->Form->input('studentindicator_id', array('label' => ''));	
		
		echo $this->Form->input('added', array('default' => date('Y-m-d'), 'type' => 'hidden', 'label' => ''));	
		echo $this->Form->input('added_by', array('default' => $userid, 'type' => 'hidden', 'label' => ''));	
		echo '</div>';
	?>
	</fieldset>	
<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
</div>
