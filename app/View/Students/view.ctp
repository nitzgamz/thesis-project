<?php echo $this->element('submenu/student'); ?>
<div class="students view">
<h3 class="default"><?php  echo __('Student Profile');?></h3>	
	<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left">	
		<h3 style="background: #f4f2f2; border: 1px solid #ccc; padding: 5px 5px;">
			<?php echo $student['Student']['lastname'].', '.$student['Student']['firstname'].' '.$student['Student']['middlename']; ?>		
		</h3>
		<table class="table table-striped table-hover table-condensed">
		<tr>
			<td><?php echo __('LRN'); ?></td>		
			<td><?php echo h($student['Student']['lrn']); ?></td>			
		</tr>
		<tr>
			<td><?php echo __('Firstname'); ?></td>		
			<td><?php echo h($student['Student']['firstname']); ?></td>
		</tr>
		<tr>
			<td><?php echo __('Middlename'); ?></td>		
			<td><?php echo h($student['Student']['middlename']); ?></td>
		</tr>
		<tr>
			<td><?php echo __('Lastname'); ?></td>		
			<td><?php echo h($student['Student']['lastname']); ?></td>
		</tr>
		<tr>
			<td><?php echo __('Gender'); ?></td>		
			<td><?php echo h($student['Student']['gender']); ?></td>
		</tr>
		<tr>
			<td><?php echo __('Birthdate'); ?></td>
			<td><?php echo h($student['Student']['birthdate']); ?></td>
		</tr>
		<tr>
			<td><?php echo __('Mother Tongue'); ?></td>
			<td>
				<?php echo h($student['Student']['mother_tongue']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Ethnic Group'); ?></td>
			<td>
				<?php echo h($student['Student']['ethnic_group']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Religion'); ?></td>
			<td>
				<?php echo h($student['Student']['religion']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Address House'); ?></td>
			<td>
				<?php echo h($student['Student']['address_house']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Address Brgy'); ?></td>
			<td>
				<?php echo h($student['Student']['address_brgy']); ?>				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Address Municipal'); ?></td>
			<td>
				<?php echo h($student['Student']['address_municipal']); ?>
			
			</td>
		</tr>
		<tr>
			<td><?php echo __('Address Province'); ?></td>
			<td>
				<?php echo h($student['Student']['address_province']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Father Fullname'); ?></td>
			<td>
				<?php echo h($student['Student']['father_fullname']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Mother Fullname'); ?></td>
			<td>
				<?php echo h($student['Student']['mother_fullname']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Guardian Fullname'); ?></td>
			<td>
				<?php echo h($student['Student']['guardian_fullname']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Relationship'); ?></td>
			<td>
				<?php echo h($student['Student']['relationship']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Contact Number'); ?></td>
			<td>
				<?php echo h($student['Student']['contact_number']); ?>
				
			</td>
		</tr>
		<tr>
			<td><?php echo __('Studentindicator'); ?></td>
			<td>
				<?php echo $student['Studentindicator']['code']; ?>
				
			</td>
		</tr>		
		<tr>
			<td><?php echo __('Current Grade Level'); ?></td>
			<td>
				<?php echo $student['Gradelevel']['name']; ?>
				
			</td>
		</tr>
		</table>
	</div>
	<div class="col-md-7 col-lg-7 col-xs-7">	
			<div class="related">
			<h3><?php echo __('Grade History');?></h3>
			<?php if (!empty($student['Schoolgrade'])){ ?>			
			<?php
				$i = 0;
				foreach ($student['Schoolgrade'] as $schoolgrade): ?>
				<div class="col-md-12 col-lg-12 col-xs-12" style="border: 1px solid #ccc; margin-bottom: 20px; background: #fff;">	
				<h3 class="default">
					<?php echo $this->Externalfunction->getnamebyid($schoolgrade['gradelevel_id'], 'gradelevels', 'getnamebyid'); ?> - <?php echo $this->Externalfunction->getnamebyid($schoolgrade['gradelevel_id'], 'gradesections', 'getnamebyid'); ?>  ( S.Y. <?php echo $this->Externalfunction->getnamebyid($schoolgrade['schoolyear_id'], 'schoolyears', 'getnamebyid'); ?> )
				</h3>
					
				<?php 
					$ginit = $schoolgrade['grades']; 
					$fg = explode(",", $ginit);
				?>
				<div class="clear"></div>
				<div class="col-md-6 col-lg-6 col-xs-6 no-padding-left">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th colspan="5"><?php echo $fg[0]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[1]; ?></td>
						<td><?php echo $fg[2]; ?></td>
						<td><?php echo $fg[3]; ?></td>
						<td><?php echo $fg[4]; ?></td>
						<td><?php echo $fg[5]; ?></td>
					</tr>
					<tr>
						<th colspan="5"><?php echo $fg[6]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[7]; ?></td>
						<td><?php echo $fg[8]; ?></td>
						<td><?php echo $fg[9]; ?></td>
						<td><?php echo $fg[10]; ?></td>
						<td><?php echo $fg[11]; ?></td>
					</tr>
					<tr>
						<th colspan="5"><?php echo $fg[12]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[13]; ?></td>
						<td><?php echo $fg[14]; ?></td>
						<td><?php echo $fg[15]; ?></td>
						<td><?php echo $fg[16]; ?></td>
						<td><?php echo $fg[17]; ?></td>
					</tr>
					<tr>
						<th colspan="5"><?php echo $fg[18]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[19]; ?></td>
						<td><?php echo $fg[20]; ?></td>
						<td><?php echo $fg[21]; ?></td>
						<td><?php echo $fg[22]; ?></td>
						<td><?php echo $fg[23]; ?></td>
					</tr>
					<tr>
						<th colspan="5"><?php echo $fg[24]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[25]; ?></td>
						<td><?php echo $fg[26]; ?></td>
						<td><?php echo $fg[27]; ?></td>
						<td><?php echo $fg[28]; ?></td>
						<td><?php echo $fg[29]; ?></td>
					</tr>		
				</table>
			</div>
			<div class="col-md-6 col-lg-6 col-xs-6 no-padding-right">		
				<table class="table table-striped table-hover table-condensed">		
					
					<tr>
						<th colspan="5"><?php echo $fg[30]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[31]; ?></td>
						<td><?php echo $fg[32]; ?></td>
						<td><?php echo $fg[33]; ?></td>
						<td><?php echo $fg[34]; ?></td>
						<td><?php echo $fg[35]; ?></td>
					</tr>
					<tr>
						<th colspan="5"><?php echo $fg[36]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[37]; ?></td>
						<td><?php echo $fg[38]; ?></td>
						<td><?php echo $fg[39]; ?></td>
						<td><?php echo $fg[40]; ?></td>
						<td><?php echo $fg[41]; ?></td>
					</tr>
					<tr>
						<th colspan="5"><?php echo $fg[42]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[43]; ?></td>
						<td><?php echo $fg[44]; ?></td>
						<td><?php echo $fg[45]; ?></td>
						<td><?php echo $fg[46]; ?></td>
						<td><?php echo $fg[47]; ?></td>
					</tr>
					<tr>
						<th colspan="5"><?php echo $fg[48]; ?></th>
					</tr>
					<tr>
						<td>1st <br /> Qtr</td>
						<td>2nd <br /> Qtr</td>
						<td>3rd <br /> Qtr</td>
						<td>4th <br /> Qtr</td>
						<td>Total <br /> Average</td>
					</tr>
					<tr>
						<td><?php echo $fg[49]; ?></td>
						<td><?php echo $fg[50]; ?></td>
						<td><?php echo $fg[51]; ?></td>
						<td><?php echo $fg[52]; ?></td>
						<td><?php echo $fg[53]; ?></td>
					</tr>
					<tr>
						<td colspan="4"><?php echo __('Total Average'); ?></td>		
						<td><?php echo h($schoolgrade['tgrade']); ?></td>	
					</tr>
					<tr>
						<td colspan="4"><?php echo __('General Average'); ?></td>
						<td><?php echo h($schoolgrade['ggrade']); ?></td>
					</tr>
				</table>				
			</div>
			<div class="clear"></div>
			</div>
			<?php endforeach; ?>			
		<?php }else{ ?>	
			<div class="error_message">Nothing found</div>
		<?php } ?>
	</div>
	</div>
	<div class="clear"></div>
</div>

