<?php echo $this->element('submenu/teachers'); ?>
<div class="teachers view">
<h3 class="default"><?php  echo __('Teacher Information');?></h3>
	<div class="col-md-3 col-lg-3 col-xs-3">	
		<table class="table table-striped table-hover table-condensed">
		<tr>
		<td><?php echo __('Title'); ?></td>
		<td>
			<?php echo h($teacher['Teacher']['title']); ?>
			&nbsp;
		</td>
		<tr>
		</tr>
		<td><?php echo __('Firstname'); ?></td>
		<td>
			<?php echo h($teacher['Teacher']['firstname']); ?>
			&nbsp;
		</td>
			<tr>
		</tr>
		<td><?php echo __('Middlename'); ?></td>
		<td>
			<?php echo h($teacher['Teacher']['middlename']); ?>
			&nbsp;
		</td>
			<tr>
		</tr>
		<td><?php echo __('Lastname'); ?></td>
		<td>
			<?php echo h($teacher['Teacher']['lastname']); ?>
			&nbsp;
		</td>
			<tr>
		</tr>
		<td><?php echo __('Others'); ?></td>
		<td>
			<?php echo h($teacher['Teacher']['others']); ?>
			&nbsp;
		</td>
		</tr>
		</table>
	</div>
	<div class="col-md-8 col-lg-8 col-xs-8">
			<div class="related">
				<h3><?php echo __('Subjects');?></h3>
				<?php if (!empty($teacher['Teachersubject'])):?>
				<table class="table table-striped table-hover table-condensed">
				<tr>
					
				
					<th><?php echo __('Subject'); ?></th>
					
				</tr>
				<?php
					$i = 0;
					foreach ($teacher['Teachersubject'] as $teachersubject): ?>
					<tr>
						
						
						<td>							
							<?php echo $this->Externalfunction->getnamebyid($teachersubject['schoolsubject_id'], 'schoolsubjects', 'getnamebyid'); ?> 
						</td>
						<!--<td class="actions">
							<?php //echo $this->Html->link(__('View'), array('controller' => 'teachersubjects', 'action' => 'view', $teachersubject['id'])); ?>
							<?php //echo $this->Html->link(__('Edit'), array('controller' => 'teachersubjects', 'action' => 'edit', $teachersubject['id'])); ?>
							<?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'teachersubjects', 'action' => 'delete', $teachersubject['id']), null, __('Are you sure you want to delete # %s?', $teachersubject['id'])); ?>
						</td>-->
					</tr>
				<?php endforeach; ?>
				</table>
			<?php endif; ?>

					<?php echo $this->Html->link( 'Tag / Assign Subject', array('controller' => 'teachersubjects', 'action' => 'add', $teacher['Teacher']['id']), array('class' => 'btn btn-xs btn-primary'));?>					
				
			</div>
	</div>
	<div class="clear"></div>
</div>


