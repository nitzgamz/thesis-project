<div class="teachers index">
	<?php echo $this->element('submenu/teachers'); ?>
	<div class="col-md-12 col-lg-12 col-xs-12" style="padding: 2px 8px 10px 8px; border: 1px solid #ccc;  margin: 5px 0 10px 0; background: #f4f2f2;">
	<div class="col-md-3 col-lg-3 col-xs-3 pull-left" style="padding-left: 0;">
		<h3>Teacher's Masterlist</h3>
	</div>
	
	<div class="col-md-5 col-lg-5 col-xs-5 pull-right">
		<?php
			echo $this->Form->create('Search');					
			echo '<div class="col-md-5 col-lg-5 col-xs-5" style="padding-right: 0;">';
				echo '<label for="">Firstname</label>';
				echo $this->Form->input('firstname', array('type' => 'text', 'label' => '', 'placeholder' => 'firstname ....'));
			echo '</div>';
			echo '<div class="col-md-5 col-lg-5 col-xs-5" style="padding-left: 0; padding-right: 0;">';
				echo '<label for="">Lastname</label>';
				echo $this->Form->input('lastname', array('type' => 'text', 'label' => '', 'placeholder' => 'lastname ....'));
			echo '</div>';
			echo '<div class="col-md-2 col-lg-2 col-xs-2">';
				echo '<br />';
				echo $this->Form->submit('Search', array('class' => 'btn btn-success')); 
			echo '</div>';
		?>
	</div>
	
	
	
	<div class="clear"></div>
	</div>
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th><?php echo $this->Paginator->sort('title');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('firstname');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('middlename');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('lastname');?><span class="caret"></span></th>					
			<th class="actions"><?php echo __('Grade');?></th>
			<th class="actions"><?php echo __('Section');?></th>
			<th class="actions"><?php echo __('Subjects');?></th>
			
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($teachers as $teacher): ?>
	<tr>		
		<td><?php echo h($teacher['Teacher']['title']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Teacher']['firstname']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Teacher']['middlename']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Teacher']['lastname']); ?>&nbsp;</td>		
		<td><?php echo h($teacher['Gradelevel']['name']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Gradesection']['name']); ?>&nbsp;</td>		
		<td>
			<?php 
				$subjects = $this->requestAction(array('controller' => 'teachersubjects', 'action' => 'getallteacher', $teacher['Teacher']['id']));
				if(!empty($subjects)){
					echo count($subjects);
				}else{
					echo '0';
				}
			 ?>
		</td>		
		<!--<td>
			<?php 
				$students = $this->requestAction(array('controller' => 'students', 'action' => 'getallstudentsinteacher', $teacher['Teacher']['id']));
				if(!empty($students)){
					//echo count($students);
				}else{
					//echo '0';
				}
			 ?>
		</td>	-->	
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $teacher['Teacher']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $teacher['Teacher']['id'])); ?>
			<?php echo $this->Html->link(__('Tag / Assign Subject'), array('controller' => 'teachersubjects', 'action' => 'add', $teacher['Teacher']['id'])); ?>			
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $teacher['Teacher']['id']), null, __('Are you sure you want to delete # %s?', $teacher['Teacher']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>
