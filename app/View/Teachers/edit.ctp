<?php echo $this->element('submenu/teachers'); ?>
<h3 class="default"><?php echo __('Edit Teacher Information'); ?></h3>
<div class="teachers form">
<?php echo $this->Form->create('Teacher');?>

		
	<?php
		echo '<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left">';
		echo '<label for="">Title</label>';
		echo $this->Form->input('id', array('label' => ''));
		echo $this->Form->input('title', array('options' => array('Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss'), 'label' => ''));
		echo '<label for="">Firstname</label>';		
		echo $this->Form->input('firstname', array('label' => ''));
		echo '<label for="">Middlename</label>';
		echo $this->Form->input('middlename', array('label' => ''));
		echo '<label for="">Lastname</label>';
		echo $this->Form->input('lastname', array('label' => ''));		
		echo '</div>';
		echo '<div class="col-md-3 col-lg-3 col-xs-3">';
		echo '<label for="">Other Information</label>';
		echo $this->Form->input('others', array('label' => ''));
		echo '<label for="">Assign Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => ''));
		echo '<label for="">Assign Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => ''));
		echo '</div>';
	?>
<div class="clear"></div>
	<br />
	<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
</div>
<div class="clear"></div>
