<div class="schoolyears index">
	<?php echo $this->element('submenu/subjects'); ?>
	<h3 class="default"><?php echo __('School Year');?></h3>	
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th class="actions"><?php echo __('S.Y.');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($schoolyears as $schoolyear): ?>
	<tr>		
		<td><?php echo $schoolyear['Schoolyear']['sy_from'].' - '.$schoolyear['Schoolyear']['sy_to']; ?>&nbsp;</td>		
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $schoolyear['Schoolyear']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $schoolyear['Schoolyear']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $schoolyear['Schoolyear']['id']), null, __('Are you sure you want to delete # %s?', $schoolyear['Schoolyear']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>

