<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">	
    <head> 
        <title><?php echo $title_for_layout; ?></title>         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<?php echo $this->Html->css('style.login'); ?>	
		<?php echo $this->Html->css('bootstrap'); ?>
		<?php echo $this->Html->css('bootstrap.theme'); ?>
		<?php echo $this->Html->script('jquery-1.7.1.min'); ?>
		<?php echo $this->Html->script('thickbox'); ?>		
		<?php echo $this->Html->script('bootstrap'); ?>		
	</head>
	<body>				
		<div class="content">				
			<div class="c-content">
				<div class="login-error-message"><div><?php echo $this->Session->flash(); ?></div></div>					
				<div class="content-handler">					
					<?php echo $content_for_layout; ?>
				</div>				
			</div>				
		</div>
	</body>
</html>