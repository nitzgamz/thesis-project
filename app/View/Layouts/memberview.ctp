<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">	
    <head> 
        <title><?php echo $title_for_layout; ?></title>         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<?php echo $this->Html->css('html5-reset'); ?>
		<?php echo $this->Html->css('style.generic.default'); ?>
	<body>						
			<div class="main-wrap" style="margin: 0;">		
				<div class="main-wrap-inner">														
					<div class="main-wrap-inner-body">												
						<div class="message-response"><?php echo $this->Session->flash(); ?></div>
						<div class="before-content-layout"><?php echo $content_for_layout; ?></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>			
		</div>
		<?php echo $this->Js->writeBuffer(); ?>
	</body>
</html>