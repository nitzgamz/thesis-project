<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">	
    <head> 
        <title><?php echo $title_for_layout; ?></title>         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<?php echo $this->Html->css('bootstrap'); ?>
		<?php echo $this->Html->css('bootstrap.theme'); ?>
		<?php echo $this->Html->css('style.generic.default'); ?>		
		<?php echo $this->Html->css('thickbox'); ?>
		<?php echo $this->Html->script('jquery-1.7.1.min'); ?>		
		<?php echo $this->Html->script('bootstrap'); ?>				
	<body>
		<div class="title-header-page">
				<?php echo $this->element('main.menu'); ?>							
		</div>
		<div id="container">		
			<div clas="main-wrap-inner-header">	
				<div class="header-c">
					<?php //echo $this->element('logo.menu'); ?>					
					<div class="clear"></div>
				</div>
			</div>							
			<div class="main-wrap">		
				<div class="main-wrap-inner">														
					<div class="main-wrap-inner-body">												
						<?php //echo $this->element('logged.user'); ?>						
						<div class="message-response"><?php echo $this->Session->flash(); ?></div>
						<div class="before-content-layout"><?php echo $content_for_layout; ?></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>	
			<div class="footer">Principals Office Forms Management System V1.0</div>
		</div>
		<?php echo $this->Js->writeBuffer(); ?>
	</body>
</html>