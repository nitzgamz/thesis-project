<?php echo $this->element('submenu/student'); ?>
<div class="schoolgrades form">
<h3 class="default"><?php echo __('Upload Grades'); ?></h3>
<?php echo $this->Form->create('Schoolgrade', array('enctype' => 'multipart/form-data'));?>
	<fieldset>

	<?php
		//echo '<label for="">Select Quarter</label>';
		//echo $this->Form->input('quarter', array('options' => array('1' => 'First Quarter', '2' => 'Second Quarter', '3' => 'Third Quarter', '4' => 'Fourth Quarter'), 'label' => ''));		
		echo '<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left">';
		echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => ''));		
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => ''));		
		echo '<label for="">Teacher</label>';
		echo $this->Form->input('teacher_id', array('label' => ''));	
		echo '</div>';
		echo '<div class="col-md-3 col-lg-3 col-xs-3">';		
		echo '<label for="">School Year</label>';
		echo $this->Form->input('schoolyear_id', array('label' => ''));	
		echo '<label for="">Select File to Upload</label>';
		echo $this->Form->file('filetoupload', array('label' => ''));				
		echo $this->Form->input('user_id', array('default' => $userid, 'type' => 'hidden'));
		echo $this->Form->input('added', array('default' => date('Y-m-d'), 'type' => 'hidden'));
		echo $this->Form->submit(__('Upload File & Save'), array('class' => 'btn btn-primary')); 
		echo '</div>';
	?>
	</fieldset>
	<br />

</div>
<br /><br />

