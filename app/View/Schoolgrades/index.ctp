<div class="schoolgrades index">
	<?php echo $this->element('submenu/student'); ?>
	<h3 class="default"><?php echo __('School Grades Masterlist');?></h3>
	<?php echo $this->element('top.navigation.less.left'); ?>
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th class="actions"><?php echo __('Lastname');?></th>
			<th class="actions"><?php echo __('Firstname');?></th>
			<th class="actions"><?php echo __('Middlename');?></th>			
			<th><?php echo $this->Paginator->sort('gradelevel_id');?><span class="caret"></th>
			<th><?php echo $this->Paginator->sort('tgrade');?></th>
			<th><?php echo $this->Paginator->sort('ggrade');?></th>
			<th><?php echo $this->Paginator->sort('schoolyear_id');?><span class="caret"></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($schoolgrades as $schoolgrade): ?>
	<tr>		
		<td>
			<?php
				echo $schoolgrade['Student']['lastname'];
			?>
		</td>
		<td>
			<?php
				echo $schoolgrade['Student']['firstname'];
			?>
		</td>
		<td>
			<?php
				echo $schoolgrade['Student']['middlename'];
			?>
		</td>
		<td>
			<?php
				echo $schoolgrade['Gradelevel']['name'];
			?>
		</td>		
		<td><?php echo h($schoolgrade['Schoolgrade']['tgrade']); ?>&nbsp;</td>
		<td><?php echo h($schoolgrade['Schoolgrade']['ggrade']); ?>&nbsp;</td>
		<td><?php echo h($schoolgrade['Schoolyear']['name']); ?>&nbsp;</td>	
		<td class="actions">
			<?php echo $this->Html->link(__('Details'), array('action' => 'view', $schoolgrade['Schoolgrade']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $schoolgrade['Schoolgrade']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $schoolgrade['Schoolgrade']['id']), null, __('Are you sure you want to delete # %s?', $schoolgrade['Schoolgrade']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>

