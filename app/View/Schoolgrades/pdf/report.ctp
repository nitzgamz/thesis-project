<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
	<title>School Form 1</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
		html, body{margin: 20px 20px;}		
		/*table {page-break-before: always; page-break-after: always;}*/
		@page{
			margin-top: 5px;
			margin-left: 20px;
			margin-bottom: 5px;
			margin-right: 20px;
		}
		table{border-collapse:collapse;}
		table,tbody,tr,th,td{margin:0;padding:0;}
		/*th,td{white-space:nowrap;}*/
		 #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after { content: counter(page, upper-roman); }
	</style>
<body>	
<div id="header">
    <h1>ibmphp.blogspot.com</h1>
  </div>
  <div id="footer">
    
  </div>
<script type="text/php">
        if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(30, 18, "Page {PAGE_NUM} out of {PAGE_COUNT}", $font, 8, array(255,0,0));

        }
    </script>
<h2 style="text-align: center; font: bold 18px arial, heveltica, sans-serif; padding: 0; margin: 0;">
	DEPARTMENT OF EDUCATION
	<div style="font-size: 12px;">Caraga Administrative Region</div>
	<div style="font-size: 12px;">Bislig City Division</div>
	<div style="font-size: 12px;">Bislig II District</div>	
	BISLIG CENTRAL ELEMENTARY SCHOOL
	<div>GRADE SHEET</div>
</h2>	

<table style="font: bold 12px arial, heveltica, sans-serif; margin: 20px 0;" cellpadding="5" cellspacing="0" width="20%">
	<tr>		
		<td style="border: 1px solid #666;">GRADE</td><td style="border: 1px solid #666; text-transform: uppercase;"><?php echo $gradelevel['Gradelevel']['name']; ?></td>
	</tr>
	<tr>
		<td style="border: 1px solid #666;">SECTION</td><td style="border: 1px solid #666; text-transform: uppercase;"><?php echo $section['Gradesection']['name']; ?></td>
	</tr>
	<tr>
		<td style="border: 1px solid #666;">SCHOOL YEAR</td><td style="border: 1px solid #666;"><?php echo $sy['Schoolyear']['name']; ?></td>		
	</tr>	
</table>

<div style="">
<table style="font: bold 12px arial, heveltica, sans-serif; margin-top: 10px;" cellpadding="2" cellspacing="0" width="100%">
	<thead>
	<tr style="text-align: center; font-size: 10px;">
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="45">SUBJECTS</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>		
	</tr>
	<tr style="text-align: center; font-size: 10px;">
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">NAME</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">ENGLISH</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">MATHEMATICS</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">SCIENCE</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">FILIPINO</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">MAKABAYAN</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">EPP</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">HEKASI</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">MSEP</td>
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">VALUES ED.</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		<td style="border: 1px solid #000; border-bottom: 0;">GA</td>		
	</tr>
	<tr style="text-align: center; font-size: 10px;">
		<td style="border: 1px solid #000; border-bottom: 0;" colspan="5">&nbsp;</td>		
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>		
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>		
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>

		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>		
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>		
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>	
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		
		<td style="border: 1px solid #000; border-bottom: 0;">1</td>
		<td style="border: 1px solid #000; border-bottom: 0;">2</td>
		<td style="border: 1px solid #000; border-bottom: 0;">3</td>
		<td style="border: 1px solid #000; border-bottom: 0;">4</td>
		<td style="border: 1px solid #000; border-bottom: 0;">T</td>
		
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>				
		
	</tr>
	
	</thead>
	
	<tbody style="page-break-before: always;">	
		<?php $i=0; ?>
			<?php if(!empty($students)) { ?>
			<?php foreach($students as $student): ?>
			<?php
					$ginit = $student['Schoolgrade']['grades']; 
					$fg = explode(",", $ginit);
			?>
			<?php $i++; ?>			
				<tr style="page-break-before: always;">
						<td style="border-bottom: 1px solid #000; border-left: 1px solid #000;"><?php echo $i; ?></td>
						<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['lastname']; ?></td>
						<td style="border-bottom: 1px solid #000;">,</td>
						<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['firstname']; ?></td>
						<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['middlename']; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[1]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[2]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[3]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[4]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[5]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[7]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[8]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[9]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[10]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[11]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[13]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[14]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[15]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[16]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[17]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[19]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[20]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[21]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[22]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[23]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[25]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[26]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[27]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[28]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[29]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[31]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[32]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[33]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[34]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[35]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[37]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[38]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[39]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[40]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[41]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[43]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[44]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[45]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[46]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[47]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo $fg[49]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[50]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[51]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[52]; ?></td>
						<td style="border: 1px solid #000;"><?php echo $fg[53]; ?></td>
						
						<td style="border: 1px solid #000;"><?php echo number_format($student['Schoolgrade']['tgrade'], 0, 0, ''); ?></td>
						<td style="border: 1px solid #000;"><?php echo number_format($student['Schoolgrade']['ggrade'], 0, 0, ''); ?></td>
						
				</tr>
			<?php endforeach; ?>
			<?php } ?>
	</tbody>
</table>
</div>
<table style="font: bold 12px arial, heveltica, sans-serif; margin-top: 50px;" cellpadding="5" cellspacing="2" width="25%">
	
	<tr><td>PREPARED BY:</td></tr>
	<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
	<tr><td style="text-align: center;">Teacher</td></tr>	
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>NOTED BY:</td></tr>
	<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
	<tr><td style="text-align: center;">Principal I</td></tr>	

	
</table>
</body>
</html>