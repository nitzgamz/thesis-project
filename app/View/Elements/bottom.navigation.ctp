<div class="label label-primary pull-left">
		<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages} &raquo; showing {:current} records out of {:count} total &raquo; starting on record {:start} &raquo; ending on {:end}')
		));
		?>
</div>
<div class="clear"></div>
<ul class="pagination pagination-sm pull-left">
	<?php		
		echo '<li>'.$this->Paginator->prev(__(' << Previous'), array(), null, array('class' => 'prev-disabled')).'</li>';
		echo '<li>'.$this->Paginator->first('First Page', array(), null, array('class' => 'first')).'</li>';
		echo '<li>'.$this->Paginator->numbers(array('separator' => '', )).'</li>';
		echo '<li>'.$this->Paginator->last('Last Page').'</li>';
		echo '<li>'.$this->Paginator->next(__(' Next >> '), array(), null, array('class' => 'next-disabled')).'</li>';
	?>
</ul>
<div class="clear"></div>
