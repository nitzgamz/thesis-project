<?php $user = $this->requestAction('users/loggeduser'); ?>
<div id="navigation2" style="padding-left: 0">	
	<div class="btn-group">		
		<div class="btn btn-primary"><?php echo $this->Html->link('Register', array('controller' => 'teachers', 'action' => 'add'), array('class' => 'danger')); ?></div>
		<div class="btn btn-primary"><?php echo $this->Html->link('Browse', array('controller' => 'teachers', 'action' => 'index'), array('class' => 'danger')); ?></div>				
		<div class="btn btn-primary"><?php echo $this->Html->link('Tag / Assign Subject', array('controller' => 'teachersubjects', 'action' => 'add'), array('class' => 'danger')); ?></div>				
		<div class="btn btn-primary dropdown">
			 <?php echo $this->Html->link('Subjects', array(), array('class' => 'danger')); ?>									
			<ul class="dropdown-menu" role="menu">			  
			  <li><?php echo $this->Html->link('Browse', array('controller' => 'schoolsubjects', 'action' => 'index'), array('class' => '')); ?>		</li>			  
			  <li><?php echo $this->Html->link('Add New', array('controller' => 'schoolsubjects', 'action' => 'add'), array('class' => '')); ?>		</li>			  			  			  
			</ul>
		 </div> 
	</div>	
</div>
