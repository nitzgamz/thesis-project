<?php $user = $this->requestAction('users/loggeduser'); ?>
<div id="navigation2" style="padding-left: 0">	
	<div class="btn-group">
		<div class="btn btn-primary"><?php echo $this->Html->link('Browse', array('controller' => 'students', 'action' => 'index'), array('class' => 'danger')); ?></div>		
		<div class="btn btn-primary"><?php echo $this->Html->link('Batch Upload', array('controller' => 'students', 'action' => 'batchupload'), array('class' => 'danger')); ?></div>
		<div class="btn btn-primary"><?php echo $this->Html->link('Add New', array('controller' => 'students', 'action' => 'add'), array('class' => 'danger')); ?></div>		
		
		<div class="btn btn-primary dropdown">
			 <?php echo $this->Html->link('Code of Indicators', array(), array('class' => 'danger')); ?>									
			<ul class="dropdown-menu" role="menu">			  
			  <li><?php echo $this->Html->link('Browse', array('controller' => 'studentindicators', 'action' => 'index'), array('class' => '')); ?>		</li>
			  <li><?php echo $this->Html->link('Add New', array('controller' => 'studentindicators', 'action' => 'add'), array('class' => '')); ?>		</li>			  
			</ul>
		 </div> 
		<div class="btn btn-primary dropdown">
			 <?php echo $this->Html->link('Grades', array(), array('class' => 'danger')); ?>									
			<ul class="dropdown-menu" role="menu">			  
			  <li><?php echo $this->Html->link('Browse', array('controller' => 'schoolgrades', 'action' => 'index'), array('class' => '')); ?>		</li>
			  <li><?php echo $this->Html->link('Upload Grades', array('controller' => 'schoolgrades', 'action' => 'upload'), array('class' => '')); ?>		</li>			  
			</ul>
		 </div> 
		<div class="btn btn-primary dropdown">
			 <?php echo $this->Html->link('Summary Reports', array(), array('class' => 'danger')); ?>									
			<ul class="dropdown-menu" role="menu">			  
			  <li><?php echo $this->Html->link('School Form 137', array('controller' => 'schoolgrades', 'action' => 'reportinitiate'), array('class' => '')); ?>		</li>			  
			  <li><?php echo $this->Html->link('School Form 1 ( SF 1 )', array('controller' => 'students', 'action' => 'form1report'), array('class' => '')); ?>		</li>			  			  
			  <li><?php echo $this->Html->link('School Form 5 ( SF 5 )', array('controller' => 'students', 'action' => 'reportinitiate'), array('class' => '')); ?>		</li>			  
			  <li><?php echo $this->Html->link('School Form 6 ( SF 56 )', array('controller' => 'gradelevels', 'action' => 'reportinitiate'), array('class' => '')); ?>		</li>			  
			</ul>
		 </div> 
	</div>	
</div>
