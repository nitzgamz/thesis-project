<?php $user = $this->requestAction('users/loggeduser'); ?>
<div style="text-align: left; padding: 20px 0;">	
	<?php echo $this->Html->link('School Form 137', array('controller' => 'schoolgrades', 'action' => 'reportinitiate'), array('class' => 'btn btn-lg btn-success white-link')); ?>
	<br /><br />
	<?php echo $this->Html->link('School Form 1 ( SF 1 ) School Register', array('controller' => 'students', 'action' => 'form1report'), array('class' => 'btn btn-lg btn-success white-link')); ?>
	<br /><br />
	<?php echo $this->Html->link('School Form 5 ( SF 5 ) Report on Promotion & Level of Proficiency', array('controller' => 'students', 'action' => 'reportinitiate'), array('class' => 'btn btn-lg btn-success white-link')); ?>
	<?php //echo $this->Html->link('School Form 5 ( SF 5 ) Report on Promotion & Level of Proficiency', array('controller' => 'students', 'action' => 'report'), array('class' => 'btn btn-lg btn-success white-link')); ?>
	<br /><br />
	<?php echo $this->Html->link('School Form 6 ( SF 6 ) Summarized Report on Promotion & Level of Proficiency', array('controller' => 'gradelevels', 'action' => 'reportinitiate'), array('class' => 'btn btn-lg btn-success white-link')); ?>
</div>
