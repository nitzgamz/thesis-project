<?php
	$this->Js->get($event_id)->event('keyup',
		$this->Js->request(
				array('controller' => $controller_id, 'action' => $action_id), 
				array(				
					'update' => $update_id,
					'method' => $method,
					'async' => true,					
					'data' => $this->Js->serializeForm(array('isForm' => true, 'inline' => true)),
					'dataExpression' => true
				)
		)
	);
?>