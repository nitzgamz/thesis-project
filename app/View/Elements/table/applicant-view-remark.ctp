<div class="related">
	<h2><?php echo __('Comments / Remarks '); ?></h2>
	<?php echo $this->element('table/table-header', array('headers' => array('Remark / Comment / Additional Information', 'Started By'), 'navigation' => false)); ?>
	<?php if (!empty($details)){ ?>
	<?php 
		$i = 0;
		foreach ($details as $applicantdetail):
		$i++;
		echo $this->element('table/table-information', array('i' => $i));
	?>		
		<?php 			
			echo '<td class="remark-table">'.$applicantdetail['remarks'].'</td>';			
			echo $this->element('table/table-related', array(
				'fields' => array(						
					'added_by',					
				),
				'controller' => $applicantdetail
			)); 
		?>			
			
		</tr>
	<?php endforeach; ?>
	<?php }else{ ?>
		<?php echo $this->element('table/empty-related', array('outputext' => 'No Comment / Remark', 'colspan' => 5)); ?>
	<?php } ?>
	<?php echo $this->element('table/table-footer', array('navigation' => false)); ?>		
</div>