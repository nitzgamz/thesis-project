<div class="page-status">
	<div class="page-status-inner">
		<?php echo __('Total Applicants Found &raquo; ').count($applicants); ?>
	</div>
</div>	
<div class="paging">	
	<div class="inside-paging">
	<?php		
		
	?>
	</div>	
	<div class="search-form">		
		<?php			
				echo $this->Form->create($model, array('default' => false, 'id' => 'ApplicantexaminationForm'), array('id' => 'ApplicantexaminationForm'));
				echo $this->Form->input('search', array('class' => 'normalinput', 'label' => '', 'placeholder' => 'Search Applicant', 'id' => 'search'), array('id' => 'search'));				
				echo $this->Form->submit('Find', array('class' => 'searchbtn', 'title' => 'Search'));			
				
				$this->Js->get('#search')->event('keyup',
						$this->Js->request(
								array('controller' => $controller, 'action' => 'addapplicant/'.$examination['Examination']['id'].'/'.$examination['Examination']['region_id'].'/'.$examination['Examination']['area_id']), 
								array(				
									'update' => '#searchapplicantresult',
									'method' => 'POST',
									'async' => true,					
									'data' => $this->Js->serializeForm(array('isForm' => true, 'inline' => true)),
									'dataExpression' => true
								)
						)
					);
		?>
	</div>
	<div class="clear"></div>
</div>
