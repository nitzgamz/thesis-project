<div class="result-box">
	<h2 style="margin: 0;"><?php echo __('File Attachment'); ?></h2>
	<div class="inner">
	<?php echo $this->element('table/table-header', array('headers' => array('Filename', 'Description', 'Uploaded By', 'Modified By', 'Actions'), 'navigation' => false)); ?>
	<?php if (!empty($attachments)){ ?>
	<?php 
		$i=0;
		foreach ($attachments as $attachment): 
		$i++;
		echo $this->element('table/table-information', array('i' => $i));
	?>		
			<?php
				echo $this->element('table/table-related', array(
					'fields' => array(
						'attachment',
						'description',
						'added_by',
						'modified_by'
					),
					'controller' => $attachment,
				));
			?>
			<td class="actions">
				<?php echo $this->Html->link(__('Download'), array('controller' => 'attachments', 'action' => 'download', $attachment['id']), array('class' => 'view', 'title' => 'Download file attachment')); ?>
				<?php //echo $this->Html->link(__('Update'), array('controller' => 'attachments', 'action' => 'edit', $attachment['id']), array('class' => 'edit')); ?>				
			</td>
		</tr>
	<?php endforeach; ?>
	<?php }else{ ?>
		<?php echo $this->element('table/empty-related', array('outputext' => 'No Attachment', 'colspan' => 5)); ?>
	<?php } ?>
	<?php echo $this->element('table/table-footer', array('navigation' => false)); ?>	
</div>
</div>