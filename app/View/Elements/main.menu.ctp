<?php $user = $this->requestAction('users/loggeduser'); ?>
<div id="navigation2">	
	<div class="btn-group pull-left">
		<div class="btn btn-primary"><?php echo $this->Html->link('Students', array('controller' => 'students', 'action' => 'index'), array('class' => 'danger')); ?></div>
		<div class="btn btn-primary"><?php echo $this->Html->link('Teachers', array('controller' => 'teachers', 'action' => 'index'), array('class' => 'danger')); ?></div>
		<div class="btn btn-primary"><?php echo $this->Html->link('Grade Levels', array('controller' => 'gradelevels', 'action' => 'index'), array('class' => 'danger')); ?></div>
		<div class="btn btn-primary"><?php echo $this->Html->link('Sections', array('controller' => 'gradesections', 'action' => 'index'), array('class' => 'danger')); ?></div>				
		<div class="btn btn-primary"><?php echo $this->Html->link('S.Y.', array('controller' => 'schoolyears', 'action' => 'index'), array('class' => 'danger')); ?></div>		
		<div class="btn btn-success"><?php echo $this->Html->link('Reports', array('controller' => 'reports', 'action' => 'index'), array('class' => 'danger')); ?></div>		
	</div>
	<div class="btn-group pull-right">				
		<div class="btn btn-danger"><?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'), array('class' => 'danger')); ?></div>				
	</div>
</div>
<div class="clear"></div>
