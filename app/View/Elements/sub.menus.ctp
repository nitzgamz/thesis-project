<?php if(!empty($menus) && !empty($controller) && !empty($action) && !empty($data)){ ?>
<div class="below-table-link">
	<?php 
		foreach($menus as $key => $menu):
			echo $this->Html->link($menu, array('controller' => $controller[$key], 'action' => $action[$key], $data[$key]), array('title' => $hints[$key]));
		endforeach;
	?>
	<div class="clear"></div>
</div>
<?php } ?>