<!-- Small modal -->
<div class="login-navigation">	
	<div class="client-login">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Administrator Login</button>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Account Details</h4>
						  </div>
						  
						  
									<div class="login-content">	
										<div class="login-inner-content">											
											<?php echo $this->Form->create('User', array('action' => 'login')); ?>
											<div class="div-separator-2"><?php echo __('Username'); ?></div>
											<div class="div-separator-2">
												<?php echo $this->Form->input('username', array('class' => 'normalinputfield', 'label' => '')); ?>
											</div>
											<div class="div-separator-2"><?php echo __('Password'); ?></div>
											<div class="div-separator-2">
												<?php echo $this->Form->input('password', array('class' => 'normalinputfield', 'label' => '')); ?>
											</div>
											<div class="div-separator">
												<?php echo $this->Form->submit('Login', array('class' => 'btn btn-primary', 'title' => 'Login')); ?>												
											</div>		
										</div>	
									</div>
    </div>
  </div>
</div>
</div>
</div>



