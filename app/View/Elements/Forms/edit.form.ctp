<div class="form-title"><h2><?php if(!empty($formtitle)){ echo $formtitle; }else{ echo __('Add / Edit Form'); }?></h2></div>
<div class="groups-form">
<table cellpadding="0" cellscpacing="0" style="border: none;">
<?php
	echo $this->Form->create($model, array('enctype' => 'multipart/form-data', 'onsubmit' => 'return confirm("The system is about to process a certain data, are you sure you want to continue?");'));
	foreach($inputs as $key => $input):
		echo '<tr class="table-form-label">';
			if(!empty($labels[$key])){
				if($labels[$key]!=NULL || $labels[$key]!=null){
					echo '<td class="text-label">'.$labels[$key].'</td>';
				}
			}
			echo '<td class="text-input">';
			echo '<div>';
				switch($input){					
					case "region_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false, 
								'id' => 'region_id'
							), array(
								'class' => 'input'
							));
						
					break;
					case "area_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false, 
								'id' => 'area_id'
							), array(
								'class' => 'input'
							));
					
					break;
					case "course_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false,  
								'id' => 'area_id'
							), array(
								'class' => 'input'
							));
				
					break;
					case "coursecategory_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false, 
								'id' => 'coursecategory_id'
							), array(
								'class' => 'input'
							));
						
					break;
					case "applicant_id":
						echo $this->Form->input($input, array(
							'label' => '', 							
							'id' => 'applicant_id',		
							'type' => 'hidden',
							'default' => $applicantid
						), array(
							'class' => 'input'
						));
					break;
					case "birthdate":
						
							echo $this->Form->input($input, array(
								'type' => 'date',
								'label' => '', 
								'minYear' => date('Y') - 45,
								'maxYear' => date('Y'),							
							), array(
								'class' => 'input'
							));
					
					break;
					case "gender":
						
							echo $this->Form->radio($input, array(
									'M' => 'Male', 
									'F' => 'Female'
								), 
								array(							
									'label' => '', 
									'legend' => false,
									'id' => 'gender'
							), array(
								'class' => 'input'
							));
					
					break;
					case "added_by":
						$user = $this->requestAction('users/loggeduser');
						echo $this->Form->input($input, array(
								'default' => $user['User']['id'],
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "modified_by":
						$user = $this->requestAction('users/loggeduser');
						echo $this->Form->input($input, array(
								'value' => $user['User']['id'],
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "modified":
						echo $this->Form->input($input, array(
								'default' => date('Y-m-d'),
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "added":
						echo $this->Form->input($input, array(
								'default' => date('Y-m-d'),
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "filetoupload":
						
						echo $this->Form->input($input, array(								
								'label' => '',
								'type' => 'file'
							), array(
								'class' => 'input'
							)
						);
						
					break;
					default:						
						
								echo $this->Form->input($input, array('label' => '', 'placeholder' => $labels[$key]), array('class' => 'input'));
					
					break;
				}	
			echo '</div>';
			echo '<div class="text-hints">';
			if(!empty($hints[$key])){
				if($hints[$key]!=NULL || $hints[$key]!=null){
					echo $this->Html->image('icons/info.png').' '.$hints[$key];
				}
			}
			echo '</div>';
			echo '</td>';
		echo '</tr>';
	endforeach;
?>
</table>
</div>
<div class="end-form">
<?php
	echo $this->Form->submit('Submit', array('class' => 'submitbtn', 'title' => 'Submit Information'));
	echo $this->Form->button('Cancel', array('type' => 'Reset', 'class' => 'cancelbtn', 'title' => 'Cancel Information'));
	echo '<div class="clear"></div>';
?>
</div>
