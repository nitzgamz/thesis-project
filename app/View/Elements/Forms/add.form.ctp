<div class="form-title"><h2><?php if(!empty($formtitle)){ echo $formtitle; }else{ echo __('Add / Edit Form'); }?></h2></div>
<div class="groups-form">
<table cellpadding="0" cellscpacing="0" style="border: none;">
<?php
	$t_op_first = array();
	for($i=0; $i <=100; $i++){
		$t_op_first[] = $i;
	}
	
	$t_op_scale = array();
	for($i=0; $i <=10; $i++){
		$t_op_scale[] = $i;
	}
	
	echo $this->Form->create($model, array('enctype' => 'multipart/form-data', 'onsubmit' => 'return confirm("The system is about to process a certain data, are you sure you want to continue?");'));
	foreach($inputs as $key => $input):
		echo '<tr class="table-form-label">';
			if(!empty($labels[$key])){
				if($labels[$key]!=NULL || $labels[$key]!=null){
					echo '<td class="text-label">'.$labels[$key].'</td>';
				}
			}
			echo '<td class="text-input">';
			echo '<div>';
				switch($input){					
					case "region_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false, 
								'id' => 'region_id'
							), array(
								'class' => 'input'
							));
						
					break;
					case "area_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false, 
								'id' => 'area_id'
							), array(
								'class' => 'input'
							));
						
					break;
					case "course_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false, 
								'id' => 'area_id'
							), array(
								'class' => 'input'
							));
						
					break;
					case "coursecategory_id":
						
							echo $this->Form->input($input, array(
								'label' => '', 
								'empty' => false, 
								'id' => 'coursecategory_id'
							), array(
								'class' => 'input'
							));
						
					break;
					case "applicant_id":
						echo $this->Form->input($input, array(
							'label' => '', 							
							'id' => 'applicant_id',		
							'type' => 'hidden',
							'default' => $applicantid
						), array(
							'class' => 'input'
						));
					break;
					case "birthdate":
						
							echo $this->Form->input($input, array(
								'type' => 'date',
								'label' => '', 
								'minYear' => date('Y') - 45,
								'maxYear' => date('Y'),							
							), array(
								'class' => 'input'
							));
						
					break;
					case "gender":
						
							echo $this->Form->radio($input, array(
									'M' => 'Male', 
									'F' => 'Female'
								), 
								array(							
									'label' => '', 
									'legend' => false,
									'id' => 'gender'
							), array(
								'class' => 'input'
							));
						
					break;
					case "added_by":
						$user = $this->requestAction('users/loggeduser');
						echo $this->Form->input($input, array(
								'default' => $user['User']['id'],
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "modified_by":
						$user = $this->requestAction('users/loggeduser');
						echo $this->Form->input($input, array(
								'value' => $user['User']['id'],
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "modified":
						echo $this->Form->input($input, array(
								'default' => date('Y-m-d'),
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "added":
						echo $this->Form->input($input, array(
								'default' => date('Y-m-d'),
								'label' => '',
								'type' => 'hidden'
							), array(
								'class' => 'input'
							)
						);
					break;
					case "filetoupload":						
						echo $this->Form->input($input, array(								
								'label' => '',
								'type' => 'file'
							), array(
								'class' => 'input'
							)
						);
						
					break;
					case "checkbox1":
						if($display_option_box){
							echo '<div class="checkbox">'.$this->Form->radio('checkbox1', array('1' => 'Similar, but different person'), array('label' => ''), array('class' => 'checkbox')).'</div>';
						}else{
							echo '<div class="checkbox1">'.$this->Form->radio('checkbox1', array('2' => 'New Applicant'), array('label' => '', 'checked' => 'checked'), array('class' => 'checkbox')).'</div>';
						}
					break;
					case "low_number":												
						echo $this->Form->input('low_number', array('options' => $t_op_first, 'class' => 'normalinput', 'label' => ''));
					break;
					case "high_number":												
						echo $this->Form->input('high_number', array('options' => $t_op_first, 'class' => 'normalinput', 'label' => ''));
					break;
					case "stenscale":												
						echo $this->Form->input('stenscale', array('options' => $t_op_scale, 'class' => 'normalinput', 'label' => ''));
					break;
					case "personalityscale_id":
						echo $this->Form->input('personalityscale_id', array('class' => 'normalinput', 'label' => '', 'empty' => ' -- Choose Scale -- '));
					break;
					case "personalityfactor_id":
						echo $this->Form->input('personalityfactor_id', array('class' => 'normalinput', 'label' => '', 'empty' => ' -- Choose Scale -- '));
					break;
					case "setexamination_id":
						if(!empty($examinationlists)){
							echo '<select name="data[Applicant][setexamination_id]">';
								echo '<option value="">---- Do not set ----</option>';
							foreach($examinationlists as $exam):
								echo '<option value="'.$exam['Examination']['id'].'">'.date('D - F d, Y', strtotime($exam['Examination']['examination_date'])).__(' ( ').$exam['Region']['name'] .__(' - '). $exam['Area']['name'].__(' ) ').'</option>';
							endforeach;
							echo '</select>';
						}else{
							echo $this->Form->input($input, array('class' => 'normalinput', 'label' => '', 'empty' => '--- Do not include ---'));
						}
					break;
					default:						
						echo $this->Form->input($input, array('label' => '', 'placeholder' => $labels[$key]), array('class' => 'input'));						
					break;
				}
			echo '</div>';
			echo '<div class="text-hints">';
			if(!empty($hints[$key])){
				if($hints[$key]!=NULL || $hints[$key]!=null){
					echo $this->Html->image('icons/info.png').' '.$hints[$key];
				}
			}
			echo '</div>';
			echo '</td>';
		echo '</tr>';
	endforeach;
?>
</table>
</div>
<div class="end-form">
<?php
	echo $this->Form->submit('Submit', array('class' => 'submitbtn', 'title' => 'Submit Information'));
	echo $this->Form->button('Cancel', array('type' => 'Reset', 'class' => 'cancelbtn', 'title' => 'Cancel Information', 'onclick' => "location.href='".$this->Html->url('../'.$this->params['controller'].'/')."'"));
	echo '<div class="clear"></div>';
?>
</div>
