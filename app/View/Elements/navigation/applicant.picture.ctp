<?php
	if(!empty($applicantpicture)){
		echo '<div style="margin: 5px 0;">';
		echo '<div class="form-title"><h2>';
			if(!empty($formtitle)){ echo $formtitle; }else{ echo __('Current Profile Picture'); }
		echo '</h2></div>';
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
			echo '<tr class="table-header">';
				foreach(array(
					"Profile Picture", 					
				) as $header):				
					echo '<th>'.$header.'</th>';
				endforeach;
			echo '</tr>';
			echo '<tr class="table-information">';
				echo '<td>';
					if(file_exists(APP.'webroot/applicant/picture/'.$applicantpicture['Applicant']['region_id'].'/'.$applicantpicture['Applicant']['area_id'].'/'.$applicantpicture['Applicantpicture']['filename'])){
						echo '<img src="'.$this->webroot.'/applicant/picture/'.$applicantpicture['Applicant']['region_id'].'/'.$applicantpicture['Applicant']['area_id'].'/'.$applicantpicture['Applicantpicture']['filename'].'"  style="width: '.$width.'px; height: '.$height.'px;" />';						
					}else{
						echo '<div style="color: red;">'.__('The image was not found').'</div>';
					}	
				echo '</td>';
			echo '</tr>';
		echo '</table>';		
		echo '</div>';
	}
?>