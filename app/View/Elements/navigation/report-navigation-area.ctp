<?php
	if(!empty($type) && !empty($date)){	
		switch($type){
			case "Annual":
				$dated = date('Y', strtotime($date));
				$dateinput = 'Y';
				$previous_link = $this->Html->link('Show Previous Year', array('controller' => 'areas', 'action' => 'report', $areaid, 'annual', date('Y-m-d', strtotime($date.'- 1 year'))));
				$next_link = $this->Html->link('Show Next Year', array('controller' => 'areas', 'action' => 'report', $areaid, 'annual', date('Y-m-d', strtotime($date.'+ 1 year'))));
			break;
			case "Monthly":
				$dated = date('F Y', strtotime($date));
				$dateinput = 'MY';
				$previous_link = $this->Html->link('Show Previous Month', array('controller' => 'areas', 'action' => 'report', $areaid, 'monthly', date('Y-m-d', strtotime($date.'- 1 month'))));
				$next_link = $this->Html->link('Show Next Month', array('controller' => 'areas', 'action' => 'report', $areaid, 'monthly', date('Y-m-d', strtotime($date.'+ 1 month'))));
			break;
		}
?>
	
			<div class="report-date"> Report Month / Year ( <?php echo $dated; ?> )</div>
<?php
			echo $this->Form->create('Searchdate', array());
			echo $this->Form->input('type', array('type' => 'hidden', 'default' => $type));
			echo $this->Form->input('regionid', array('type' => 'hidden', 'default' => $areaid));
			echo '<table style="width: 420px;" cellpadding="0" cellspacing="1">';
				echo '<tr class="table-information">';
					echo '<td>'.$this->Form->input('fdate', array('type' => 'date', 'label' => 'Select Month / Year to Sort ', 'dateFormat' => $dateinput, 'maxYear' => date('Y'), 'minYear' => date('Y') - 10)).'</td>';
					echo '<td>'.$this->Form->submit('Generate Report', array('class' => 'normalbtns')).'</td>';				
				echo '</tr>';
			echo '</table>';
			echo '<div class="mreport">'.$previous_link.'</div>';
			echo '<div class="mreport">'.$next_link.'</div>';
			echo '<div class="clear"></div>';
	}
		
?>