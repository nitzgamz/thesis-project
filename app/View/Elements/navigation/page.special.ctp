<div class="page-status">
	<div class="page-status-inner">
		<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages} &raquo; showing {:current} records out of {:count} total &raquo; starting on record {:start} &raquo; ending on {:end}')
		));
		?>
	</div>
</div>
<div class="paging">	
	<div class="inside-paging">
	<?php		
		$prev_img_active = $this->Html->image("menus/previousl-button04.png", array('escape' => false, 'class' => 'previous'));
		$prev_img_inactive = $this->Html->image("menus/previousl-button03.png", array('escape' => false, 'class' => 'previous'));
		$next_img_active = $this->Html->image("menus/next-button04.png", array('escape' => false, 'class' => 'next'));
		$next_img_inactive = $this->Html->image("menus/next-button03.png", array('escape' => false, 'class' => 'next'));
		
		echo $this->Paginator->prev(!$this->Paginator->hasPrev() ? $prev_img_inactive : $prev_img_active, array('escape' => false, 'class' => 'previous'));
		
		//echo $this->Paginator->prev(__($this->Html->image('menus/previousl-button04.png')), array(), null, array('class' => 'prev-disabled', 'escape' => false));
		//echo $this->Paginator->first('First Page', array(), null, array('class' => 'first'));
		
		//echo $this->Paginator->numbers(array('separator' => '', ));
		//echo $this->Paginator->last('Last Page', array(), null, array('class' => 'last'));
		//echo $this->Paginator->next(__($this->Html->image('menus/next-button04.png')), array(), null, array('class' => 'next-disabled', 'escape' => false));
		
		echo $this->Paginator->next(!$this->Paginator->hasNext() ? $next_img_inactive : $next_img_active, array('class' => 'next', 'escape' => false));
	?>
	</div>	
	<div class="search-form">		
		<?php
			if(!empty($model) && !empty($controller) && !empty($action)){
				echo $this->Form->create($model, array(
					'url' => array(
						'controller' => $controller, 
						'action' => $action,
					)
				));
				echo $this->Form->input('search', array('class' => 'normalinput', 'label' => '', 'placeholder' => 'Search '.$model));
				echo $this->Form->submit('Find', array('class' => 'searchbtn', 'title' => 'Search'));
			}
		?>
	</div>
	<div class="clear"></div>
</div>