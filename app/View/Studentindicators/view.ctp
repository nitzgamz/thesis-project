<div class="studentindicators view">
<h2><?php  echo __('Studentindicator');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($studentindicator['Studentindicator']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($studentindicator['Studentindicator']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($studentindicator['Studentindicator']['code']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Studentindicator'), array('action' => 'edit', $studentindicator['Studentindicator']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Studentindicator'), array('action' => 'delete', $studentindicator['Studentindicator']['id']), null, __('Are you sure you want to delete # %s?', $studentindicator['Studentindicator']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Studentindicators'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Studentindicator'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Students');?></h3>
	<?php if (!empty($studentindicator['Student'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Lrn'); ?></th>
		<th><?php echo __('Firstname'); ?></th>
		<th><?php echo __('Middlename'); ?></th>
		<th><?php echo __('Lastname'); ?></th>
		<th><?php echo __('Gender'); ?></th>
		<th><?php echo __('Birthdate'); ?></th>
		<th><?php echo __('Mother Tongue'); ?></th>
		<th><?php echo __('Ethnic Group'); ?></th>
		<th><?php echo __('Religion'); ?></th>
		<th><?php echo __('Address House'); ?></th>
		<th><?php echo __('Address Brgy'); ?></th>
		<th><?php echo __('Address Municipal'); ?></th>
		<th><?php echo __('Address Province'); ?></th>
		<th><?php echo __('Father Fullname'); ?></th>
		<th><?php echo __('Mother Fullname'); ?></th>
		<th><?php echo __('Guardian Fullname'); ?></th>
		<th><?php echo __('Relationship'); ?></th>
		<th><?php echo __('Contact Number'); ?></th>
		<th><?php echo __('Studentindicator Id'); ?></th>
		<th><?php echo __('Added'); ?></th>
		<th><?php echo __('Added By'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Modified By'); ?></th>
		<th><?php echo __('Gradelevel Id'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($studentindicator['Student'] as $student): ?>
		<tr>
			<td><?php echo $student['id'];?></td>
			<td><?php echo $student['lrn'];?></td>
			<td><?php echo $student['firstname'];?></td>
			<td><?php echo $student['middlename'];?></td>
			<td><?php echo $student['lastname'];?></td>
			<td><?php echo $student['gender'];?></td>
			<td><?php echo $student['birthdate'];?></td>
			<td><?php echo $student['mother_tongue'];?></td>
			<td><?php echo $student['ethnic_group'];?></td>
			<td><?php echo $student['religion'];?></td>
			<td><?php echo $student['address_house'];?></td>
			<td><?php echo $student['address_brgy'];?></td>
			<td><?php echo $student['address_municipal'];?></td>
			<td><?php echo $student['address_province'];?></td>
			<td><?php echo $student['father_fullname'];?></td>
			<td><?php echo $student['mother_fullname'];?></td>
			<td><?php echo $student['guardian_fullname'];?></td>
			<td><?php echo $student['relationship'];?></td>
			<td><?php echo $student['contact_number'];?></td>
			<td><?php echo $student['studentindicator_id'];?></td>
			<td><?php echo $student['added'];?></td>
			<td><?php echo $student['added_by'];?></td>
			<td><?php echo $student['modified'];?></td>
			<td><?php echo $student['modified_by'];?></td>
			<td><?php echo $student['gradelevel_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'students', 'action' => 'view', $student['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'students', 'action' => 'edit', $student['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'students', 'action' => 'delete', $student['id']), null, __('Are you sure you want to delete # %s?', $student['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
