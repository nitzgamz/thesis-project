<?php echo $this->element('submenu/subjects'); ?>
<h3 class="default"><?php echo __('Register Section'); ?></h3>
<div class="gradesections form">
<?php echo $this->Form->create('Gradesection');?>
	<fieldset>
	
	<?php
		echo '<label for="">Select Grade</label>';
		echo $this->Form->input('gradelevel_id', array('label' => ''));
		echo '<label for="">Name</label>';
		echo $this->Form->input('name', array('label' => ''));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
</div>
