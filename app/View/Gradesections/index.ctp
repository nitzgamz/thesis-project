<div class="gradesections index">
	<?php echo $this->element('submenu/subjects'); ?>
	<h3 class="default"><?php echo __('Sections');?></h3>	
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th class="actions"><?php echo __('Grade Level');?></th>
			<th class="actions"><?php echo __('Teachers');?></th>
			<th class="actions"><?php echo __('No. of Student');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($gradesections as $gradesection): ?>
	<tr>		
		<td><?php echo h($gradesection['Gradesection']['name']); ?>&nbsp;</td>
		<td><?php echo h($gradesection['Gradelevel']['name']); ?>&nbsp;</td>
		<td>
			<?php 
				$teachers = $this->requestAction(array('controller' => 'teachers', 'action' => 'getallteachersinsections', $gradesection['Gradesection']['id']));
				if(!empty($teachers)){
					echo '<ul>';
					foreach($teachers as $teacher):
						echo '<li>'.$teacher['Teacher']['title'] .' '. $teacher['Teacher']['firstname'] .' '.$teacher['Teacher']['lastname'].'</li>';
					endforeach;
					echo '</ul>';
				}else{
					echo '0';
				}
			 ?>
		</td>
		<td>
			<?php 
				$students = $this->requestAction(array('controller' => 'students', 'action' => 'getallstudentsinsection', $gradesection['Gradesection']['id']));
				if(!empty($students)){
					echo count($students);
				}else{
					echo '0';
				}
			 ?>
		</td>
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $gradesection['Gradesection']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $gradesection['Gradesection']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $gradesection['Gradesection']['id']), null, __('Are you sure you want to delete # %s?', $gradesection['Gradesection']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>

