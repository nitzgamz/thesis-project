<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */
/******************************************
* TCPDF INCLUDE FILES					  *
******************************************/
require_once("tcpdf.php");
require_once("config/lang/eng.php");

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('LMS Administrator');
$pdf->SetTitle($course->name);
$pdf->SetSubject($quiz->name);
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING);
$pdf->SetHeaderData('logo_3.png', '42', '', '');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('Arial', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();


// Set some content to print  
    // Get this user's attempts.
  
// Print table with existing attempts

$html .='<div style="font-size: 50px; font-weight: bold;">'.$quiz->name.'</div>
<div style="font-size: 30px; font-weight: bold; margin-top: 0;">Type : '.$course->fullname.'</div>
<div style="font-size: 30px;">
Name: <font style="color: blue; font-weight: bold;">'.strtoupper($USER->lastname) .' , '. strtoupper($USER->firstname).'</font><br />
Region / Area: <font style="color: blue; font-weight: bold;">'.strtoupper($USER->city).'</font><br />
Department / Branch: <font style="color: blue; font-weight: bold;">'.strtoupper($USER->department).'</font>
</div>
<div style="marign-top: 2px; padding: 5px 0; border-top: 1px solid #666; border-bottom: 1px solid #666;">Assessment Summary Report</div>';
//$html .='<div class="water_mark" style="position: absolute; border: 1px solid red; width: 30%; height: 30%;">';
$pdf->Image('tcpdf/images/ml_water_mark.png', 15, 10, 210, 297, '', '', 'http://bos.mlhuillier1.com:4000/mod/quiz/view.php?id='.$_GET['id'].'', false, 300, '', false, false, 0);
//$pdf->Image('../images/image_with_alpha.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
//$html .='<img src="tcpdf/images/ml_water_mark.png" style="margin-top: -20px;"/>';	
//$html .='</div>';
$html .='<table width="100%" cellpadding="4" cellspacing="0" style="border-bottom: 2px solid #666;">
		<tr style="color: red; font-size: 33px;">
			<th style="border-bottom: 1px solid #666; border-right: 1px solid #666; text-align: center; width: 10%;">Attempt</th>
			<th style="border-bottom: 1px solid #666; border-right: 1px solid #666; text-align: center; width: 40%;">Assessment Date & Time</th>
			<th style="border-bottom: 1px solid #666; border-right: 1px solid #666; text-align: center; width: 20%;">Time Taken</th>
			<th style="border-bottom: 1px solid #666; border-right: 1px solid #666; text-align: center; width: 15%;">Grade / 20</th>
			<th style="border-bottom: 1px solid #666; text-align: center; width: 15%;">Feedback</th>
		</tr>
';

foreach($attempts as $attempt){		
		/*$attemptgrade = quiz_rescale_grade($attempt->sumgrades, $quiz, false);	
		$qresponse = quiz_feedback_for_grade($attemptgrade, $quiz->id);		*/
		global $DB;
		$grade = $attempt->sumgrades;
	
		// With CBM etc, it is possible to get -ve grades, which would then not match
		// any feedback. Therefore, we replace -ve grades with 0.
		$grade = max($grade, 0);

		$feedback = $DB->get_record_select('quiz_feedback',
				'quizid = ? AND mingrade <= ? AND ? < maxgrade', array($quiz->id, $grade, $grade));

		if (empty($feedback->feedbacktext)) {
			$f_feedback = '';
		}else{
			$f_feedback = $feedback->feedbacktext;
		}
	
		$timetaken = '';
        $datecompleted = '';
            if ($attempt->timefinish > 0){
                // attempt has finished
                $timetaken = format_time($attempt->timefinish - $attempt->timestart);				
				$datecompleted = 'Started at : '.userdate($attempt->timestart).'<br />Finished at : '.userdate($attempt->timefinish);				
				//$datecompleted = date('m/d/Y h:s a', userdate($attempt->timestart)).' - '.date('m/d/Y h:s a', userdate($attempt->timefinish));				
            } else if ($available) {
                // The attempt is still in progress.
                $timetaken = format_time(time() - $attempt->timestart);
                $datecompleted = '';
            } else if ($quiz->timeclose) {
                // The attempt was not completed but is also not available any more becuase the quiz is closed.
                $timetaken = format_time($quiz->timeclose - $attempt->timestart);
                $datecompleted = 'Started at : '.userdate($attempt->timestart).'<br />Finished at : '.userdate($quiz->timeclose);
				//$datecompleted = date('m/d/Y h:s a', userdate($attempt->timestart)).' - '.date('m/d/Y h:s a', userdate($attempt->timeclose));				
            } else {
                // Something weird happened.
                $timetaken = '';
                $datecompleted = '';
            }
		
	//if($attempt->sumgrades < $fgrade){				
		$htmlstyle ='text-align: center; border: 1px solid #666;';
	/*}else{
	    $htmlstyle .='text-align: center; background-color: yellow; border: 1px solid #666; font-weight: bold;';
	}*/
	
	$html .='<tr style="font-size: 33px;">
			<td style="'.$htmlstyle.'">'.$attempt->attempt.'</td>			
			<td style="'.$htmlstyle.' font-size: 20px;">'.strtoupper($datecompleted).'</td>
			<td style="'.$htmlstyle.' font-size: 27px;">'.$timetaken.'</td>
			<td style="'.$htmlstyle.'">'.number_format($attempt->sumgrades).'</td>
		    <td style="'.$htmlstyle.'">'.$f_feedback.'</td>
			</tr>';
}	

$html .='<tr><td colspan="5" style="border-bottom: 1px solid #666;"></td></tr></table><br />';
$html .='<div style="font-size: 20px; float: left;">';
	$html .='For question and clarification about this report<br />
			 please contact Adam Antonio Fidelino<br />
			 Cellphone # 09059991713<br />
			 Tel (02) 8923094 or (02) 813-7004 to 07 Local 504 and 505<br />
			 or email to afidelino@mlhuillier.com';
$html .='</div>';
$html .='<div style="float: left; widt: 250px; height: 250px;">';
	
$html .='</div>';
$html .='<div style="clear: both;"></div>';
// Print text using writeHTMLCell()
//$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTML($html, true, false, true, false, '');
// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($quiz->name.'_'.$course->fullname.'_result.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+