<?php
	
	if(isset($_GET['examinationdate'])){
		$dateofexam = $_GET['examinationdate'];
	}else{
		echo "Unable to retreive data, Please try again";
		exit();
	}
	
	if(isset($_GET['mapid'])){
		$mapid = $_GET['mapid'];
	}else{
		echo "Unable to retrieve data, Please try again";
		exit();
	}
	
	$exam_year = date('Y', strtotime($dateofexam));
	$exam_month = date('m', strtotime($dateofexam));	
	$main_dir = "accounts";
	$default_file = $dateofexam.'_'.$mapid.'.csv';
	
	function checkFile($year, $month, $dir, $file){
		if(file_exists($dir.'/'.$year.'/'.$month.'/'.$file)){
			return true;
		}else{
			return false;
		}
	}
	
	if(checkFile($exam_year, $exam_month, $main_dir, $default_file)){
		echo '<div style ="text-align: center; font: 12px verdana, heveltica, sans-serif;">';
			echo 'File is now ready for download <a href="'.$main_dir.'/'.$exam_year.'/'.$exam_month.'/'.$default_file.'" style="color: red;">Click Here</a>';
		echo '</div>';
	}else{
		echo '<div style="text-align: center; color: red; font: 13px tahoma, heveltica, sans-serif;">Waiting for file to be uploaded, Please try again later.</div>';
	}
	
?>