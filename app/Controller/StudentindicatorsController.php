<?php
App::uses('AppController', 'Controller');
/**
 * Studentindicators Controller
 *
 * @property Studentindicator $Studentindicator
 */
class StudentindicatorsController extends AppController {

	
	public function getindicator($name=null){		
		$indicator = $this->Studentindicator->findByCode($name);
		
		if(!empty($indicator)){
			return $indicator['Studentindicator']['id'];
		}else{
			return 1;
		}
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Studentindicator->recursive = 0;
		$this->set('studentindicators', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Studentindicator->id = $id;
		if (!$this->Studentindicator->exists()) {
			throw new NotFoundException(__('Invalid studentindicator'));
		}
		$this->set('studentindicator', $this->Studentindicator->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Studentindicator->create();
			if ($this->Studentindicator->save($this->request->data)) {
				$this->Session->setFlash(__('The studentindicator has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The studentindicator could not be saved. Please, try again.'), 'error_message');
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Studentindicator->id = $id;
		if (!$this->Studentindicator->exists()) {
			throw new NotFoundException(__('Invalid studentindicator'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Studentindicator->save($this->request->data)) {
				$this->Session->setFlash(__('The studentindicator has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The studentindicator could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Studentindicator->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Studentindicator->id = $id;
		if (!$this->Studentindicator->exists()) {
			throw new NotFoundException(__('Invalid studentindicator'));
		}
		if ($this->Studentindicator->delete()) {
			$this->Session->setFlash(__('Studentindicator deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Studentindicator was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
