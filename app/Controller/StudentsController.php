<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'phpexcel/index');
//App::import('Vendor', 'dompdf/dompdf_config.inc');


/**
 * Students Controller
 *
 * @property Student $Student
 */
class StudentsController extends AppController {

	public function getallstudents($id){
		$students = $this->Student->find('all', array('conditions' => array('Student.gradelevel_id' => $id)));
		return $students;
	}
	
	public function getallstudentsinsection($id){
		$students = $this->Student->find('all', array('conditions' => array('Student.gradesection_id' => $id)));
		return $students;
	}
	
	public function getallstudentsinteacher($id){
		$students = $this->Student->find('all', array('conditions' => array('Student.teacher_id' => $id)));
		return $students;
	}
	
	public function getstudentidbeforeupload($firstname=null, $middlename=null, $lastname=null, $schoolyear=null){
		$student = $this->Student->find('first', array('conditions' => array(
				'Student.firstname' 	=> $firstname,
				'Student.middlename' 	=> $middlename,
				'Student.lastname' 		=> $lastname,
				'Student.schoolyear_id' 		=> $schoolyear
				)
			)
		);
		
		if(!empty($student)){
			return $student['Student']['id'];
		}else{	
			return 0;
		}
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index($sy=null) {
		$this->Student->recursive = 0;
		
		if($this->request->is('post') || $this->request->is('put')){
			
			if(!empty($this->data['Search']['keyword'])){
			
				$keywords = $this->data['Search']['keyword'];						
				$lrn = $this->data['Search']['lrn'];	
				
				if(!empty($keyword) && !empty($lrn)){
						$this->paginate = array('conditions' => array(
							'OR' => array(
								'Student.firstname LIKE' => "%$keywords%",
								'Student.lastname LIKE' => "%$keywords%",
								'Student.lrn LIKE' => "%$lrn%"								
							)
						));
				}else{
					if(!empty($keyword)){
						$this->paginate = array('conditions' => array(
							'OR' => array(
								'Student.firstname LIKE' => "%$keywords%",
								'Student.lastname LIKE' => "%$keywords%"
							)
						));
					}
					
					if(!empty($lrn)){
						$this->paginate = array('conditions' => array(
							'OR' => array(
								'Student.lrn LIKE' => "%$lrn%"				
							)
						));
					}
				}
				
				$this->set('studentscount', $this->Student->find('count', array('conditions' => array(
					'OR' => array(
						'Student.firstname LIKE' => "%$keywords%",
						'Student.lastname LIKE' => "%$keywords%"
					)
				))));
				
			}else{
			
				$this->paginate = array(
					'conditions' => array('Student.schoolyear_id'	=> $this->data['Filter']['schoolyear_id']),
					'limit'		=> 200,
					'order'	=> array('Student.lastname' => 'ASC')
				);
				
				$this->set('studentscount', $this->Student->find('count', array('conditions' => array('Student.schoolyear_id'	=> $this->data['Filter']['schoolyear_id']))));
			}
						
		}else{
			$this->paginate = array(
				'conditions' => array('Student.schoolyear_id'	=> 1),
				'order'	=> array('Student.lastname' => 'ASC')
			);
			
			$this->set('studentscount', $this->Student->find('count', array('conditions' => array('Student.schoolyear_id'	=> 1))));
		}
		
		$this->set('students', $this->paginate());
	
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('schoolyears'));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Student->id = $id;
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		$this->set('student', $this->Student->read(null, $id));
		
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$userid = $this->Auth->user('id');
		$this->set('userid', $userid);
		
		if ($this->request->is('post')) {			
			if($this->checkifstudentexists($this->data['Student']['firstname'], $this->data['Student']['lastname'], $this->data['Student']['middlename'], $this->combinebirthdate($this->data['Student']['birthdate'], $this->data['Student']['schoolyear_id']))){
				$this->Student->create();
				if ($this->Student->save($this->request->data)) {
					$this->Session->setFlash(__('The student information has been saved'), 'success_message');
					$this->redirect(array('action' => 'add'));
				} else {
					$this->Session->setFlash(__('The student could not be saved. Please, try again.'), 'error_message');
				}
			}else{
				$this->Session->setFlash(__('The student could not be saved. Data already exists.'), 'error_message');				
			}
		}
		$studentindicators = $this->Student->Studentindicator->find('list');
		$gradelevels = $this->Student->Gradelevel->find('list');
		$teachers = $this->Student->Teacher->find('list');
		$gradesections = $this->Student->Gradesection->find('list');
		$schoolyears = $this->Student->Schoolyear->find('list');
		$this->set(compact('studentindicators', 'gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	public function combinebirthdate($birthday){
		$year = $birthday['year'];
		$month = $birthday['month'];
		$day = $birthday['day'];
		$birthday  = $year.'-'.$month.'-'.$day;
		return $birthday;
	}
	
	public function batchupload(){
			
		$objPHPExcel = new PHPExcel();
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader = new PHPExcel_Reader_Excel5();
		$objReader->setReadDataOnly(true);		
		
		$userid = $this->Auth->user('id');
		$total = 0;
		
		if ($this->request->is('post')) {
			if(empty($this->data['Student']['filetoupload']['name'])  || empty($this->data['Student']['filetoupload']['tmp_name'])){
				$this->Session->setFlash(__('Invalid file, please try again.'), 'error_message');
			}else{
				if($this->checkforextension($this->data['Student']['filetoupload'])){			
					
					$objPHPExcel = PHPExcel_IOFactory::load($this->data['Student']['filetoupload']['tmp_name'], "r");
					$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);										
					$heighestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();	
					
					for($i=10; $i<=$heighestRow; $i++){ 	
									
									$lrn				= $objWorksheet->getCell('A'.$i.'')->getValue();
									$lastname			= $objWorksheet->getCell('B'.$i.'')->getValue();
									$firstname			= $objWorksheet->getCell('C'.$i.'')->getValue();
									$middlename			= $objWorksheet->getCell('D'.$i.'')->getValue();
									$gender				= $objWorksheet->getCell('E'.$i.'')->getValue();
									$birthdate			= date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString($objWorksheet->getCell('F'.$i.'')->getValue(), 'YYYY-MM-DD')));
									$age 				= $objWorksheet->getCell('J'.$i.'')->getValue(); //as of  first friday of june
									$birthpalce			= $objWorksheet->getCell('H'.$i.'')->getValue();
									$mother_tongue		= $objWorksheet->getCell('I'.$i.'')->getValue();
									$ethnic_group		= $objWorksheet->getCell('J'.$i.'')->getValue();
									$religion			= $objWorksheet->getCell('K'.$i.'')->getValue();
									$address_house		= $objWorksheet->getCell('L'.$i.'')->getValue();
									$address_brgy		= $objWorksheet->getCell('M'.$i.'')->getValue();
									$address_municipal	= $objWorksheet->getCell('N'.$i.'')->getValue();
									$adderss_province	= $objWorksheet->getCell('O'.$i.'')->getValue();
									$father_fullname	= $objWorksheet->getCell('P'.$i.'')->getValue();
									$mother_fullname	= $objWorksheet->getCell('Q'.$i.'')->getValue();
									$guardian_fullname	= $objWorksheet->getCell('R'.$i.'')->getValue();
									$relationship		= $objWorksheet->getCell('S'.$i.'')->getValue();
									$contact_number		= $objWorksheet->getCell('T'.$i.'')->getValue();
									$studentindicatorname	= $objWorksheet->getCell('U'.$i.'')->getValue();
									
									$schoolyear			= $this->data['Student']['schoolyear_id'];
									
									$indicator = $this->Uploadfile->getindicatorid($studentindicatorname);
									
									//check if data exists update the information

										if((preg_match('/^[0-9]*$/', $lrn) && strlen($lrn) >= 3) && !empty($firstname) && !empty($lastname)){											
											$lrn_id = $this->checkiflrnexists($lrn);
											$studentid = $this->checkifexists($firstname, $lastname, $middlename, $birthdate, $schoolyear);
											if(!empty($studentid) || !empty($lrn_id)){
												//update the data
											}else{
												$total++;
												$data[] = array(
													'Student' => array(												
														 'lrn'					=>	$lrn,
														 'firstname'			=>	$firstname,
														 'middlename'			=>	$middlename,
														 'lastname'				=>	$lastname,
														 'gender'				=>	$gender,
														 'birthdate'			=>	$birthdate,
														 'age'					=>	0,
														 'birthpalce'			=>	$birthpalce,
														 'mother_tongue'		=>	$mother_tongue,
														 'ethnic_group'			=>	$ethnic_group,
														 'religion'				=>	$religion,
														 'address_house'		=>	$address_house,
														 'address_brgy'			=>	$address_brgy,
														 'address_municipal'	=>	$address_municipal,
														 'adderss_province'		=>	$adderss_province,
														 'father_fullname'		=>	$father_fullname,
														 'mother_fullname'		=>	$mother_fullname,
														 'guardian_fullname'	=>	$guardian_fullname,
														 'relationship'			=>	$relationship,
														 'contact_number'		=>	$contact_number,
														 'studentindicator_id'	=>	$indicator,
														 'added'				=>	date('Y-m-d'),
														 'added_by'				=>	$this->Auth->user('id'),
														 'modified'				=>	date('Y-m-d'),
														 'modified_by'			=>	0,
														 'gradelevel_id'		=>	$this->data['Student']['gradelevel_id'],
														 'gradesection_id'		=>	$this->data['Student']['gradesection_id'],
														 'teacher_id'			=>	$this->data['Student']['teacher_id'],
														 'schoolyear_id'		=>	$this->data['Student']['schoolyear_id']
													)
												);
											}
										}
									
																	
					}
					
					if(!empty($data)){
						$this->Student->create();
						if ($this->Student->saveAll($data)) {
							$this->Session->setFlash('( '. $total . ' ) Total student information that has been saved & updated.', 'success_message');
							$this->redirect(array('action' => 'index'));
						} else {
							$this->Session->setFlash(__('The student could not be saved. Please, try again.'), 'error_message');
						}
					}else{
							$this->Session->setFlash(__('No data to be processed, please try again.'), 'error_message');
					}
				}else{
					$this->Session->setFlash(__('You have uploaded an invalid file, please try again.'), 'error_message');
				}
			}
		}
		$studentindicators = $this->Student->Studentindicator->find('list');
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$gradesections = $this->Student->Gradesection->find('list', array('order' => array('Gradesection.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list', array('order' => array('Teacher.name' => 'ASC')));
		$schoolyears = $this->Student->Schoolyear->find('list');
		$this->set(compact('studentindicators', 'gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	public function checkforextension($filename){
		if(!empty($filename['name'])){
				$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
				$ext=0;
				
				//check the extension
				switch($extension){
					case "xls": $ext++; break;				
					case "xlsx": $ext++; break;													
				}	
				
				if($ext > 0){
					return true;
				}else{
					return false;
				}
		}else{
			return false;
		}
							
	}
	
	
	public function checkiflrnexists($lrn){
		$student = $this->Student->find('first', array(
			'conditions' => array(
				'Student.lrn' 	=> $lrn			
				)
			)
		);
		
		if(!empty($student)){
			return $student['Student']['id'];
		}else{
			return 0;
		}
	}
	
	public function checkifexists($firstname, $lastname, $middlename, $birthdate, $schoolyear){
		$student = $this->Student->find('first', array(
			'conditions' => array(
				'Student.firstname' 	=> $firstname, 
				'Student.lastname'  	=> $lastname, 
				'Student.middlename' 	=> $middlename,
				'Student.birthdate'		=> $birthdate,
				'Student.schoolyear_id'	=> $schoolyear,
				)
			)
		);
		
		if(!empty($student)){
			return $student['Student']['id'];
		}else{
			return 0;
		}
	}
	
	public function checkifstudentexists($firstname, $lastname, $middlename, $birthdate){
		$student = $this->Student->find('count', array('conditions' => array(
					'Student.firstname' 	=> $firstname, 
					'Student.lastname'  	=> $lastname, 
					'Student.middlename' 	=> $middlename,
					'Student.birthdate'		=> $birthdate
				)
			)
		);
		
		if($student > 0){
			return false;
		}else{
			return true;
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Student->id = $id;
		$userid = $this->Auth->user('id');
		
		$this->set('userid', $userid);
		
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Student->save($this->request->data)) {
				$this->Session->setFlash(__('The student information has been saved'), 'success_message');
				$this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The student could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Student->read(null, $id);
		}
		
		$studentindicators = $this->Student->Studentindicator->find('list');
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list');
		$gradesections = $this->Student->Gradesection->find('list');
		$this->set(compact('studentindicators', 'gradelevels', 'teachers', 'gradesections'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function homepage(){
		
	}
	
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Student->id = $id;
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->Student->delete()) {
			$this->Session->setFlash(__('Student deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Student was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function reportinitiate(){
		
		$this->set('download', false);
		if ($this->request->is('post')) {				
			//$this->redirect(array('action' => 'report', 'ext' => 'pdf', $this->data['Student']['teacher_id'], $this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id']));
			$this->set('download', true);
		} 
		
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list');
		$gradesections = $this->Student->Gradesection->find('list');
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	public function form1report(){
		
		
		$this->set('download', false);
		if ($this->request->is('post')) {	
			$this->set('download', true);
			//$this->redirect(array('action' => 'registrareport', 'ext' => 'pdf', $this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id']));
		} 
		
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list');
		$gradesections = $this->Student->Gradesection->find('list');
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	public function registrareport($gradelevelid, $sectionid, $syid){
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->Student->recursive = 0;
				
		$this->set('gradelevel', $this->Student->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Student->Gradesection->findById($sectionid));
		$this->set('sy', $this->Student->Schoolyear->findById($syid));
		
		$this->paginate = array(
			'conditions' => array(				
				'Student.gradelevel_id'		=> $gradelevelid,			
				'Student.gradesection_id'	=> $sectionid,			
				'Student.schoolyear_id'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC'),
			'limit'	=> 200
		);
		
		$this->set('students', $this->paginate());
	}
	
	public function report($teacherid, $gradelevelid, $sectionid, $syid){
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->Student->recursive = 0;
		
		$this->set('teacher', $this->Student->Teacher->findById($teacherid));
		$this->set('gradelevel', $this->Student->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Student->Gradesection->findById($sectionid));
		$this->set('sy', $this->Student->Schoolyear->findById($syid));
		
		$this->paginate = array(
			'conditions' => array(
				'Student.teacher_id'		=> $teacherid,			
				'Student.gradelevel_id'		=> $gradelevelid,			
				'Student.gradesection_id'	=> $sectionid,			
				'Student.schoolyear_id'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC'),
			'limit'	=> 200
		);
		
		$this->set('students', $this->paginate());
	}
	
	public function gradelevelcount($id, $gender, $sy){
		$student = $this->Student->find('count', array('conditions' => array('Student.gender' => $gender, 'Student.gradelevel_id' => $id, 'Student.schoolyear_id' => $sy)));
		return $student;
	}
}
