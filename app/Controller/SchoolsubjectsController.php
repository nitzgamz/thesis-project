<?php
App::uses('AppController', 'Controller');
/**
 * Schoolsubjects Controller
 *
 * @property Schoolsubject $Schoolsubject
 */
class SchoolsubjectsController extends AppController {

	
	public function getnamebyid($id){
	$name = $this->Schoolsubject->findById($id);
	return $name['Schoolsubject']['name'];
	
  }
  
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Schoolsubject->recursive = 0;
		$this->set('schoolsubjects', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Schoolsubject->id = $id;
		if (!$this->Schoolsubject->exists()) {
			throw new NotFoundException(__('Invalid schoolsubject'));
		}
		$this->set('schoolsubject', $this->Schoolsubject->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Schoolsubject->create();
			if ($this->Schoolsubject->save($this->request->data)) {
				$this->Session->setFlash(__('The school subject has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schoolsubject could not be saved. Please, try again.'), 'error_message');
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Schoolsubject->id = $id;
		if (!$this->Schoolsubject->exists()) {
			throw new NotFoundException(__('Invalid schoolsubject'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Schoolsubject->save($this->request->data)) {
				$this->Session->setFlash(__('The school subject has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schoolsubject could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Schoolsubject->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Schoolsubject->id = $id;
		if (!$this->Schoolsubject->exists()) {
			throw new NotFoundException(__('Invalid schoolsubject'));
		}
		if ($this->Schoolsubject->delete()) {
			$this->Session->setFlash(__('Schoolsubject deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Schoolsubject was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
