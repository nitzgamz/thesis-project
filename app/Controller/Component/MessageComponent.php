<?php
	class MessageComponent extends Component{		
		
		public function getMessage($message){
			switch($message){
		
				case "ok":
				$message = "Data has been successfully processed.";
				break;
				
				case "error":
				$message = "Unable to process your request, Please try again";
				break;
				
				case "login_error":
				$message = "Login failed, Please try again";
				break;
				
				default:
				$message = "Save Error : ".' '.$message;
				break;
			}
			
			return $message;
		}
		
		
		
	}
?>