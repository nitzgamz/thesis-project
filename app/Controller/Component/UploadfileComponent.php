<?php
	class UploadfileComponent extends Component{		
		
		public function checkifexists($dir, $subfolders, $filename){
			if(file_exists($dir.'/'.$subfolders.'/'.$filename)){
				return true;
			}else{
				return false;
			}
		}
		
		public function loadthefile($dir, $subfolders, $filename){
			$loadthefile = $dir.'/'.$subfolders.'/'.$filename;
			return $loadthefile;
		}
		public function checkFolder($dir, $foldername){
			if(is_dir($dir.'/'.$foldername) && is_writeable($dir.'/'.$foldername)){							
				return true;
			}else{
				if(mkdir($dir.'/'.$foldername, 0777)){
					return true;
				}else{
					return false;
				}
			}
		}
			
		public function uploadprimarypicture($filename=null){
			$continueupload = false;
			
			if(!empty($filename['name'])){
				$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
				$ext=0;
				
				//check the extension
				switch($extension){
					case "png": $ext++; break;				
					case "jpg": $ext++; break;				
					case "jpeg": $ext++; break;												
				}	
				
				if($ext > 0){
					$continueupload = true;
				}
				
				if($continueupload){	
					//check the size
					
						return true;
					
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		
		public function uploadattachment($filename=null){
			$continueupload = false;
			
			if(!empty($filename['name'])){
				$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
				$ext=0;
				
				//check the extension
				switch($extension){
					case "png": $ext++; break;				
					case "jpg": $ext++; break;				
					case "jpeg": $ext++; break;												
					case "doc": $ext++; break;												
					case "docx": $ext++; break;												
					case "xls": $ext++; break;												
					case "xlsx": $ext++; break;												
					case "pdf": $ext++; break;												
					case "ppt": $ext++; break;												
					case "txt": $ext++; break;																	
					case "csv": $ext++; break;																	
				}	
				
				if($ext > 0){
					$continueupload = true;
				}
				
				if($continueupload){	
					//check the size
					
						return true;
					
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		
		public function uploadforDownloadfile($filename, $directory, $newfilename=null){
			$dir = $directory;						
			$upload_dir  = $dir . basename($filename['name']);												
			if(move_uploaded_file($filename['tmp_name'], $upload_dir)){
				$file_handler = fopen($upload_dir, 'r');	
				fclose($file_handler);
				if(!empty($newfilename)){
					if(rename($upload_dir, $dir.''.$newfilename)){			
						return true;
					}else{
						return false;
					}
				}else{
					return true;
				}				
			}else{
				return false;
			}			
		}
		
		public function uploadSchedule($filename, $directory, $newfilename, $forextractdir){
			$dir = $directory;						
			$upload_dir  = $dir . basename($filename['name']);												
			if(move_uploaded_file($filename['tmp_name'], $upload_dir)){
				$file_handler = fopen($upload_dir, 'r');	
				fclose($file_handler);
				if(!empty($newfilename)){
					if(rename($upload_dir, $dir.''.$newfilename)){
						if(copy($dir.'/'.$newfilename, $forextractdir.'/'.$newfilename)){
							return true;
						}else{
							return false;
						}
					}else{
						return false;
					}
				}
			}else{
				return false;
			}			
		}
		
		public function uploadResult($filename, $directory, $newfilename){
			$dir = $directory;						
			$upload_dir  = $dir . basename($filename['name']);												
			if(move_uploaded_file($filename['tmp_name'], $upload_dir)){
				$file_handler = fopen($upload_dir, 'r');	
				fclose($file_handler);
				if(!empty($newfilename)){
					if(rename($upload_dir, $dir.''.$newfilename)){
						return true;
					}else{
						return false;
					}
				}
			}else{
				return false;
			}			
		}
		
		public function uploadSchedulecopy($filetocopy, $directory){
			$dir = $directory;	
			/*
			| check if the file still exists
			*/ 
			if(file_exists($filetocopy)){
				$file_handler = fopen($filetocopy, 'r');	
				fclose($file_handler);								
				if(copy($filetocopy, $dir.'/schedule.xls')){
					if(unlink($filetocopy)){ return true; }else{ return false; }
				}else{ return false; }					
			}else{
				return false;
			}
		}
		
				
		
		public function renameImage($filename, $newfilename){
			$filename = $filename['name'];
			$extension = end(explode(".", $filename));
			$newfilename = $newfilename.".".$extension;
			return $newfilename;
		}
	
		public function checkfileExtensionforfile($filename){
			$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
			//$extension = substr($filename, strrpos($filename, '.')+1);
			$ext=0;
			switch($extension){				
				case "xls": $ext++; break;	
				default: $ext=0; break;
			}						
			if($ext>0){return true;}else{ return false;}
		}
		
		public function checkfileExtensionforfile2007($filename){
			$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
			//$extension = substr($filename, strrpos($filename, '.')+1);
			$ext=0;
			switch($extension){				
				case "xlsx": $ext++; break;	
				default: $ext=0; break;
			}						
			if($ext>0){return true;}else{ return false;}
		}
			
		public function checkfileExtensionforfileforpicture($filename){			
			$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
			$ext=0;
			switch($extension){
				case "png": $ext++; break;				
				case "jpeg": $ext++; break;				
				case "jpg": $ext++; break;								
			}						
			if($ext>0){return true;}else{ return false;}
		}
		
		public function checkfileExtensionforfileforattachment($filename){			
			$extension = pathinfo($filename['name']);
			$ext=0;
			switch($extension['extension']){
				case "doc": $ext++; break;				
				case "docx": $ext++; break;				
				case "pdf": $ext++; break;								
				case "png": $ext++; break;								
				case "jpg": $ext++; break;								
				case "xls": $ext++; break;								
				case "xlsx": $ext++; break;								
			}						
			if($ext>0){return true;}else{ return false;}
		}
		
		
		public function resizeImage($inputFileName, $maxSize=100){			
			$info = getimagesize($inputFileName);
			$type = isset($info['type']) ? $info['type'] : $info[2];
			if (!(imagetypes() & $type)) {
				return false;
			}

			$width = isset($info['width']) ? $info['width'] : $info[0];
			$height = isset($info['height']) ? $info['height'] : $info[1];

			// Calculate aspect ratio
			$wRatio = $maxSize / $width;
			$hRatio = $maxSize / $height;

			// Using imagecreatefromstring will automatically detect the file type
			$sourceImage = imagecreatefromstring(file_get_contents($inputFileName));

			// Calculate a proportional width and height no larger than the max size.
			if (($width <= $maxSize) && ($height <= $maxSize)) {
				// Input is smaller than thumbnail, do nothing
				return $sourceImage;
			} elseif (($wRatio * $height) < $maxSize) {
				// Image is horizontal
				$tHeight = ceil($wRatio * $height);
				$tWidth = $maxSize;
			} else {
				// Image is vertical
				$tWidth = ceil($hRatio * $width);
				$tHeight = $maxSize;
			}

			$thumb = imagecreatetruecolor($tWidth, $tHeight);

			if ($sourceImage === false) {
				// Could not load image
				return false;
			}

			// Copy resampled makes a smooth thumbnail
			imagecopyresampled($thumb, $sourceImage, 0, 0, 0, 0, $tWidth, $tHeight, $width, $height);
			imagedestroy($sourceImage);

			return $thumb;
		}
		
		
		public function getindicatorid($name){
			$indicator = $this->requestAction(array('controller' => 'studentindicators', 'action' => 'getindicator', $name));
			if(empty($indicator)){
				$indicator = 1;
			}else{
				$indicator = $indicator;
			}
			return $indicator;
		}
		
		
		public function getstudentidbeforeupload($firstname, $middlename, $lastname, $schoolyear){
			$studentid = $this->requestAction(array('controller' => 'students', 'action' => 'getstudentidbeforeupload', $firstname, $middlename, $lastname, $schoolyear));
			return $studentid;
		}
		
		
	}
?>