<?php
App::uses('AppController', 'Controller');
/**
 * Schoolyears Controller
 *
 * @property Schoolyear $Schoolyear
 */
class SchoolyearsController extends AppController {
	
  public function getnamebyid($id){
	$name = $this->Schoolyear->findById($id);
		return $name['Schoolyear']['name'];
	
  }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Schoolyear->recursive = 0;
		$this->set('schoolyears', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Schoolyear->id = $id;
		if (!$this->Schoolyear->exists()) {
			throw new NotFoundException(__('Invalid schoolyear'));
		}
		$this->set('schoolyear', $this->Schoolyear->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Schoolyear->create();
			if ($this->Schoolyear->save($this->request->data)) {
				$this->Session->setFlash(__('The schoolyear has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schoolyear could not be saved. Please, try again.'), 'error_message');
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Schoolyear->id = $id;
		if (!$this->Schoolyear->exists()) {
			throw new NotFoundException(__('Invalid schoolyear'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Schoolyear->save($this->request->data)) {
				$this->Session->setFlash(__('The schoolyear has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schoolyear could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Schoolyear->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Schoolyear->id = $id;
		if (!$this->Schoolyear->exists()) {
			throw new NotFoundException(__('Invalid schoolyear'));
		}
		if ($this->Schoolyear->delete()) {
			$this->Session->setFlash(__('Schoolyear deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Schoolyear was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
