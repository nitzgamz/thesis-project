<?php
App::uses('AppController', 'Controller');
/**
 * Teachersubjects Controller
 *
 * @property Teachersubject $Teachersubject
 */
class TeachersubjectsController extends AppController {

	
	public function getallteacher($id){
		$subjects = $this->Teachersubject->find('all', array('conditions' => array('Teachersubject.teacher_id' => $id)));
		return $subjects;
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Teachersubject->recursive = 0;
		$this->set('teachersubjects', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Teachersubject->id = $id;
		if (!$this->Teachersubject->exists()) {
			throw new NotFoundException(__('Invalid teachersubject'));
		}
		$this->set('teachersubject', $this->Teachersubject->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($teacherid=null) {
		if ($this->request->is('post')) {
			if($this->check_if_assigned_already($this->data['Teachersubject']['schoolsubject_id'], $this->data['Teachersubject']['teacher_id'])){
				$this->Teachersubject->create();
				if ($this->Teachersubject->save($this->request->data)) {
					$this->Session->setFlash(__('The teachersubject has been saved'), 'success_message');
					if(isset($teacherid) && !empty($teacherid)){
						$this->redirect(array('controller' => 'teachersubjects', 'action' => 'add', $teacherid));
					}else{
						$this->redirect(array('controller' => 'teachersubjects', 'action' => 'add', $this->data['Teachersubject']['teacher_id']));
					}
				} else {
					$this->Session->setFlash(__('The teachersubject could not be saved. Please, try again.'), 'error_message');
				}
			}else{
				$this->Session->setFlash(__('The teacher already assigned to this subject.'), 'error_message');
			}
		}
		
		if(isset($teacherid) && !empty($teacherid)){
			$teachers = $this->Teachersubject->Teacher->find('list', array('conditions' => array('Teacher.id' => $teacherid)));
		}else{
			$teachers = $this->Teachersubject->Teacher->find('list');
		}
		$schoolsubjects = $this->Teachersubject->Schoolsubject->find('list');
		$this->set(compact('teachers', 'schoolsubjects'));
	}
	
	function check_if_assigned_already($tid, $sid){
		$id = $this->Teachersubject->find('first', array('conditions' => array('Teachersubject.schoolsubject_id' => $sid, 'Teachersubject.teacher_id' => $tid)));
		if(!empty($id)){
			return false;
		}else{
			return true;
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Teachersubject->id = $id;
		if (!$this->Teachersubject->exists()) {
			throw new NotFoundException(__('Invalid teachersubject'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if($this->check_if_assigned_already($this->data['Teachersubject']['schoolsubject_id'], $this->data['Teachersubject']['teacher_id'])){
				if ($this->Teachersubject->save($this->request->data)) {
					$this->Session->setFlash(__('The teachersubject has been saved'), 'success_message');
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The teachersubject could not be saved. Please, try again.'), 'error_message');
				}
			}else{
				$this->Session->setFlash(__('The teacher already assigned to this subject.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Teachersubject->read(null, $id);
		}
		$teachers = $this->Teachersubject->Teacher->find('list');
		$schoolsubjects = $this->Teachersubject->Schoolsubject->find('list');
		$this->set(compact('teachers', 'schoolsubjects'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Teachersubject->id = $id;
		if (!$this->Teachersubject->exists()) {
			throw new NotFoundException(__('Invalid teachersubject'));
		}
		if ($this->Teachersubject->delete()) {
			$this->Session->setFlash(__('Teachersubject deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Teachersubject was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
