<?php
App::uses('AppController', 'Controller');
/**
 * Gradelevels Controller
 *
 * @property Gradelevel $Gradelevel
 */
class GradelevelsController extends AppController {

	
  public function getnamebyid($id){
	$name = $this->Gradelevel->findById($id);
	return $name['Gradelevel']['name'];
	
  }
  
/**
 * index method
 *
 * @return void
 */
	public function index() {		
		$this->Gradelevel->recursive = 0;
		$this->paginate = array(
			'order' => array('Gradelevel.name' => 'ASC')
		);
		$this->set('gradelevels', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Gradelevel->id = $id;
		if (!$this->Gradelevel->exists()) {
			throw new NotFoundException(__('Invalid gradelevel'));
		}
		$this->set('gradelevel', $this->Gradelevel->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Gradelevel->create();
			if ($this->Gradelevel->save($this->request->data)) {
				$this->Session->setFlash(__('The information has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gradelevel could not be saved. Please, try again.'), 'error_message');
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Gradelevel->id = $id;
		if (!$this->Gradelevel->exists()) {
			throw new NotFoundException(__('Invalid gradelevel'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Gradelevel->save($this->request->data)) {
				$this->Session->setFlash(__('The gradelevel has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gradelevel could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Gradelevel->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Gradelevel->id = $id;
		if (!$this->Gradelevel->exists()) {
			throw new NotFoundException(__('Invalid gradelevel'));
		}
		if ($this->Gradelevel->delete()) {
			$this->Session->setFlash(__('Gradelevel deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Gradelevel was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function reportinitiate(){
		
		$this->set('download', false);
		if ($this->request->is('post')) {				
			//$this->redirect(array('action' => 'report', 'ext' => 'pdf', $this->data['Gradelevel']['schoolyear_id']));
			$this->set('download', true);
		} 

		$schoolyears = $this->Gradelevel->Student->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('schoolyears'));
	}
	
	public function report($syid){		
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->set('sy', $this->Gradelevel->Student->Schoolyear->findById($syid));
	
	}
}
