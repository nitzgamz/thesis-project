<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'phpexcel/index');

/**
 * Schoolgrades Controller
 *
 * @property Schoolgrade $Schoolgrade
 */
class SchoolgradesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Schoolgrade->recursive = 0;
		$this->paginate = array(
			'order' => array(
				'Student.lastname'	=> 'ASC'
			),
		);
		
		$this->set('schoolgrades', $this->paginate());
	}
	
	public function upload(){
		
		$objPHPExcel = new PHPExcel();
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader = new PHPExcel_Reader_Excel5();
		$objReader->setReadDataOnly(true);		
		
		$userid = $this->Auth->user('id');
		$this->set('userid', $userid);
		
		if ($this->request->is('post')) {
			if(empty($this->data['Schoolgrade']['filetoupload']['name'])){
				$this->Session->setFlash(__('Invalid file, please try again.'), 'error_message');
			}else{
				if($this->checkforextension($this->data['Schoolgrade']['filetoupload'])){			
					
					$objPHPExcel = PHPExcel_IOFactory::load($this->data['Schoolgrade']['filetoupload']['tmp_name'], "r");
					$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);										
					$heighestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();	
					
					for($i=11; $i<=$heighestRow; $i++){ 	
						
						$grades_init = array(
								"ENGLISH",
								$objWorksheet->getCell('F'.$i.'')->getValue(),
								$objWorksheet->getCell('G'.$i.'')->getValue(),
								$objWorksheet->getCell('H'.$i.'')->getValue(),
								$objWorksheet->getCell('I'.$i.'')->getValue(),
								$objWorksheet->getCell('J'.$i.'')->getValue(),
								"MATHEMATICS",
								$objWorksheet->getCell('K'.$i.'')->getValue(),
								$objWorksheet->getCell('L'.$i.'')->getValue(),
								$objWorksheet->getCell('M'.$i.'')->getValue(),
								$objWorksheet->getCell('N'.$i.'')->getValue(),
								$objWorksheet->getCell('O'.$i.'')->getValue(),
								"SCIENCE",
								$objWorksheet->getCell('P'.$i.'')->getValue(),
								$objWorksheet->getCell('Q'.$i.'')->getValue(),
								$objWorksheet->getCell('R'.$i.'')->getValue(),
								$objWorksheet->getCell('S'.$i.'')->getValue(),
								$objWorksheet->getCell('T'.$i.'')->getValue(),
								"FILIPINO",
								$objWorksheet->getCell('U'.$i.'')->getValue(),
								$objWorksheet->getCell('V'.$i.'')->getValue(),
								$objWorksheet->getCell('W'.$i.'')->getValue(),
								$objWorksheet->getCell('X'.$i.'')->getValue(),
								$objWorksheet->getCell('Y'.$i.'')->getValue(),
								"MAKABAYAN",
								$objWorksheet->getCell('Z'.$i.'')->getValue(),
								$objWorksheet->getCell('AA'.$i.'')->getValue(),
								$objWorksheet->getCell('AB'.$i.'')->getValue(),
								$objWorksheet->getCell('AC'.$i.'')->getValue(),
								$objWorksheet->getCell('AD'.$i.'')->getValue(),
								"EPP",
								$objWorksheet->getCell('AE'.$i.'')->getValue(),
								$objWorksheet->getCell('AF'.$i.'')->getValue(),
								$objWorksheet->getCell('AG'.$i.'')->getValue(),
								$objWorksheet->getCell('AH'.$i.'')->getValue(),
								$objWorksheet->getCell('AI'.$i.'')->getValue(),
								"HEKASI",
								$objWorksheet->getCell('AJ'.$i.'')->getValue(),
								$objWorksheet->getCell('AK'.$i.'')->getValue(),
								$objWorksheet->getCell('AL'.$i.'')->getValue(),
								$objWorksheet->getCell('AM'.$i.'')->getValue(),
								$objWorksheet->getCell('AN'.$i.'')->getValue(),
								"MSEP",
								$objWorksheet->getCell('AO'.$i.'')->getValue(),
								$objWorksheet->getCell('AP'.$i.'')->getValue(),
								$objWorksheet->getCell('AQ'.$i.'')->getValue(),
								$objWorksheet->getCell('AR'.$i.'')->getValue(),
								$objWorksheet->getCell('AS'.$i.'')->getValue(),
								"VALUES EDUCATION",
								$objWorksheet->getCell('AT'.$i.'')->getValue(),
								$objWorksheet->getCell('AU'.$i.'')->getValue(),
								$objWorksheet->getCell('AV'.$i.'')->getValue(),
								$objWorksheet->getCell('AW'.$i.'')->getValue(),
								$objWorksheet->getCell('AX'.$i.'')->getValue()								
								
						);
						
						$fgrades 	= implode(",", $grades_init);
						
						$number 			= $objWorksheet->getCell('A'.$i.'')->getValue();
						$firstname 			= $objWorksheet->getCell('D'.$i.'')->getValue();
						$lastname 			= $objWorksheet->getCell('B'.$i.'')->getValue();
						$middlename 		= $objWorksheet->getCell('E'.$i.'')->getValue();
						$gradelevel_id		= $this->data['Schoolgrade']['gradelevel_id'];
						$gradesection_id	= $this->data['Schoolgrade']['gradesection_id'];
						$teacher_id			= $this->data['Schoolgrade']['teacher_id'];
						$grades				= $fgrades;
						$tgrade				= $objWorksheet->getCell('AY'.$i.'')->getValue();
						$ggrade				= $objWorksheet->getCell('AZ'.$i.'')->getValue();
						$schoolyear_id		= $this->data['Schoolgrade']['schoolyear_id'];
						$user_id			= $this->data['Schoolgrade']['user_id'];
						$added				= $this->data['Schoolgrade']['added'];
										
										
						//get student id
						
						$studentid = $this->Uploadfile->getstudentidbeforeupload($firstname, $middlename, $lastname, $schoolyear_id);
						
					
						
						if(!empty($studentid) && !empty($firstname) && !empty($lastname) && strlen($number) <= 3 ){

									//check if the data already exists base
									//on student id, gradelevel id, grade section id, teacher id, school year id
									
									//$current_student_id = $this->getstudentcurrentrecord($studentid, $gradelevel_id, $gradesection_id, $teacher_id, $schoolyear_id);
									$current_student_id = $this->getstudentcurrentrecord($studentid, $gradelevel_id, $gradesection_id, $teacher_id, $schoolyear_id);
									
									if(!empty($current_student_id)){
										//update the record;
										$this->Schoolgrade->id = $current_student_id; 	// This avoids the query performed by read()
										$this->Schoolgrade->saveField('grades', $grades);
										$this->Schoolgrade->saveField('tgrade', $tgrade);
										$this->Schoolgrade->saveField('ggrade', $ggrade);
										$this->Schoolgrade->saveField('modified', date('Y-m-d'));
										$this->Schoolgrade->saveField('modified_by', $userid);
										
									}else{
										//save new record
										$data[] = array(
											'Schoolgrade' => array(
												'student_id' 		=> $studentid,
												'gradelevel_id' 	=> $gradelevel_id,
												'gradesection_id' 	=> $gradesection_id,
												'teacher_id' 		=> $teacher_id,
												'grades'			=> $grades,											
												'tgrade'			=> $tgrade,
												'ggrade'			=> $ggrade,
												'schoolyear_id' 	=> $schoolyear_id,
												'user_id'			=> $user_id,
												'added'				=> $added
											)
										);
									}
							
										
						}
					}
					
					if(!empty($data)){
						$this->Schoolgrade->create();
						if ($this->Schoolgrade->saveAll($data)) {
							$this->Session->setFlash(__('All grades has been saved & updated'), 'success_message');
							$this->redirect(array('action' => 'index'));
						} else {
							$this->Session->setFlash(__('Grades information could not be saved. Please, try again.'), 'error_message');
						}
					}else{
						$this->Session->setFlash(__('You have uploaded an invalid file, please try again.'), 'error_message');
						$this->redirect(array('action' => 'index'));
					}
				}else{
					$this->Session->setFlash(__('You have uploaded an invalid file, please try again.'), 'error_message');
			
				}
					
					
			}
			
			
		}
		
				
		$teachers = $this->Schoolgrade->Teacher->find('list');
		$gradelevels = $this->Schoolgrade->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$gradesections = $this->Schoolgrade->Gradesection->find('list', array('order' => array('Gradesection.name' => 'ASC')));
		$schoolyears = $this->Schoolgrade->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	public function getstudentcurrentrecord($studentid, $gradelevel_id, $gradesection_id, $teacher_id, $schoolyear_id){
		$student = $this->Schoolgrade->find('first', array(
			'conditions' => array(
				'Schoolgrade.student_id' 		=> $studentid,
				'Schoolgrade.gradelevel_id' 	=> $gradelevel_id,	
				'Schoolgrade.gradesection_id' 	=> $gradesection_id,
				'Schoolgrade.teacher_id' 		=> $teacher_id,
				'Schoolgrade.schoolyear_id' 	=> $schoolyear_id
			)
		));
		
		if(!empty($student)){		
			return $student['Schoolgrade']['id'];
		}else{
			return 0;
		}
										
	}
	
	public function checkforextension($filename){
		if(!empty($filename['name'])){
				$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
				$ext=0;
				
				//check the extension
				switch($extension){
					case "xls": $ext++; break;				
					case "xlsx": $ext++; break;													
				}	
				
				if($ext > 0){
					return true;
				}else{
					return false;
				}
		}else{
			return false;
		}
							
	}
	
	
/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Schoolgrade->id = $id;
		if (!$this->Schoolgrade->exists()) {
			throw new NotFoundException(__('Invalid schoolgrade'));
		}
		$this->set('schoolgrade', $this->Schoolgrade->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	/*public function add() {
		if ($this->request->is('post')) {
			$this->Schoolgrade->create();
			if ($this->Schoolgrade->save($this->request->data)) {
				$this->Session->setFlash(__('The schoolgrade has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schoolgrade could not be saved. Please, try again.'));
			}
		}
		$students = $this->Schoolgrade->Student->find('list');
		$schoolsubjects = $this->Schoolgrade->Schoolsubject->find('list');
		$users = $this->Schoolgrade->User->find('list');
		$this->set(compact('students', 'schoolsubjects', 'users'));
	}*/

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Schoolgrade->id = $id;
		if (!$this->Schoolgrade->exists()) {
			throw new NotFoundException(__('Invalid schoolgrade'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Schoolgrade->save($this->request->data)) {
				$this->Session->setFlash(__('The schoolgrade has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schoolgrade could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Schoolgrade->read(null, $id);
		}
		$students = $this->Schoolgrade->Student->find('list');
		$schoolsubjects = $this->Schoolgrade->Schoolsubject->find('list');
		$users = $this->Schoolgrade->User->find('list');
		$this->set(compact('students', 'schoolsubjects', 'users'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	/*public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Schoolgrade->id = $id;
		if (!$this->Schoolgrade->exists()) {
			throw new NotFoundException(__('Invalid schoolgrade'));
		}
		if ($this->Schoolgrade->delete()) {
			$this->Session->setFlash(__('Schoolgrade deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Schoolgrade was not deleted'));
		$this->redirect(array('action' => 'index'));
	}*/
	
	public function studentprofeciency($grade, $comparison, $gender, $sy, $gradelevel, $section, $teacher){
		$total = $this->Schoolgrade->find('count', array('conditions' => array(
			'Schoolgrade.schoolyear_id' 		=> $sy,
			'Schoolgrade.ggrade '.$comparison 	=> $grade,
			'Schoolgrade.gradelevel_id'			=> $gradelevel,
			'Schoolgrade.gradesection_id'		=> $section,
			'Schoolgrade.teacher_id'			=> $teacher,
			'Student.gender'					=> $gender
		)));
		
		return $total;
	}
	
	
	
	public function studentprofeciency2($grade, $grade2, $gender, $sy, $gradelevel, $section, $teacher){
		$total = $this->Schoolgrade->find('count', array('conditions' => array(
			'Schoolgrade.schoolyear_id' 		=> $sy,
			'Schoolgrade.ggrade >='				=> $grade,
			'Schoolgrade.ggrade <='				=> $grade2,
			'Schoolgrade.gradelevel_id'			=> $gradelevel,
			'Schoolgrade.gradesection_id'		=> $section,
			'Schoolgrade.teacher_id'			=> $teacher,
			'Student.gender'					=> $gender
		)));
		
		return $total;
	}
	
	public function studentprofeciencyall($grade, $comparison, $gender, $glevel1, $glevel2, $sy){
		$total = $this->Schoolgrade->find('count', array('conditions' => array(
			'Schoolgrade.schoolyear_id' 		=> $sy,
			'Schoolgrade.ggrade '.$comparison 	=> $grade,
			'Schoolgrade.gradelevel_id'			=> array($glevel1, $glevel2),
			'Student.gender'					=> $gender
		)));
		
		return $total;
	}
	
	public function studentprofeciencyall2($grade, $grade2, $gender, $glevel1, $glevel2, $sy){
		$total = $this->Schoolgrade->find('count', array('conditions' => array(
			'Schoolgrade.schoolyear_id' 		=> $sy,
			'Schoolgrade.ggrade >='				=> $grade,
			'Schoolgrade.ggrade <='				=> $grade2,
			'Schoolgrade.gradelevel_id'			=> array($glevel1, $glevel2),
			'Student.gender'					=> $gender
		)));
		
		return $total;
	}
	
	
	public function reportinitiate(){
		
		
		$this->set('download', false);
		
		if ($this->request->is('post')) {				
			//$this->redirect(array('action' => 'report', 'ext' => 'pdf', $this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id']));
			$this->set('download', true);
		} 
		
		$gradelevels = $this->Schoolgrade->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Schoolgrade->Teacher->find('list');
		$gradesections = $this->Schoolgrade->Gradesection->find('list');
		$schoolyears = $this->Schoolgrade->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	public function report($gradelevelid, $sectionid, $syid){
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->Schoolgrade->recursive = 0;
				
		$this->set('gradelevel', $this->Schoolgrade->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Schoolgrade->Gradesection->findById($sectionid));
		$this->set('sy', $this->Schoolgrade->Schoolyear->findById($syid));
		
		$this->paginate = array(
			'conditions' => array(				
				'Schoolgrade.gradelevel_id'		=> $gradelevelid,			
				'Schoolgrade.gradesection_id'	=> $sectionid,			
				'Schoolgrade.schoolyear_id'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC'),
			'limit'	=> 200
		);
		
		$this->set('students', $this->paginate());
	}
	
}
